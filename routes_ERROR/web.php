<?php
#-------------------------------------
#RUTAS PUBLICAS
#-------------------------------------
Auth::routes(['verify' => true]);

#Login y Deslogueo
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login.post');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout');

#Registro de Usuario
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

# Password Reset
Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

#Socialite o Socialogin
//Route::get('login/facebook', 'SocialiteController@redirectToProvider')->name('login.facebook');
//Route::get('login/facebook/callback', 'SocialiteController@handleProviderCallback');
//
//Route::get('login/twitter', 'SocialiteController@redirectToTwitterProvider')->name('login.twitter');
//Route::get('login/twitter/callback', 'SocialiteController@handleTwitterProviderCallback');
//
//Route::get('login/google', 'SocialiteController@redirectToGoogleProvider')->name('login.google');
//Route::get('login/google/callback', 'SocialiteController@handleGoogleProviderCallback');

#Geografico
Route::get('searchState/{pais_id}', 'EstadoController@searchState')->name('searchState');
Route::get('searchCity/{estado_id}', 'CiudadController@searchCity')->name('searchCity');

#Buscar Vendedores
Route::get('searchVendedores/{cliente_id}', 'VendedorController@searchVendedores')->name('searchVendedores');

#Buscar Datos de un Servicio
Route::get('searchServicio/{servicio_id}', 'TiposController@searchServicio')->name('searchServicio');


#--------------------------------------------------------------------------------------
#RUTAS PROTEGIDAS POR LA AUTENTICACION Y VERIFICACION DE EMAIL AL MOMENTO DEL REGISTRO
#--------------------------------------------------------------------------------------

Route::group(['middleware' => array('auth', 'verified')], function () {

    #Home
    Route::get('/dashboard', 'HomeController@dashboard')->name('home-one');

    #Rutas del Perfil
    Route::get('/perfil', 'HomeController@perfil')->name('perfil');
    Route::post('/storeperfil', 'HomeController@storeperfil')->name('storeperfil');

    #Roles
    Route::resource('roles', 'RoleController');

    #Menus
    Route::resource('menus', 'MenuController');

    #Permisos
    Route::resource('permissions', 'PermissionController');

    #Usuarios
    Route::resource('users', 'UserController');
    Route::post('users/{user}', 'UserController@cambioClave')->name('users.cambioClave');

    #Paypal
    Route::get('payment/{pricing_id}', 'PaypalController@postPayment')->name('payment');
    Route::get('payment/status/{pricing_id}', 'PaypalController@getPaymentStatus')->name('payment.status');
    Route::get('pagado/{credito_id}', 'PaypalController@pagado')->name('pagado');

    #Tipos de Servicio
    Route::resource('tipos', 'TiposController');

    #Vendedores
    Route::resource('vendedors', 'VendedorController');

    #Clientes
    Route::resource('clientes', 'ClientesController');
    Route::get('estadoFinanciero', 'ClientesController@estadoFinanciero')->name('estadoFinanciero');

    #Solicitudes
    Route::resource('solicitudes', 'SolicitudesController');

    #Tipo de Obras
    Route::resource('tipoObras', 'TipoObrasController');

    #Ordenes de Trabajo
    Route::resource('ordenes', 'OrdenesController');

    Route::resource('productos', 'ProductosController');
    Route::get('searchProductosXCategoria', 'ProductosController@searchProductosXCategoria')->name('searchProductosXCategoria');

    Route::resource('productosCategorias', 'ProductosCategoriasController');

    Route::resource('subOrden', 'SubOrdenController');
    Route::get('subOrden/listar/{orden_id}', 'SubOrdenController@listar')->name('subOrden.listar');
    Route::get('searchXfechaXcliente/{cliente}', 'SubOrdenController@searchXfechaXcliente')->name('searchXfechaXcliente');
    Route::get('searchXfechaXclienteSinVencer/{cliente}', 'SubOrdenController@searchXfechaXclienteSinVencer')->name('searchXfechaXclienteSinVencer');
    Route::get('agenda', 'SubOrdenController@agenda')->name('agenda');
    Route::get('searchAgenda/{selectMes}/{tipoId}', 'SubOrdenController@searchAgenda')->name('searchAgenda');
    Route::get('reclamos', 'SubOrdenController@reclamos')->name('reclamos');
    Route::get('vencimientos', 'SubOrdenController@vencimientos')->name('vencimientos');

    Route::resource('subOrdenDetalle', 'SubOrdenDetalleController');


    Route::resource('autos', 'AutosController');

    Route::resource('cajas', 'CajasController');

    Route::resource('cajasProductos', 'CajasProductosController');

    Route::resource('recibos', 'RecibosController');

    Route::resource('seguimiento', 'SeguimientoController');

    Route::resource('herramientas', 'HerramientasController');

    Route::resource('convenios', 'ConveniosController');

    Route::resource('adicionalesEficiencias', 'AdicionalesEficienciaController');
    Route::get('adicionalesEficiencia', 'AdicionalesEficienciaController@adicionalesEficiencia')->name('adicionalesEficiencia');
    Route::post('guardarAdicionales', 'AdicionalesEficienciaController@guardarAdicionales')->name('guardarAdicionales');

    Route::resource('adicionalesChofers', 'AdicionalesChoferController');
    Route::get('adicionalesChofer', 'AdicionalesChoferController@adicionalesChofer')->name('adicionalesChofer');
    Route::post('guardarChofer', 'AdicionalesChoferController@guardarChofer')->name('guardarChofer');

    Route::resource('seguimientos', 'SeguimientoController');

    Route::resource('ingresosEgresos', 'IngresosEgresosController');
    Route::get('proyeccion', 'IngresosEgresosController@proyeccion')->name('proyeccion');

        /*damian */
    Route::get('mysuborders', 'SubOrdenController@mysuborders')->name('mysuborders');
    Route::get('llegadasalida', 'SeguimientoController@llegadasalida')->name('llegadasalida');
    Route::get('productividad', 'SubOrdenController@productividad')->name('productividad');

    Route::get('liquidar', 'LiquidacionController@liquidar')->name('rrhh.liquidar');
    Route::get('cuentacorriente')->name('cuentacorriente');
    Route::get('clientevencimientos')->name('clientevencimientos');
    Route::get('limitesfinanciacion')->name('limitesfinanciacion');
    Route::get('listaprecios')->name('listaprecios');
    Route::get('notificaciones')->name('notificaciones');
    Route::get('liquidacion')->name('liquidacion');

    // Route::resource('notificaciones', 'NotificacionesController');
    //
    // Route::resource('notificacionUsers', 'NotificacionUserController');
    //
    // Route::resource('sueldos', 'SueldosController');

});
