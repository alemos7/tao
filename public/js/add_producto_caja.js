$(document).ready(function () {

    $("#tablaCaja").on("click","#btnAgregarPdto",function(){
        let productoId = $('#producto_id').val();
        let productoTxt = $('#producto_id option:selected').text();
        let cantidad = parseInt($('#cantidad').val());

        if (productoId >0 && cantidad > 0) {

            $('#msg').html('');

            let items = new Object();
            items.productoId = productoId;
            items.cantidad = cantidad;

            let filaItem = `<tr>
                                        <td>${productoTxt}</td>
                                        <td>${cantidad}</td>
                                        <td align="center">
                                            <input type="hidden" name="items[]" class="itemsJSON" value='${JSON.stringify(items)}'>
                                            <button class="btn btn-outline-danger btn-sm btnDelItem" type="button" title="Quitar Item">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>                                            
                                        </td>
                                    </tr>`;

            $('#tablaCaja tbody tr:last').after(filaItem);

            $('#producto_id').val('');
            $('#cantidad').val('');
        } else {
            $('#msg').html('<div class="alert alert-danger text-center">Debe seleccionar un producto e indicar su cantidad.</div>');
        }

    });
});

$("#tablaCaja").on("click",".btnDelItem",function(){
    $(this).closest('tr').remove();
});