let clienteId = $('#cliente_id').val();
let vendedorId = $('#vendedor_select').val();

if(clienteId) {
    const url = `/searchVendedores/${clienteId}`;
    $.getJSON(url, data => {
        "use strict"
        let htmlOptions = '<option value="">:: Seleccione ::</option>';
        data.forEach(item => {
            const seleccionado = vendedorId;
            let select = '';
            if(seleccionado==item.id){
                select = 'selected';
            }
            htmlOptions += `<option value="${item.id}" ${select}>${item.vendedor}</option>`;
        });

        $('#vendedor_id').html(htmlOptions);
    });
}

$('#cliente_id').change(function () {
    "use strict"
    let clienteId = $(this).val();
    if(clienteId) {
        const url = `/searchVendedores/${clienteId}`;
        $.getJSON(url, data => {
            let htmlOptions = '<option value="">:: Seleccione ::</option>';
            data.forEach(item => {
                htmlOptions += `<option value='${item.id}'>${item.vendedor}</option>`;
            });

            $('#vendedor_id').html(htmlOptions);
        });
    }else{
        $('#vendedor_id').html(':: Debe Selecionar un Cliente ::');
    }
});