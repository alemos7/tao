let provinciaId = $('#provincia_id').val();
let localidadId = $('#ciudad_select').val();
console.log( 'provinciaId',provinciaId  );
console.log( 'localidadId', localidadId );
if(provinciaId) {
    const url = `/searchCity/${provinciaId}`;
    $.getJSON(url, data => {
        let htmlOptions = '<option value="">:: Seleccione ::</option>';
        data.forEach(item => {
            const seleccionado = localidadId;
            let select = '';
            if(seleccionado==item.id){
                select = 'selected';
            }
            htmlOptions += `<option value="${item.id}" ${select}>${item.name}</option>`;
        });

        $('#localidad_id').html(htmlOptions);
    });
}

$('#provincia_id').change(function () {

    let provinciaId = $(this).val();
    if(provinciaId) {
        const url = `/searchCity/${provinciaId}`;
        $.getJSON(url, data => {
            let htmlOptions = '<option value="">:: Seleccione ::</option>';
            data.forEach(item => {
                htmlOptions += `<option value='${item.id}'>${item.name}</option>`;
            });

            $('#localidad_id').html(htmlOptions);
        });
    }else{
        $('#localidad_id').html(':: Debe escoger una Provincia ::');
    }
});