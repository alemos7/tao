function redondeo(numero, decimales) {
    'use strict';
    let flotante = parseFloat(numero);
    let resultado = Math.round(flotante * Math.pow(10, decimales)) / Math.pow(10, decimales);
    return resultado;
}

function recalcularTotales() {
    'use strict';
    let totalInstalacion = 0;

    $('.subtotal').each(function () {
        totalInstalacion += parseInt($(this).val());
    });

    // Dibujando en Vista
    $('#tdTotalInstalacion').html(`<h3>${totalInstalacion}</h3>`);
}

$(document).ready(function () {
    $('.btnSave').click(function () {
        let subordenid = $(this).data('subordenid');
        let valor = $(this).data('valor');
        let cantidad = $('#cantidad'+subordenid).val();
        let subtotal = redondeo(cantidad * valor,2);

        $('#subtotal'+subordenid).val(subtotal);
        $('#tdsubtotal'+subordenid).html(subtotal);

        recalcularTotales();

        $.ajax({
            type:'PUT',
            url:`/subOrdenDetalle/${subordenid}`,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data:{
                cantidad:cantidad,
                valor:valor,
                subtotal: subtotal
            },
            success:function(){
                $('#msgInfo').css('display', 'block');
                $('#msgInfo').html('Cantidad actualizada satisfactoriamente.');
            }
        });
    });
});