<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Feriado;
use Faker\Generator as Faker;

$factory->define(Feriado::class, function (Faker $faker) {

    return [
        'fecha' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
