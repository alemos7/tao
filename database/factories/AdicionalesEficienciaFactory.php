<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AdicionalesEficiencia;
use Faker\Generator as Faker;

$factory->define(AdicionalesEficiencia::class, function (Faker $faker) {

    return [
        'convenio_id' => $faker->randomDigitNotNull,
        'minimo' => $faker->randomDigitNotNull,
        'maximo' => $faker->randomDigitNotNull,
        'adicional_costo' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
