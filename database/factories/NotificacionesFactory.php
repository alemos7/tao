<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Notificaciones;
use Faker\Generator as Faker;

$factory->define(Notificaciones::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'comentario' => $faker->text,
        'fecha_desde' => $faker->date('Y-m-d H:i:s'),
        'fecha_hasta' => $faker->date('Y-m-d H:i:s'),
        'permission_role_id' => $faker->randomDigitNotNull,
        'estatus' => $faker->randomDigitNotNull
    ];
});
