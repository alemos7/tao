<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductosCategorias;
use Faker\Generator as Faker;

$factory->define(ProductosCategorias::class, function (Faker $faker) {

    return [
        'codigo' => $faker->word,
        'categoria' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
