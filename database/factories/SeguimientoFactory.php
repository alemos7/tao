<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seguimiento;
use Faker\Generator as Faker;

$factory->define(Seguimiento::class, function (Faker $faker) {

    return [
        'suborden_id' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'hora' => $faker->word,
        'fecha' => $faker->word,
        'tipo' => $faker->word,
        'observacion' => $faker->text,
        'latitud' => $faker->word,
        'longitud' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
