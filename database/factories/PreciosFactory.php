<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Precios;
use Faker\Generator as Faker;

$factory->define(Precios::class, function (Faker $faker) {

    return [
        'categoria_id' => $faker->randomDigitNotNull,
        'codigo' => $faker->word,
        'servicio' => $faker->word,
        'particulares' => $faker->randomDigitNotNull,
        'obras' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
