<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Autos;
use Faker\Generator as Faker;

$factory->define(Autos::class, function (Faker $faker) {

    return [
        'autos' => $faker->word
    ];
});
