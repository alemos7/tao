<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cajas;
use Faker\Generator as Faker;

$factory->define(Cajas::class, function (Faker $faker) {

    return [
        'id' => $faker->randomDigitNotNull,
        'caja' => $faker->word
    ];
});
