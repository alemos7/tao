<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductosTiposObras;
use Faker\Generator as Faker;

$factory->define(ProductosTiposObras::class, function (Faker $faker) {

    return [
        'producto_id' => $faker->randomDigitNotNull,
        'tipo_obra_id' => $faker->randomDigitNotNull,
        'cantidad' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
