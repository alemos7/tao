<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Convenios;
use Faker\Generator as Faker;

$factory->define(Convenios::class, function (Faker $faker) {

    return [
        'convenio' => $faker->word,
        'valor_cobrado' => $faker->randomDigitNotNull,
        'periodo' => $faker->randomDigitNotNull,
        'entrada' => $faker->word,
        'salida' => $faker->word,
        'total_horas' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
