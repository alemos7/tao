<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\IngresosEgresos;
use Faker\Generator as Faker;

$factory->define(IngresosEgresos::class, function (Faker $faker) {

    return [
        'concepto' => $faker->word,
        'monto' => $faker->randomDigitNotNull,
        'periocidad' => $faker->randomDigitNotNull,
        'tipo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
