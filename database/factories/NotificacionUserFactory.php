<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\NotificacionUser;
use Faker\Generator as Faker;

$factory->define(NotificacionUser::class, function (Faker $faker) {

    return [
        'notificaciones_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'clientes_id' => $faker->randomDigitNotNull,
        'estatus' => $faker->randomDigitNotNull
    ];
});
