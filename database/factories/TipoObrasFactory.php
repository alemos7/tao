<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TipoObras;
use Faker\Generator as Faker;

$factory->define(TipoObras::class, function (Faker $faker) {

    return [
        'tipo_obra' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
