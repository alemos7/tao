<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Solicitudes;
use Faker\Generator as Faker;

$factory->define(Solicitudes::class, function (Faker $faker) {

    return [
        'solicitud' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
