<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SubOrdenDetalle;
use Faker\Generator as Faker;

$factory->define(SubOrdenDetalle::class, function (Faker $faker) {

    return [
        'orden_id' => $faker->randomDigitNotNull,
        'producto_id' => $faker->randomDigitNotNull,
        'cantidad' => $faker->randomDigitNotNull,
        'precio' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
