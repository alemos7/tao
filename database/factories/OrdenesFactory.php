<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Ordenes;
use Faker\Generator as Faker;

$factory->define(Ordenes::class, function (Faker $faker) {

    return [
        'cliente_id' => $faker->randomDigitNotNull,
        'tipos_id' => $faker->randomDigitNotNull,
        'subtipo_id' => $faker->randomDigitNotNull,
        'consumidor_nombre' => $faker->word,
        'consumidor_tlf' => $faker->word,
        'consumidor_address' => $faker->text,
        'consumidor_provincia_id' => $faker->randomDigitNotNull,
        'consumidor_localidad_id' => $faker->randomDigitNotNull,
        'verificador_id' => $faker->randomDigitNotNull,
        'instalador_id' => $faker->randomDigitNotNull,
        'vendedor_id' => $faker->randomDigitNotNull,
        'solicitud_id' => $faker->randomDigitNotNull,
        'tipo_obra_id' => $faker->randomDigitNotNull,
        'horario' => $faker->word,
        'duracion' => $faker->word,
        'fecha_verificacion' => $faker->word,
        'fecha_inicio_obra' => $faker->word,
        'hora_inicio' => $faker->word,
        'descripcion' => $faker->text,
        'total_instalacion' => $faker->randomDigitNotNull,
        'total_servicios' => $faker->randomDigitNotNull,
        'total_orden' => $faker->randomDigitNotNull,
        'user_create_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
