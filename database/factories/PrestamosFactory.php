<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Prestamos;
use Faker\Generator as Faker;

$factory->define(Prestamos::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'monto' => $faker->randomDigitNotNull,
        'fecha' => $faker->word,
        'cuotas' => $faker->randomDigitNotNull,
        'pendientes' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
