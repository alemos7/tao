<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Barrios;
use Faker\Generator as Faker;

$factory->define(Barrios::class, function (Faker $faker) {

    return [
        'barrio' => $faker->word,
        'kilometro' => $faker->word,
        'adjunto_1' => $faker->word,
        'adjunto_2' => $faker->word,
        'adjunto_3' => $faker->word,
        'adjunto_4' => $faker->word,
        'adjunto_5' => $faker->word,
        'adjunto_6' => $faker->word,
        'adjunto_7' => $faker->word,
        'adjunto_8' => $faker->word,
        'adjunto_9' => $faker->word,
        'adjunto_10' => $faker->word,
        'adjunto_11' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
