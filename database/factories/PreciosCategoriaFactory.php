<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PreciosCategoria;
use Faker\Generator as Faker;

$factory->define(PreciosCategoria::class, function (Faker $faker) {

    return [
        'categoria' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
