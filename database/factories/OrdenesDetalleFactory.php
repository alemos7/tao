<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenesDetalle;
use Faker\Generator as Faker;

$factory->define(OrdenesDetalle::class, function (Faker $faker) {

    return [
        'orden_id' => $faker->randomDigitNotNull,
        'precio_id' => $faker->randomDigitNotNull,
        'cantidad' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
