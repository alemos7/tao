<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AdicionalesChofer;
use Faker\Generator as Faker;

$factory->define(AdicionalesChofer::class, function (Faker $faker) {

    return [
        'adicional_costo' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
