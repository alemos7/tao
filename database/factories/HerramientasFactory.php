<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Herramientas;
use Faker\Generator as Faker;

$factory->define(Herramientas::class, function (Faker $faker) {

    return [
        'herramienta' => $faker->word
    ];
});
