<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Liquidaciones;
use Faker\Generator as Faker;

$factory->define(Liquidaciones::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'fecha' => $faker->date('Y-m-d H:i:s'),
        'periodo' => $faker->word,
        'monto' => $faker->word,
        'premio' => $faker->word,
        'extra' => $faker->word
    ];
});
