<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CajasProductos;
use Faker\Generator as Faker;

$factory->define(CajasProductos::class, function (Faker $faker) {

    return [
        'caja_id' => $faker->randomDigitNotNull,
        'producto' => $faker->word
    ];
});
