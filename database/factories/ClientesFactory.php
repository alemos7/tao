<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Clientes;
use Faker\Generator as Faker;

$factory->define(Clientes::class, function (Faker $faker) {

    return [
        'cuit' => $faker->word,
        'dni' => $faker->word,
        'cliente' => $faker->word,
        'contacto' => $faker->word,
        'telefono' => $faker->word,
        'email' => $faker->word,
        'provincia_id' => $faker->randomDigitNotNull,
        'localidad_id' => $faker->randomDigitNotNull,
        'direccion' => $faker->text,
        'observacion' => $faker->text,
        'bonificacion' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
