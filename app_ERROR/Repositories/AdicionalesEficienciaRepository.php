<?php

namespace App\Repositories;

use App\Models\AdicionalesEficiencia;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AdicionalesEficienciaRepository
 * @package App\Repositories
 * @version February 28, 2020, 9:53 pm EST
 *
 * @method AdicionalesEficiencia findWithoutFail($id, $columns = ['*'])
 * @method AdicionalesEficiencia find($id, $columns = ['*'])
 * @method AdicionalesEficiencia first($columns = ['*'])
*/
class AdicionalesEficienciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'convenio_id',
        'minimo',
        'maximo',
        'adicional_costo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AdicionalesEficiencia::class;
    }
}
