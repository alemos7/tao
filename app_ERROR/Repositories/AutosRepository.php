<?php

namespace App\Repositories;

use App\Models\Autos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AutosRepository
 * @package App\Repositories
 * @version January 28, 2020, 4:06 pm EST
 *
 * @method Autos findWithoutFail($id, $columns = ['*'])
 * @method Autos find($id, $columns = ['*'])
 * @method Autos first($columns = ['*'])
*/
class AutosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'autos'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Autos::class;
    }
}
