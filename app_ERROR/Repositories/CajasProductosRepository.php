<?php

namespace App\Repositories;

use App\Models\CajasProductos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CajasProductosRepository
 * @package App\Repositories
 * @version January 28, 2020, 4:07 pm EST
 *
 * @method CajasProductos findWithoutFail($id, $columns = ['*'])
 * @method CajasProductos find($id, $columns = ['*'])
 * @method CajasProductos first($columns = ['*'])
*/
class CajasProductosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'caja_id',
        'producto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CajasProductos::class;
    }
}
