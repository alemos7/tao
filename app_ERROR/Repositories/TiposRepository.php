<?php

namespace App\Repositories;

use App\Models\Tipos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TiposRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:48 am EST
 *
 * @method Tipos findWithoutFail($id, $columns = ['*'])
 * @method Tipos find($id, $columns = ['*'])
 * @method Tipos first($columns = ['*'])
*/
class TiposRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tipos::class;
    }
}
