<?php

namespace App\Repositories;

use App\Models\Seguimiento;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SeguimientoRepository
 * @package App\Repositories
 * @version February 28, 2020, 11:42 pm EST
 *
 * @method Seguimiento findWithoutFail($id, $columns = ['*'])
 * @method Seguimiento find($id, $columns = ['*'])
 * @method Seguimiento first($columns = ['*'])
*/
class SeguimientoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'suborden_id',
        'user_id',
        'hora',
        'fecha',
        'tipo',
        'observacion',
        'latitud',
        'longitud'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Seguimiento::class;
    }
}
