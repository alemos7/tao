<?php

namespace App\Repositories;

use App\Models\Ordenes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrdenesRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:36 am EST
 *
 * @method Ordenes findWithoutFail($id, $columns = ['*'])
 * @method Ordenes find($id, $columns = ['*'])
 * @method Ordenes first($columns = ['*'])
*/
class OrdenesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'tipos_id',
        'subtipo_id',
        'consumidor_nombre',
        'consumidor_tlf',
        'consumidor_address',
        'consumidor_provincia_id',
        'consumidor_localidad_id',
        'verificador_id',
        'instalador_id',
        'vendedor_id',
        'solicitud_id',
        'tipo_obra_id',
        'horario',
        'duracion',
        'fecha_verificacion',
        'fecha_inicio_obra',
        'hora_inicio',
        'descripcion',
        'total_instalacion',
        'total_servicios',
        'total_orden',
        'user_create_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ordenes::class;
    }
}
