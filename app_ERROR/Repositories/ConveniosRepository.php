<?php

namespace App\Repositories;

use App\Models\Convenios;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ConveniosRepository
 * @package App\Repositories
 * @version February 28, 2020, 7:08 pm EST
 *
 * @method Convenios findWithoutFail($id, $columns = ['*'])
 * @method Convenios find($id, $columns = ['*'])
 * @method Convenios first($columns = ['*'])
*/
class ConveniosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'convenio',
        'valor_cobrado',
        'periodo',
        'entrada',
        'salida',
        'total_horas'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Convenios::class;
    }
}
