<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateConveniosRequest;
use App\Http\Requests\UpdateConveniosRequest;
use App\Repositories\ConveniosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ConveniosController extends AppBaseController
{
    /** @var  ConveniosRepository */
    private $conveniosRepository;

    public function __construct(ConveniosRepository $conveniosRepo)
    {
        $this->conveniosRepository = $conveniosRepo;
    }

    /**
     * Display a listing of the Convenios.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->conveniosRepository->pushCriteria(new RequestCriteria($request));
        $convenios = $this->conveniosRepository->all();

        return view('convenios.index')
            ->with('convenios', $convenios);
    }

    /**
     * Show the form for creating a new Convenios.
     *
     * @return Response
     */
    public function create()
    {
        return view('convenios.create');
    }

    /**
     * Store a newly created Convenios in storage.
     *
     * @param CreateConveniosRequest $request
     *
     * @return Response
     */
    public function store(CreateConveniosRequest $request)
    {
        $input = $request->all();

        $convenios = $this->conveniosRepository->create($input);

        Flash::success('Convenios saved successfully.');

        return redirect(route('convenios.index'));
    }

    /**
     * Display the specified Convenios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $convenios = $this->conveniosRepository->findWithoutFail($id);

        if (empty($convenios)) {
            Flash::error('Convenios not found');

            return redirect(route('convenios.index'));
        }

        return view('convenios.show')->with('convenios', $convenios);
    }

    /**
     * Show the form for editing the specified Convenios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $convenios = $this->conveniosRepository->findWithoutFail($id);

        if (empty($convenios)) {
            Flash::error('Convenios not found');

            return redirect(route('convenios.index'));
        }

        return view('convenios.edit')->with('convenios', $convenios);
    }

    /**
     * Update the specified Convenios in storage.
     *
     * @param  int              $id
     * @param UpdateConveniosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConveniosRequest $request)
    {
        $convenios = $this->conveniosRepository->findWithoutFail($id);

        if (empty($convenios)) {
            Flash::error('Convenios not found');

            return redirect(route('convenios.index'));
        }

        $convenios = $this->conveniosRepository->update($request->all(), $id);

        Flash::success('Convenios updated successfully.');

        return redirect(route('convenios.index'));
    }

    /**
     * Remove the specified Convenios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $convenios = $this->conveniosRepository->findWithoutFail($id);

        if (empty($convenios)) {
            Flash::error('Convenios not found');

            return redirect(route('convenios.index'));
        }

        $this->conveniosRepository->delete($id);

        Flash::success('Convenios deleted successfully.');

        return redirect(route('convenios.index'));
    }
}
