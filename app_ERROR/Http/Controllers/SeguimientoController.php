<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSeguimientoRequest;
use App\Http\Requests\UpdateSeguimientoRequest;
use App\Repositories\SeguimientoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Seguimiento;
use App\Models\SeguimientoFoto;

class SeguimientoController extends AppBaseController
{
    /** @var  SeguimientoRepository */
    private $seguimientoRepository;

    public function __construct(SeguimientoRepository $seguimientoRepo)
    {
        $this->seguimientoRepository = $seguimientoRepo;
    }

    /**
     * Display a listing of the Seguimiento.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->seguimientoRepository->pushCriteria(new RequestCriteria($request));
        $seguimientos = $this->seguimientoRepository->all();

        return view('seguimientos.index')
            ->with('seguimientos', $seguimientos);
    }

    public function llegadasalida(Request $request)
    {
        $user = auth()->user();
        $userid = $user->id;
        $this->seguimientoRepository->pushCriteria(new RequestCriteria($request));
        $seguimientos = $this->seguimientoRepository->where('user_id', '=', $userid)->get();

        return view('seguimientos.mihorario')
            ->with('seguimientos', $seguimientos);
    }

    /**
     * Show the form for creating a new Seguimiento.
     *
     * @return Response
     */
    public function create()
    {
        return view('seguimientos.create');
    }

    /**
     * Store a newly created Seguimiento in storage.
     *
     * @param CreateSeguimientoRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
        $input['hora'] = date("H:s");
        $input['fecha'] = date("Y-m-d");

        $seguimiento = Seguimiento::create($input);

        if ($input['tipo'] == 2) {

            $files = $request->file('foto');

            if ($request->hasFile('foto')) {
                foreach ($files as $key => $file) {
                    if (!empty($file)) {

                        $filename = $file->getClientOriginalName();
                        $foto = \File::put('files/mefui/' . $filename, \File::get($file));

                        $data = [
                            'seguimiento_id' => $seguimiento->id,
                            'foto' => $foto
                        ];

                        SeguimientoFoto::create($data);
                    }
                }
            }
        }

        Flash::success('Estatus Registrado');

        return redirect(route('home-one'));
    }

    /**
     * Show the form for editing the specified Seguimiento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seguimiento = $this->seguimientoRepository->findWithoutFail($id);

        if (empty($seguimiento)) {
            Flash::error('Seguimiento not found');

            return redirect(route('seguimientos.index'));
        }

        return view('seguimientos.edit')->with('seguimiento', $seguimiento);
    }

    /**
     * Update the specified Seguimiento in storage.
     *
     * @param  int $id
     * @param UpdateSeguimientoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeguimientoRequest $request)
    {
        $seguimiento = $this->seguimientoRepository->findWithoutFail($id);

        if (empty($seguimiento)) {
            Flash::error('Seguimiento not found');

            return redirect(route('seguimientos.index'));
        }

        $seguimiento = $this->seguimientoRepository->update($request->all(), $id);

        Flash::success('Seguimiento updated successfully.');

        return redirect(route('seguimientos.index'));
    }

    /**
     * Remove the specified Seguimiento from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seguimiento = $this->seguimientoRepository->findWithoutFail($id);

        if (empty($seguimiento)) {
            Flash::error('Seguimiento not found');

            return redirect(route('seguimientos.index'));
        }

        $this->seguimientoRepository->delete($id);

        Flash::success('Seguimiento deleted successfully.');

        return redirect(route('seguimientos.index'));
    }
}
