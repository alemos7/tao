<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecibosRequest;
use App\Http\Requests\UpdateRecibosRequest;
use App\Models\SubOrden;
use App\Repositories\RecibosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Clientes;
use Illuminate\Support\Facades\DB;

class RecibosController extends AppBaseController
{
    /** @var  RecibosRepository */
    private $recibosRepository;

    public function __construct(RecibosRepository $recibosRepo)
    {
        $this->recibosRepository = $recibosRepo;
    }

    /**
     * Display a listing of the Recibos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->recibosRepository->pushCriteria(new RequestCriteria($request));
        $recibos = $this->recibosRepository->all();

        return view('recibos.index')
            ->with('recibos', $recibos);
    }

    /**
     * Show the form for creating a new Recibos.
     *
     * @return Response
     */
    public function create()
    {
        $clientes = Clientes::orderBy('cliente')->get();
        return view('recibos.create', compact('clientes'));
    }

    /**
     * Store a newly created Recibos in storage.
     *
     * @param CreateRecibosRequest $request
     *
     * @return Response
     */
    public function store(CreateRecibosRequest $request)
    {
        $input = $request->all();
        $input['fecha'] = date("Y-m-d");
        $input['monto'] = str_replace(',','.',str_replace('.','',$input['monto']));

        $cliente = Clientes::where('id',$input['cliente_id'])->first();

        $subordenes = DB::table('ordenes_suborden AS S')
            ->select('S.id AS suborden_id', 'S.orden_id', 'S.created_at', 'S.pendiente')
            ->leftJoin('ordenes AS O', 'S.orden_id', 'O.id')
            ->where('O.cliente_id', $cliente->id)
            ->where('S.pagada', 0)
            ->where('S.solicitud_id', 6) //6 Viene siendo el id del Tipo de Solicitud ALTA
            ->where('S.created_at', '<=', DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$cliente->periodo_facturarcion.' DAY)'))
            ->orderBy('S.created_at')
            ->get();

        $data['cliente_id'] = $cliente->id;
        $data['fecha'] = date("Y-m-d");
        $data['monto'] = $input['monto'];

        $acumulador_pago = $input['monto'];

        foreach ($subordenes AS $suborden) {

            $data['suborden_id'] = $suborden->suborden_id;
            $data['total_suborden'] = ($suborden->pendiente>0)? $suborden->pendiente : SubOrden::totalSuborden($suborden->suborden_id);

            if ($acumulador_pago >= $data['total_suborden']){
                $acumulador_pago = $acumulador_pago - $data['total_suborden'];
                $data['pago_suborden'] = $data['total_suborden'];

                $so = SubOrden::where('id',$suborden->suborden_id)->first();
                $so->pagada = 1;
                $so->pendiente = 0;
                $so->save();

                //Recibo::create($data);
                $this->recibosRepository->create($data);

            }else{
                $pendiente = $data['total_suborden'] - $acumulador_pago;

                if($data['total_suborden']!=$pendiente) {
                    $data['pago_suborden'] = $acumulador_pago;

                    $so = SubOrden::where('id',$suborden->suborden_id)->first();
                    $so->pagada = 0;
                    $so->pendiente = $pendiente;
                    $so->save();

                    //Recibo::create($data);
                    $this->recibosRepository->create($data);
                }
                $acumulador_pago = 0;
            }
        }

        Flash::success('Recibos saved successfully.');

        return redirect(route('recibos.index'));
    }

    /**
     * Display the specified Recibos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recibos = $this->recibosRepository->findWithoutFail($id);

        if (empty($recibos)) {
            Flash::error('Recibos not found');

            return redirect(route('recibos.index'));
        }

        return view('recibos.show')->with('recibos', $recibos);
    }

    /**
     * Show the form for editing the specified Recibos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recibos = $this->recibosRepository->findWithoutFail($id);

        if (empty($recibos)) {
            Flash::error('Recibos not found');

            return redirect(route('recibos.index'));
        }

        return view('recibos.edit')->with('recibos', $recibos);
    }

    /**
     * Update the specified Recibos in storage.
     *
     * @param  int              $id
     * @param UpdateRecibosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecibosRequest $request)
    {
        $recibos = $this->recibosRepository->findWithoutFail($id);

        if (empty($recibos)) {
            Flash::error('Recibos not found');

            return redirect(route('recibos.index'));
        }

        $recibos = $this->recibosRepository->update($request->all(), $id);

        Flash::success('Recibos updated successfully.');

        return redirect(route('recibos.index'));
    }

    /**
     * Remove the specified Recibos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recibos = $this->recibosRepository->findWithoutFail($id);

        if (empty($recibos)) {
            Flash::error('Recibos not found');

            return redirect(route('recibos.index'));
        }

        $this->recibosRepository->delete($id);

        Flash::success('Recibos deleted successfully.');

        return redirect(route('recibos.index'));
    }
}
