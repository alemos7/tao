<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductosCategoriasRequest;
use App\Http\Requests\UpdateProductosCategoriasRequest;
use App\Repositories\ProductosCategoriasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProductosCategoriasController extends AppBaseController
{
    /** @var  ProductosCategoriasRepository */
    private $productosCategoriasRepository;

    public function __construct(ProductosCategoriasRepository $productosCategoriasRepo)
    {
        $this->productosCategoriasRepository = $productosCategoriasRepo;
    }

    /**
     * Display a listing of the ProductosCategorias.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productosCategoriasRepository->pushCriteria(new RequestCriteria($request));
        $productosCategorias = $this->productosCategoriasRepository->all();

        return view('productos_categorias.index')
            ->with('productosCategorias', $productosCategorias);
    }

    /**
     * Show the form for creating a new ProductosCategorias.
     *
     * @return Response
     */
    public function create()
    {
        return view('productos_categorias.create');
    }

    /**
     * Store a newly created ProductosCategorias in storage.
     *
     * @param CreateProductosCategoriasRequest $request
     *
     * @return Response
     */
    public function store(CreateProductosCategoriasRequest $request)
    {
        $input = $request->all();

        $productosCategorias = $this->productosCategoriasRepository->create($input);

        Flash::success('Productos Categorias guardado Satisfactiamente.');

        return redirect(route('productosCategorias.index'));
    }

    /**
     * Display the specified ProductosCategorias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productosCategorias = $this->productosCategoriasRepository->findWithoutFail($id);

        if (empty($productosCategorias)) {
            Flash::error('Productos Categorias not found');

            return redirect(route('productosCategorias.index'));
        }

        return view('productos_categorias.show')->with('productosCategorias', $productosCategorias);
    }

    /**
     * Show the form for editing the specified ProductosCategorias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productosCategorias = $this->productosCategoriasRepository->findWithoutFail($id);

        if (empty($productosCategorias)) {
            Flash::error('Productos Categorias not found');

            return redirect(route('productosCategorias.index'));
        }

        return view('productos_categorias.edit')->with('productosCategorias', $productosCategorias);
    }

    /**
     * Update the specified ProductosCategorias in storage.
     *
     * @param  int              $id
     * @param UpdateProductosCategoriasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductosCategoriasRequest $request)
    {
        $productosCategorias = $this->productosCategoriasRepository->findWithoutFail($id);

        if (empty($productosCategorias)) {
            Flash::error('Productos Categorias not found');

            return redirect(route('productosCategorias.index'));
        }

        $productosCategorias = $this->productosCategoriasRepository->update($request->all(), $id);

        Flash::success('Productos Categorias guardado Satisfactiamente.');

        return redirect(route('productosCategorias.index'));
    }

    /**
     * Remove the specified ProductosCategorias from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productosCategorias = $this->productosCategoriasRepository->findWithoutFail($id);

        if (empty($productosCategorias)) {
            Flash::error('Productos Categorias not found');

            return redirect(route('productosCategorias.index'));
        }

        $this->productosCategoriasRepository->delete($id);

        Flash::success('Productos Categorias eliminado satisfactoriamente.');

        return redirect(route('productosCategorias.index'));
    }
}
