<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAutosRequest;
use App\Http\Requests\UpdateAutosRequest;
use App\Repositories\AutosRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LiquidacionController extends AppBaseController
{
    /** @var  AutosRepository */
    private $autosRepository;

    /**
     * Display a listing of the Autos.
     *
     * @param Request $request
     * @return Response
     */
    public function liquidar()
    {
        $usuarios = User::get();
        return view('rrhh.liquidar', compact(
          'usuarios'
        ));
    }

}
