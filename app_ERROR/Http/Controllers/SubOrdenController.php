<?php

namespace App\Http\Controllers;

use App\Models\Cajas;
use App\Models\Estados;
use App\Repositories\OrdenesDetallesRepository;
use Illuminate\Http\Request;

use App\Models\SubOrdenDetalle;
use Flash;
use App\Models\SubOrden;
use App\Models\Ordenes;
use App\Models\Productos;
use App\Models\Tipos;
use App\Models\ProductosCategorias;
use App\User;
use Carbon\Carbon;
use App\Models\Solicitudes;
use Illuminate\Support\Facades\DB;
use App\Models\Clientes;

class SubOrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vencimientos()
    {
        $subordenes = DB::table('ordenes_suborden AS S')
            ->select('S.created_at AS fecha', 'S.orden_id', 'S.id AS suborden_id', 'S.pendiente', 'C.cliente', 'C.periodo_facturarcion',
                    DB::raw('DATE_ADD(S.created_at, INTERVAL C.periodo_facturarcion DAY) AS fecha_vencimiento'),
                    DB::raw('TIMESTAMPDIFF(DAY,DATE_ADD(S.created_at, INTERVAL C.periodo_facturarcion DAY),CURDATE()) AS dias_vencidos'))
            ->leftJoin('ordenes AS O', 'S.orden_id', 'O.id')
            ->leftJoin('clientes AS C', 'O.cliente_id', 'C.id')
            ->where('S.solicitud_id', 6)//6 Viene siendo el id del Tipo de Solicitud ALTA
//            ->where('S.created_at', '<=', DB::raw('DATE_SUB(CURDATE(), INTERVAL C.periodo_facturarcion DAY)')) #Surordenes Vencidas
            ->where('S.pagada', 0)
            ->orderBy('S.created_at')
            ->get();

        /*
         *  foreach ($subordenes AS $suborden) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . Carbon::parse($suborden->fecha)->format('d/m/Y') . '</td>';
                $tabla .= '<td>' . str_pad($suborden->orden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                $tabla .= '<td>' . str_pad($suborden->suborden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                $tabla .= '<td align="right">' . number_format(($suborden->pendiente) ? $suborden->pendiente : SubOrden::totalSuborden($suborden->suborden_id), 2, ',', '.') . '</td>';
                $tabla .= '</tr>';

                $totalOrdenes += ($suborden->pendiente) ? $suborden->pendiente : $this->totalSuborden($suborden->suborden_id);
            }*/
        return view('subOrden.vencimientos', compact('subordenes'));

    }

    /*Damian*/

    public function mysuborders()
    {
        $user = auth()->user();
        $userid = $user->id;

        $subordenes = DB::select(
           DB::raw("
           SELECT
            ordenes_suborden.tecnico_id1,
            ordenes_suborden.tecnico_id2,
            ordenes_suborden.tecnico_id3,
            ordenes_suborden.tecnico_id4,
            ordenes.cliente_id,
            tipos.tipo,
            ordenes_suborden.fecha_inicio,
            ordenes_suborden.hora_inicio,
            clientes.cliente,
            ordenes_suborden.id AS suborderid,
            ordenes.id AS orderid,
            clientes.contacto,
            clientes.telefono,
            clientes.email,
            clientes.direccion,
            geo_cities.`name` AS localidad,
            geo_states.`name` AS provincia,
            ordenes.consumidor_nombre,
            ordenes.consumidor_tlf
            FROM
            ordenes_suborden
            LEFT JOIN ordenes ON ordenes_suborden.orden_id = ordenes.id
            LEFT JOIN tipos ON ordenes_suborden.tipo_id = tipos.id
            LEFT JOIN clientes ON ordenes.cliente_id = clientes.id
            LEFT JOIN geo_cities ON clientes.localidad_id = geo_cities.id
            LEFT JOIN geo_states ON clientes.provincia_id = geo_states.id
            WHERE
            (ordenes_suborden.tecnico_id1 = $userid OR
            ordenes_suborden.tecnico_id2 = $userid OR
            ordenes_suborden.tecnico_id3 = $userid OR
            ordenes_suborden.tecnico_id4 = $userid) AND
            ordenes_suborden.solicitud_id = 6 AND
            (ordenes_suborden.estado_id = 2 OR
            ordenes_suborden.estado_id = 5)
           "));
        return view('subOrden.mysuborders', compact('subordenes'));
    }

    public function reclamos()
    {

        $subordenes = DB::select(
           DB::raw("
           SELECT
            ordenes.cliente_id,
            clientes.cliente,
            ordenes_suborden.id,
            ordenes_suborden.created_at,
            ordenes_suborden.observaciones
            FROM
            ordenes_suborden
            INNER JOIN ordenes ON ordenes_suborden.orden_id = ordenes.id
            INNER JOIN clientes ON ordenes.cliente_id = clientes.id
            WHERE
            ordenes_suborden.tipo_id = 4
           "));


        return view('subOrden.reclamos', compact('subordenes'));
    }

    public function listar($orden_id)
    {
        $subordenes = SubOrden::where('orden_id', $orden_id)
            ->get();

        $total_orden = SubOrden::where('orden_id', $orden_id)
            ->leftJoin('ordenes_suborden_detalle', 'ordenes_suborden.id', '=', 'ordenes_suborden_detalle.suborden_id')
            ->sum('subtotal');
        $tipos = Tipos::orderBy('tipo', 'asc')->get();

        return view('subOrden.index', compact('orden_id', 'subordenes', 'total_orden', 'tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tipo_id = $request->tipos_id;

        switch ($tipo_id) {
            case 1:
                $nombre_tecnico = 'Instalador';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 4)->get(); //Instalador
                break;
            case 2:
                $nombre_tecnico = 'Medidor';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 6)->get(); //Medidor
                break;
            case 3:
                $nombre_tecnico = 'Técnico';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 7)->get(); //Verificador
                break;
            case 4:
                $nombre_tecnico = '';
                $tecnicos = array();
                break;
            case 5:
                $nombre_tecnico = 'Verificador';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 5)->get(); //Verificador
                break;
        }

        $choferes = User::where('registro','Si')->get();
        $cajas = Cajas::orderBy('caja')->get();
        $estados = Estados::all();
        $orden = Ordenes::where('id', $request->idOrden)->first();
        $solicitudes = Solicitudes::orderBy('solicitud', 'asc')->get();
        $productos_categorias = ProductosCategorias::orderBy('categoria')->get();

        return view('subOrden.create', compact('tipo_id', 'solicitudes', 'orden', 'nombre_tecnico', 'productos_categorias', 'tecnicos', 'estados', 'cajas', 'choferes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if (isset($input['fecha_inicio'])) {
            $fecha_inicioDT = Carbon::createFromFormat('d/m/Y', $input['fecha_inicio']);
            $input['fecha_inicio'] = $fecha_inicioDT->format('Y-m-d');
        }

        $suborden = SubOrden::create($input);

        foreach ($input['items'] AS $json) {

            $item = json_decode($json);

            $data['suborden_id'] = $suborden->id;
            $data['producto_id'] = $item->productoId;
            $data['cantidad'] = $item->cantidad;
            $data['valor'] = $item->valor;
            $data['subtotal'] = $item->subtotal;

            if ($data['suborden_id'] != '' && $data['producto_id'] != '' && $data['cantidad'] > 0 && $data['valor'] > 0 && $data['subtotal'] > 0) {
                SubOrdenDetalle::create($data);
            }
        }


        Flash::success('Suborden agregada satisfactoriamente.');

        return redirect(route('ordenes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(subOrden $subOrden)
    {

      $user = auth()->user();
      $userid = $user->id;

      $roluser = DB::select(
         DB::raw("
         SELECT
         role_user.role_id
         FROM
         role_user
         WHERE
        role_user.user_id = $userid
         "));


      $tipo_id = $subOrden->tipo_id;

      switch ($tipo_id) {
          case 1:
              $nombre_tecnico = 'Instalador';
              $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 4)->get(); //Instalador
              break;
          case 2:
              $nombre_tecnico = 'Medidor';
              $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 6)->get(); //Medidor
              break;
          case 3:
              $nombre_tecnico = 'Técnico';
              $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 7)->get(); //Verificador
              break;
          case 4:
              $nombre_tecnico = '';
              $tecnicos = array();
              break;
          case 5:
              $nombre_tecnico = 'Verificador';
              $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 5)->get(); //Verificador
              break;
      }

      $choferes = User::where('registro','Si')->get();
      $cajas = Cajas::orderBy('caja')->get();
      $estados = Estados::all();
      $orden = Ordenes::where('id', $subOrden->orden_id)->first();
      $solicitudes = Solicitudes::orderBy('solicitud', 'asc')->get();
      $productos_categorias = ProductosCategorias::orderBy('categoria')->get();

      return view('subOrden.show', compact('subOrden', 'tipo_id', 'solicitudes', 'orden', 'nombre_tecnico', 'productos_categorias', 'tecnicos', 'cajas', 'estados', 'choferes','roluser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(subOrden $subOrden)
    {
        $tipo_id = $subOrden->tipo_id;

        switch ($tipo_id) {
            case 1:
                $nombre_tecnico = 'Instalador';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 4)->get(); //Instalador
                break;
            case 2:
                $nombre_tecnico = 'Medidor';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 6)->get(); //Medidor
                break;
            case 3:
                $nombre_tecnico = 'Técnico';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 7)->get(); //Verificador
                break;
            case 4:
                $nombre_tecnico = '';
                $tecnicos = array();
                break;
            case 5:
                $nombre_tecnico = 'Verificador';
                $tecnicos = User::select('users.id', 'nombre')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 5)->get(); //Verificador
                break;
        }

        $choferes = User::where('registro','Si')->get();
        $cajas = Cajas::orderBy('caja')->get();
        $estados = Estados::all();
        $orden = Ordenes::where('id', $subOrden->orden_id)->first();
        $solicitudes = Solicitudes::orderBy('solicitud', 'asc')->get();
        $productos_categorias = ProductosCategorias::orderBy('categoria')->get();

        return view('subOrden.edit', compact('subOrden', 'tipo_id', 'solicitudes', 'orden', 'nombre_tecnico', 'productos_categorias', 'tecnicos', 'cajas', 'estados', 'choferes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $suborden = SubOrden::where('id',$id)->first();
        $input = $request->all();

        if (isset($input['fecha_inicio'])) {
            $fecha_inicioDT = Carbon::createFromFormat('d/m/Y', $input['fecha_inicio']);
            $input['fecha_inicio'] = $fecha_inicioDT->format('Y-m-d');
        }

        $suborden->fill($input);
        $suborden->save();

        SubOrdenDetalle::where('suborden_id', $id)->delete();

        foreach ($input['items'] AS $json) {

            $item = json_decode($json);

            $data['suborden_id'] = $suborden->id;
            $data['producto_id'] = $item->productoId;
            $data['cantidad'] = $item->cantidad;
            $data['valor'] = $item->valor;
            $data['subtotal'] = $item->subtotal;

            if ($data['suborden_id'] != '' && $data['producto_id'] != '' && $data['cantidad'] > 0 && $data['valor'] > 0 && $data['subtotal'] > 0) {
                SubOrdenDetalle::create($data);
            }
        }

        Flash::success('Suborden agregada satisfactoriamente.');

        return redirect(route('ordenes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subOrden = SubOrden::where('id', $id)->first();
        $orden_id = $subOrden->orden_id;

        if (empty($subOrden)) {
            Flash::error('SubOrden not found');

            return redirect(route('subOrden.index'));
        }

        $subOrden->delete($id);

        Flash::success('SubOrden Eliminada.');

        return redirect(route('subOrden.listar', $orden_id));
    }

    public function searchXfechaXcliente(Clientes $cliente)
    {

        if ($cliente->periodo_facturarcion) {
            $subordenes = DB::table('ordenes_suborden AS S')
                ->select('S.created_at AS fecha', 'S.orden_id', 'S.id AS suborden_id', 'S.pendiente')
                ->leftJoin('ordenes AS O', 'S.orden_id', 'O.id')
                ->where('O.cliente_id', $cliente->id)
                ->where('S.solicitud_id', 6)//6 Viene siendo el id del Tipo de Solicitud ALTA
                ->where('S.created_at', '<=', DB::raw('DATE_SUB(CURDATE(), INTERVAL ' . $cliente->periodo_facturarcion . ' DAY)'))
                ->where('S.pagada', 0)
                ->orderBy('S.created_at')
                ->get();

            if (count($subordenes) > 0) {

                $tabla = '';
                $totalOrdenes = 0;
                foreach ($subordenes AS $suborden) {
                    $tabla .= '<tr>';
                    $tabla .= '<td>' . Carbon::parse($suborden->fecha)->format('d/m/Y') . '</td>';
                    $tabla .= '<td>' . str_pad($suborden->orden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                    $tabla .= '<td>' . str_pad($suborden->suborden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                    $tabla .= '<td align="right">' . number_format(($suborden->pendiente) ? $suborden->pendiente : SubOrden::totalSuborden($suborden->suborden_id), 2, ',', '.') . '</td>';
                    $tabla .= '</tr>';

                    $totalOrdenes += ($suborden->pendiente) ? $suborden->pendiente : $this->totalSuborden($suborden->suborden_id);
                }

                $datos['tabla'] = $tabla;
                $datos['totalOrdenes'] = number_format($totalOrdenes, 2, ',', '.');
                $datos['sinperiodo'] = 0;
            } else {
                $datos['tabla'] = '';
                $datos['totalOrdenes'] = 0;
                $datos['sinperiodo'] = 0;
            }
        } else {
            $datos['sinperiodo'] = 1;
        }

        return $datos;
    }

    public function searchXfechaXclienteSinVencer(Clientes $cliente)
    {

        if ($cliente->periodo_facturarcion) {
            $subordenes = DB::table('ordenes_suborden AS S')
                ->select('S.created_at AS fecha', 'S.orden_id', 'S.id AS suborden_id', 'S.pendiente')
                ->leftJoin('ordenes AS O', 'S.orden_id', 'O.id')
                ->where('O.cliente_id', $cliente->id)
                ->where('S.solicitud_id', 6)//6 Viene siendo el id del Tipo de Solicitud ALTA
                ->where('S.pagada', 0)
                ->orderBy('S.created_at')
                ->get();

            if (count($subordenes) > 0) {

                $tabla = '';
                $totalOrdenes = 0;
                foreach ($subordenes AS $suborden) {
                    $tabla .= '<tr>';
                    $tabla .= '<td>' . Carbon::parse($suborden->fecha)->format('d/m/Y') . '</td>';
                    $tabla .= '<td>' . str_pad($suborden->orden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                    $tabla .= '<td>' . str_pad($suborden->suborden_id, 6, '0', STR_PAD_LEFT) . '</td>';
                    $tabla .= '<td align="right">' . number_format(($suborden->pendiente) ? $suborden->pendiente : SubOrden::totalSuborden($suborden->suborden_id), 2, ',', '.') . '</td>';
                    $tabla .= '</tr>';

                    $totalOrdenes += ($suborden->pendiente) ? $suborden->pendiente : $this->totalSuborden($suborden->suborden_id);
                }

                $datos['tabla'] = $tabla;
                $datos['totalOrdenes'] = number_format($totalOrdenes, 2, ',', '.');
                $datos['sinperiodo'] = 0;
            } else {
                $datos['tabla'] = '';
                $datos['totalOrdenes'] = 0;
                $datos['sinperiodo'] = 0;
            }
        } else {
            $datos['sinperiodo'] = 1;
        }

        return $datos;
    }

    public function totalSuborden($suborden_id)
    {
        $res = SubOrdenDetalle::select(DB::raw('SUM(subtotal) as totalSuborden'))
            ->where('suborden_id', $suborden_id)
            ->groupBy('suborden_id')
            ->first();

        return $res->totalSuborden;
    }

/*damian*/
    public function miagenda()
    {
        $meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        return view('subOrden.miagenda', compact('meses'));
    }

    public function searchMyAgenda($selectMes, $tipoId)
    {
      $user = auth()->user();
      $userid = $user->id;

            $subOrdenes = SubOrden::whereMonth('fecha_inicio', $selectMes)->where('tecnico_id1', $userid)->orwhere('tecnico_id2', $userid)->orwhere('tecnico_id3', $userid)->orwhere('tecnico_id4', $userid)->get();

        $month = date($selectMes);
        $year = date("Y");
        $diaActual = date("j");
        $diaSemana = date("w", mktime(0, 0, 0, $month, 1, $year)) + 7;
        $ultimoDiaMes = date("d", (mktime(0, 0, 0, $month + 1, 1, $year) - 1));

        $agenda = '<table id="calendar" class="table">
                                    <thead>
                                    <tr>
                                        <th width="14.28%">Lun</th>
                                        <th width="14.28%">Mar</th>
                                        <th width="14.28%">Mie</th>
                                        <th width="14.28%">Jue</th>
                                        <th width="14.28%">Vie</th>
                                        <th width="14.28%">Sab</th>
                                        <th width="14.28%">Dom</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>';

        $last_cell = $diaSemana + $ultimoDiaMes;

        for ($i = 1; $i <= 42; $i++) {

            if ($i == $diaSemana) {
                $day = 1;
            }

            if ($i < $diaSemana || $i >= $last_cell) {
                $agenda .= "<td>&nbsp;</td>";
            } else {
                if ($day == $diaActual) {
                    $agenda .= "<td class='hoy'>";
                } else {
                    $agenda .= "<td>";
                }

                $agenda .= str_pad($day, 2, '0', STR_PAD_LEFT);
                $cant = 0;
                foreach ($subOrdenes AS $suborden){
                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                        $cant++;
                    }
                }

                switch ($cant) {
                    case 0:
                        $agenda .= "&nbsp;";
                        break;
                    case 1:
                        foreach ($subOrdenes AS $suborden){

                            if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){

                                $agenda .= '<p><span class="mytooltip tooltip-effect-1">
                                <span class="tooltip-item2 bg-'.$suborden->tipos->color.' p-2 text-center rounded"><a class="font-weight-bold text-white" href="/subOrden/'.$suborden->id.'/">+'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</a></span>
                                <span class="tooltip-content4">';
                                foreach ($subOrdenes AS $suborden){
                                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                                        $agenda .= '<span class="text-'.$suborden->tipos->color.'">'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</span><br>';
                                        if(isset($suborden->tecnicos1->nombre)){
                                            $agenda .= $suborden->tecnicos1->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos2->nombre)){
                                            $agenda .= $suborden->tecnicos2->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos3->nombre)){
                                            $agenda .= $suborden->tecnicos3->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos4->nombre)){
                                            $agenda .= $suborden->tecnicos4->nombre.'</br>';
                                        }
                                    }
                                }
                                $agenda .= '</span>
                            </span></p>';
                            }
                        }
                        break;
                    default:
                        $agenda .= '<p><span class="mytooltip tooltip-effect-1">
                                <span class="tooltip-item2">+'.$cant.' Subordenes</span>
                                <span class="tooltip-content4">';
                                foreach ($subOrdenes AS $suborden){
                                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                                        $agenda .= '<span class="text-'.$suborden->tipos->color.'">'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</span><br>';
                                        if(isset($suborden->tecnicos1->nombre)){
                                            $agenda .= $suborden->tecnicos1->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos2->nombre)){
                                            $agenda .= $suborden->tecnicos2->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos3->nombre)){
                                            $agenda .= $suborden->tecnicos3->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos4->nombre)){
                                            $agenda .= $suborden->tecnicos4->nombre.'</br>';
                                        }
                                    }
                                }
                                $agenda .= '</span>
                            </span></p>';

                        break;
                }

                $agenda .= "</td>";
                $day++;
            }


            if ($i % 7 == 0) {

                $agenda .= "</tr><tr>\n";
            }
        }

        $agenda .= '</tr>
                    </tbody>
                    </table>';

        return $agenda;
    }

// DAMIAN
    public function agenda()
    {
        $meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        return view('subOrden.agenda', compact('meses'));
    }

    public function searchAgenda($selectMes, $tipoId)
    {
        if($tipoId){
            $subOrdenes = SubOrden::whereMonth('fecha_inicio', $selectMes)
                                        ->where('tipo_id', $tipoId)
                                        ->get();
        }else{
            $subOrdenes = SubOrden::whereMonth('fecha_inicio', $selectMes)->get();
        }

        $month = date($selectMes);
        $year = date("Y");
        $diaActual = date("j");
        $diaSemana = date("w", mktime(0, 0, 0, $month, 1, $year)) + 7;
        $ultimoDiaMes = date("d", (mktime(0, 0, 0, $month + 1, 1, $year) - 1));

        $agenda = '<table id="calendar" class="table">
                                    <thead>
                                    <tr>
                                        <th width="14.28%">Lun</th>
                                        <th width="14.28%">Mar</th>
                                        <th width="14.28%">Mie</th>
                                        <th width="14.28%">Jue</th>
                                        <th width="14.28%">Vie</th>
                                        <th width="14.28%">Sab</th>
                                        <th width="14.28%">Dom</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>';

        $last_cell = $diaSemana + $ultimoDiaMes;

        for ($i = 1; $i <= 42; $i++) {

            if ($i == $diaSemana) {
                $day = 1;
            }

            if ($i < $diaSemana || $i >= $last_cell) {
                $agenda .= "<td>&nbsp;</td>";
            } else {
                if ($day == $diaActual) {
                    $agenda .= "<td class='hoy'>";
                } else {
                    $agenda .= "<td>";
                }

                $agenda .= str_pad($day, 2, '0', STR_PAD_LEFT);
                $cant = 0;
                foreach ($subOrdenes AS $suborden){
                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                        $cant++;
                    }
                }

                switch ($cant) {
                    case 0:
                        $agenda .= "&nbsp;";
                        break;
                    case 1:
                        foreach ($subOrdenes AS $suborden){

                            if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){

                                $agenda .= '<p><span class="mytooltip tooltip-effect-1">
                                <span class="tooltip-item2 bg-'.$suborden->tipos->color.' p-2 text-center rounded"><a class="font-weight-bold text-white" href="/subOrden/'.$suborden->id.'/edit">+'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</a></span>
                                <span class="tooltip-content4">';
                                foreach ($subOrdenes AS $suborden){
                                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                                        $agenda .= '<span class="text-'.$suborden->tipos->color.'">'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</span><br>';
                                        if(isset($suborden->tecnicos1->nombre)){
                                            $agenda .= $suborden->tecnicos1->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos2->nombre)){
                                            $agenda .= $suborden->tecnicos2->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos3->nombre)){
                                            $agenda .= $suborden->tecnicos3->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos4->nombre)){
                                            $agenda .= $suborden->tecnicos4->nombre.'</br>';
                                        }
                                    }
                                }
                                $agenda .= '</span>
                            </span></p>';
                            }
                        }
                        break;
                    default:
                        $agenda .= '<p><span class="mytooltip tooltip-effect-1">
                                <span class="tooltip-item2">+'.$cant.' Subordenes</span>
                                <span class="tooltip-content4">';
                                foreach ($subOrdenes AS $suborden){
                                    if($suborden->fecha_inicio==date('Y').'-'.str_pad($selectMes, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT)){
                                        $agenda .= '<span class="text-'.$suborden->tipos->color.'">'.$suborden->hora_inicio.' '.$suborden->tipos->tipo.'</span><br>';
                                        if(isset($suborden->tecnicos1->nombre)){
                                            $agenda .= $suborden->tecnicos1->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos2->nombre)){
                                            $agenda .= $suborden->tecnicos2->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos3->nombre)){
                                            $agenda .= $suborden->tecnicos3->nombre.'</br>';
                                        }
                                        if(isset($suborden->tecnicos4->nombre)){
                                            $agenda .= $suborden->tecnicos4->nombre.'</br>';
                                        }
                                    }
                                }
                                $agenda .= '</span>
                            </span></p>';

                        break;
                }

                $agenda .= "</td>";
                $day++;
            }


            if ($i % 7 == 0) {

                $agenda .= "</tr><tr>\n";
            }
        }

        $agenda .= '</tr>
                    </tbody>
                    </table>';

        return $agenda;
    }





    public function productividad()
    {

      $user = auth()->user();
      $userid = $user->id;
      $today = date("Y-m-d");

        $productividad = DB::select(
           DB::raw("
           SELECT
           Sum(ordenes_suborden_detalle.cantidad) as cantidad,
            YEAR(fecha_inicio) as year,
            MONTH(fecha_inicio) as month
            FROM
            ordenes_suborden
            INNER JOIN ordenes_suborden_detalle ON ordenes_suborden_detalle.suborden_id = ordenes_suborden.id
            WHERE
            (fecha_inicio <= '$today') AND
            ordenes_suborden.tecnico_id1 = $userid OR
            ordenes_suborden.tecnico_id2 = $userid OR
            ordenes_suborden.tecnico_id3 = $userid OR
            ordenes_suborden.tecnico_id4 = $userid
            GROUP BY
            YEAR(fecha_inicio),
            MONTH(fecha_inicio)
            ;
           "));

           $productividaddetail = DB::select(
              DB::raw("
              SELECT
              Sum(ordenes_suborden_detalle.cantidad) AS cantidad,
              ordenes_suborden.id,
              ordenes_suborden.fecha_inicio
              FROM
              ordenes_suborden_detalle
              INNER JOIN ordenes_suborden ON ordenes_suborden_detalle.suborden_id = ordenes_suborden.id
               WHERE
               (fecha_inicio <= '$today') AND
               ordenes_suborden.tecnico_id1 = $userid OR
               ordenes_suborden.tecnico_id2 = $userid OR
               ordenes_suborden.tecnico_id3 = $userid OR
               ordenes_suborden.tecnico_id4 = $userid
               GROUP BY
               ordenes_suborden.id,
               ordenes_suborden.fecha_inicio
               ;
              "));


        return view('subOrden.productividad', compact('productividad','productividaddetail'));
    }


















}
