<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCajasRequest;
use App\Http\Requests\UpdateCajasRequest;
use App\Models\CajasProductos;
use App\Models\Herramientas;
use App\Repositories\CajasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CajasController extends AppBaseController
{
    /** @var  CajasRepository */
    private $cajasRepository;

    public function __construct(CajasRepository $cajasRepo)
    {
        $this->cajasRepository = $cajasRepo;
    }

    /**
     * Display a listing of the Cajas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cajasRepository->pushCriteria(new RequestCriteria($request));
        $cajas = $this->cajasRepository->all();

        return view('cajas.index')
            ->with('cajas', $cajas);
    }

    /**
     * Show the form for creating a new Cajas.
     *
     * @return Response
     */
    public function create()
    {
        $herramientas = Herramientas::orderBy('herramienta')->get();
        return view('cajas.create', compact('herramientas'));
    }

    /**
     * Store a newly created Cajas in storage.
     *
     * @param CreateCajasRequest $request
     *
     * @return Response
     */
    public function store(CreateCajasRequest $request)
    {
        $input = $request->all();

        $cajas = $this->cajasRepository->create($input);

        $productos = $input['items'];

        foreach ($productos AS $json){
            $producto = json_decode($json);

            $data = [
                "caja_id" => $cajas->id,
                "producto_id" => $producto->productoId,
                "cantidad" => $producto->cantidad
            ];
            CajasProductos::create($data);
        }

        Flash::success('Cajas saved successfully.');

        return redirect(route('cajas.index'));
    }

    /**
     * Display the specified Cajas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cajas = $this->cajasRepository->findWithoutFail($id);
        $herramientas = CajasProductos::where('caja_id',$id)->get();

        if (empty($cajas)) {
            Flash::error('Cajas not found');

            return redirect(route('cajas.index'));
        }

        return view('cajas.show', compact('herramientas', 'cajas'));
    }

    /**
     * Show the form for editing the specified Cajas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $herramientas = Herramientas::orderBy('herramienta')->get();
        $productosCaja = CajasProductos::where('caja_id',$id)->get();
        $cajas = $this->cajasRepository->findWithoutFail($id);

        if (empty($cajas)) {
            Flash::error('Cajas not found');

            return redirect(route('cajas.index'));
        }

        return view('cajas.edit', compact('cajas','herramientas', 'productosCaja'));
    }

    /**
     * Update the specified Cajas in storage.
     *
     * @param  int              $id
     * @param UpdateCajasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCajasRequest $request)
    {
        $cajas = $this->cajasRepository->findWithoutFail($id);

        if (empty($cajas)) {
            Flash::error('Cajas not found');

            return redirect(route('cajas.index'));
        }

        $cajas = $this->cajasRepository->update($request->all(), $id);

        $productos = $request['items'];

        CajasProductos::where('caja_id',$id)->forceDelete();

        foreach ($productos AS $json){
            $producto = json_decode($json);

            $data = [
                "caja_id" => $cajas->id,
                "producto_id" => $producto->productoId,
                "cantidad" => $producto->cantidad
            ];
            CajasProductos::create($data);
        }

        Flash::success('Cajas updated successfully.');

        return redirect(route('cajas.index'));
    }

    /**
     * Remove the specified Cajas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cajas = $this->cajasRepository->findWithoutFail($id);

        if (empty($cajas)) {
            Flash::error('Cajas not found');

            return redirect(route('cajas.index'));
        }

        $this->cajasRepository->delete($id);

        Flash::success('Cajas deleted successfully.');

        return redirect(route('cajas.index'));
    }
}
