<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdicionalesEficienciaRequest;
use App\Http\Requests\UpdateAdicionalesEficienciaRequest;
use App\Models\AdicionalesEficiencia;
use App\Models\Convenios;
use App\Repositories\AdicionalesEficienciaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AdicionalesEficienciaController extends AppBaseController
{
    /** @var  AdicionalesEficienciaRepository */
    private $adicionalesEficienciaRepository;

    public function __construct(AdicionalesEficienciaRepository $adicionalesEficienciaRepo)
    {
        $this->adicionalesEficienciaRepository = $adicionalesEficienciaRepo;
    }


    public function adicionalesEficiencia()
    {
        $adicionales = AdicionalesEficiencia::all();
        $convenios = Convenios::orderBy('convenio')->get();

        return view('adicionales_eficiencias.edit', compact('adicionales', 'convenios'));
    }

    /**
     * Update the specified AdicionalesEficiencia in storage.
     *
     * @param  int              $id
     * @param UpdateAdicionalesEficienciaRequest $request
     *
     * @return Response
     */
    public function guardarAdicionales(Request $request)
    {

        $datos = $request->input();

        foreach ($datos['convenio_id'] AS $i=> $convenio_id){

            $data = [
                    'convenio_id'=>$convenio_id,
                    'minimo'=>$datos['minimo'][$i],
                    'maximo'=>$datos['maximo'][$i],
                    'adicional_costo'=>$datos['adicional_costo'][$i],
            ];

            AdicionalesEficiencia::where('id', $datos['id'][$i])->update($data);

        }


        Flash::success('Adicionales Eficiencia updated successfully.');

        return redirect(route('adicionalesEficiencia'));
    }

}
