<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductosRequest;
use App\Http\Requests\UpdateProductosRequest;
use App\Models\Productos;
use App\Models\ProductosCategorias;
use App\Models\ProductosTiposObras;
use App\Models\SubOrdenDetalle;
use App\Models\TipoObras;
use App\Repositories\ProductosRepository;
use App\Http\Controllers\AppBaseController;
use Doctrine\DBAL\Events;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProductosController extends AppBaseController
{
    /** @var  ProductosRepository */
    private $productosRepository;

    public function __construct(ProductosRepository $productosRepo)
    {
        $this->productosRepository = $productosRepo;
    }

    /**
     * Display a listing of the Productos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productosRepository->pushCriteria(new RequestCriteria($request));
        $productos = $this->productosRepository->all();
        $tiposObras = TipoObras::orderBy('tipo_obra')->get();

        return view('productos.index', compact('productos', 'tiposObras'));

    }

    /**
     * Show the form for creating a new Productos.
     *
     * @return Response
     */
    public function create()
    {
        $categoriasProductos = ProductosCategorias::orderBy('categoria')->get();

        return view('productos.create', compact('categoriasProductos'));
    }

    /**
     * Store a newly created Productos in storage.
     *
     * @param CreateProductosRequest $request
     *
     * @return Response
     */
    public function store(CreateProductosRequest $request)
    {
        $input = $request->all();

        $productos = $this->productosRepository->create($input);

        Flash::success('Producto guardado satisfactoriamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Display the specified Productos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productos = $this->productosRepository->findWithoutFail($id);

        if (empty($productos)) {
            Flash::error('Productos not found');

            return redirect(route('productos.index'));
        }

        return view('productos.show')->with('productos', $productos);
    }

    /**
     * Show the form for editing the specified Productos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoriasProductos = ProductosCategorias::orderBy('categoria')->get();
        $productos = $this->productosRepository->findWithoutFail($id);

        if (empty($productos)) {
            Flash::error('Productos not found');

            return redirect(route('productos.index'));
        }

        return view('productos.edit', compact('productos', 'categoriasProductos'));
    }

    /**
     * Update the specified Productos in storage.
     *
     * @param  int $id
     * @param UpdateProductosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductosRequest $request)
    {

        $productos = $this->productosRepository->findWithoutFail($id);

        if (empty($productos)) {
            Flash::error('Productos not found');

            return redirect(route('productos.index'));
        }

        $productos = $this->productosRepository->update($request->all(), $id);

        ProductosTiposObras::where('producto_id',$productos->id)->forceDelete();

        Flash::success('Productos guardado satisfactoriamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Remove the specified Productos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productos = $this->productosRepository->findWithoutFail($id);

        if (empty($productos)) {
            Flash::error('Productos not found');

            return redirect(route('productos.index'));
        }

        $this->productosRepository->delete($id);

        Flash::success('Productos eliminado satisfactoriamente.');

        return redirect(route('productos.index'));
    }

    public function searchProductosXCategoria(Request $request)
    {
        $categoriaId = $request->categoriaId;
        $coeficienteConstante = $request->coeficienteConstante;
        $subOrdenId = $request->subOrdenId;

        $productos = Productos::where('categoria_id', $categoriaId)->get();

        $data = '<table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Producto</th>
                        <th width="10%">Cantidad</th>
                        <th width="10%">Valor</th>
                        <th width="10%">Agregar</th>
                    </tr>
                    </thead>
                    <tbody>';

                    foreach ($productos AS $producto){

                        $data .= '<tr>
                            <td>'.$producto->codigo.'</td>
                            <td>'.$producto->producto.'</td>
                            <td>
                                <input name="cantidad'.$producto->id.'" id="cantidad'.$producto->id.'" type="text" class="form-control">
                            </td>
                            <td>
                                '.($coeficienteConstante * $producto->coeficiente).'
                            </td>
                            <td align="center">
                                <a href="#" onclick="return false" class="text-success btnAddProducto"
                                   data-productoid="'.$producto->id.'"
                                   data-codigo="'.$producto->codigo.'"
                                   data-producto="'.$producto->producto.'"
                                   data-valorproducto="'.($coeficienteConstante * $producto->coeficiente).'"
                                >
                                    <i class="fas fa-plus-circle fa-2x text-center"></i>
                                </a>
                            </td>
                        </tr>';
                    }

        $data .= '</tbody></table>';

        $data .= '<h4 class="mt-4">Detalle de la Sub Orden:</h4>
                <hr class="hrgrueso mt-0">
                <div id="msg"></div>
                    <table class="table table-hover table-bordered" id="tablaDetalles">
                        <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Concepto</th>
                            <th width="10%">Cant.</th>
                            <th width="10%">Valor</th>
                            <th width="10%">Subtotal</th>
                            <th width="10%">Acción</th>
                        </tr>
                        </thead>
                        <tbody>';
        $total=0;
        if($subOrdenId){

            $detalles = SubOrdenDetalle::where('suborden_id', $subOrdenId)->get();

            foreach ($detalles AS $detalle){

                $items = [
                    'productoId' => $detalle->producto_id,
                    'cantidad' => $detalle->cantidad,
                    'valor' => $detalle->valor,
                    'subtotal' => $detalle->subtotal,
                ];

                $total += $detalle->subtotal;
                $data .= '<tr>
                            <td>'.$detalle->productos->codigo.'</td>
                            <td>'.$detalle->productos->producto.'</td>
                            <td>'.$detalle->cantidad.'</td>
                            <td>'.$detalle->valor.'</td>
                            <td>'.$detalle->subtotal.'</td>
                            <td align="center">
                                <input type="hidden" name="items[]" class="itemsJSON" value=\''.json_encode($items).'\'>   
                                <a href="#" onclick="return false" class="btn btn-outline-danger btn-round btn-sm">
                                    <i class="fas fa-trash-alt fa-2x"></i>
                                </a>
                            </td>
                        </tr>';
            }

        }else{
            $data .= '<tr></tr>';
        }
        $data .= '</tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4" class="text-right">Total:</th>
                            <th id="tdTotalInstalacion" class="text-right">'.$total.'</th>
                        </tr>
                        </tfoot>
                    </table>';

        return $data;
    }
}
