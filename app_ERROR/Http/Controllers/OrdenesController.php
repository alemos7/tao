<?php

namespace App\Http\Controllers;

use App\Estado;
use App\Http\Requests\CreateOrdenesRequest;
use App\Http\Requests\UpdateOrdenesRequest;
use App\Models\Autos;
use App\Models\Clientes;
use App\Models\OrdenesArchivos;
use App\Models\OrdenesDetalles;
use App\Models\ProductosCategorias;
use App\Models\Solicitudes;
use App\Models\TipoObras;
use App\Models\TipoMedicion;
use App\Models\Tipos;
use App\Repositories\OrdenesRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Carbon\Carbon;
use DB;

class OrdenesController extends AppBaseController
{
    /** @var  OrdenesRepository */
    private $ordenesRepository;

    public function __construct(OrdenesRepository $ordenesRepo)
    {
        $this->ordenesRepository = $ordenesRepo;
    }

    /**
     * Display a listing of the Ordenes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
      $user = auth()->user();
      $userid = $user->id;


      $user_role = DB::select(
       DB::raw("
       SELECT
        role_user.id,
        role_user.role_id,
        role_user.user_id
        FROM
        role_user
        WHERE
        role_user.user_id = $userid"));

        $validaterole = $user_role[0]->role_id;

        if ($validaterole =="8") {
          $this->ordenesRepository->pushCriteria(new RequestCriteria($request));
          $ordenes = $this->ordenesRepository->join('vendedor', 'ordenes.vendedor_id', '=', 'vendedor.id')->where('userid', '=',$userid)->get();
          $tipos = Tipos::orderBy('tipo', 'asc')->get();

          return view('ordenes.index', compact('tipos'))
              ->with('ordenes', $ordenes);

        }

        
        else {
          $this->ordenesRepository->pushCriteria(new RequestCriteria($request));
          $ordenes = $this->ordenesRepository->all();
          $tipos = Tipos::orderBy('tipo', 'asc')->get();

          return view('ordenes.index', compact('tipos'))
              ->with('ordenes', $ordenes);
        }
    }

    /**
     * Show the form for creating a new Ordenes.
     *
     * @return Response
     */
    public function create()
    {
        $autos = Autos::orderBy('autos', 'ASC')->get();
        $clientes = Clientes::orderBy('cliente', 'asc')->get();
        $tipo_obras = TipoObras::orderBy('tipo_obra', 'asc')->get();
        $provincias = Estado::where('country_id', 10)->orderBy('name', 'asc')->get();

        return view('ordenes.create', compact('clientes', 'tipo_obras', 'provincias', 'autos'));
    }

    /**
     * Store a newly created Ordenes in storage.
     *
     * @param CreateOrdenesRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenesRequest $request)
    {
        $input = $request->all();
        $input['user_create_id'] = Auth::user()->id;

        $orden = $this->ordenesRepository->create($input);

        //    SUBIR ARCHIVO FACTURA
        $files = $request->file('file');
        if(isset($files) and $files!='') {
            foreach ($files as $key => $file) {
                if (!empty($file)) {

                    $extension = explode('.', $file->getClientOriginalName());
                    $extension = end($extension);

                    $filename = "Documentacion_Orden_" . $orden->id . '_F' . date('Ymd') . '_H' . date('His') . '_' . rand(1000, 9999) . '.' . $extension;
                    \File::put('files/documentacion/' . $filename, \File::get($file));
                }

                OrdenesArchivos::create([
                    'orden_id' => $orden->id,
                    'archivo' => $filename,
                ]);
            }
        }
        Flash::success('Orden Generada satisfactoriamente.');

        return redirect(route('ordenes.index'));
    }

    /**
     * Display the specified Ordenes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenes = $this->ordenesRepository->findWithoutFail($id);

        if (empty($ordenes)) {
            Flash::error('Ordenes not found');

            return redirect(route('ordenes.index'));
        }

        return view('ordenes.show')->with('ordenes', $ordenes);
    }

    /**
     * Show the form for editing the specified Ordenes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $ordenes = $this->ordenesRepository->findWithoutFail($id);
        $autos = Autos::orderBy('autos', 'ASC')->get();
        $clientes = Clientes::orderBy('cliente', 'asc')->get();
        $tipo_obras = TipoObras::orderBy('tipo_obra', 'asc')->get();
        $provincias = Estado::where('country_id', 10)->orderBy('name', 'asc')->get();

        if (empty($ordenes)) {
            Flash::error('Ordenes not found');

            return redirect(route('ordenes.index'));
        }

        return view('ordenes.edit', compact('ordenes', 'clientes', 'tipo_obras', 'provincias', 'autos'));
    }

    /**
     * Update the specified Ordenes in storage.
     *
     * @param  int $id
     * @param UpdateOrdenesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenesRequest $request)
    {
        $ordenes = $this->ordenesRepository->findWithoutFail($id);

        if (empty($ordenes)) {
            Flash::error('Ordenes not found');

            return redirect(route('ordenes.index'));
        }

        $input = $request->all();

        if (isset($input['fecha_verificacion'])) {
            $fecha_verificacionDT = Carbon::createFromFormat('d/m/Y', $input['fecha_verificacion']);
            $input['fecha_verificacion'] = $fecha_verificacionDT->format('Y-m-d');
        }
        if (isset($input['fecha_inicio_obra'])) {
            $fecha_hora_inicioDT = Carbon::createFromFormat('d/m/Y - H:i', $input['fecha_inicio_obra']);
            $input['fecha_inicio_obra'] = $fecha_hora_inicioDT->format('Y-m-d');
            $input['hora_inicio'] = $fecha_hora_inicioDT->format('H:i');
        }
        if (isset($input['fecha_medicion'])) {
            $fecha_medicionDT = Carbon::createFromFormat('d/m/Y', $input['fecha_medicion']);
            $input['fecha_medicion'] = $fecha_medicionDT->format('Y-m-d');
        }

        $this->ordenesRepository->update($input, $id);

        Flash::success('Orden de Trabajo guardada satisfactoriamente.');

        return redirect(route('ordenes.index'));
    }

    /**
     * Remove the specified Ordenes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenes = $this->ordenesRepository->findWithoutFail($id);

        if (empty($ordenes)) {
            Flash::error('Ordenes not found');

            return redirect(route('ordenes.index'));
        }

        $this->ordenesRepository->delete($id);

        Flash::success('Orden de Trabajo eliminada satisfactoriamente.');

        return redirect(route('ordenes.index'));
    }
}
