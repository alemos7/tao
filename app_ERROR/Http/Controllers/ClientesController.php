<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClientesRequest;
use App\Http\Requests\UpdateClientesRequest;
use App\Models\Clientes;
use App\Models\Recibos;
use App\Models\SubOrden;
use App\Repositories\ClientesRepository;
use App\Http\Controllers\AppBaseController;
use Doctrine\DBAL\Events;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Estado;
use Illuminate\Support\Facades\DB;

class ClientesController extends AppBaseController
{
    /** @var  ClientesRepository */
    private $clientesRepository;

    public function __construct(ClientesRepository $clientesRepo)
    {
        $this->clientesRepository = $clientesRepo;
    }

    /**
     * Display a listing of the Clientes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->clientesRepository->pushCriteria(new RequestCriteria($request));
        $clientes = $this->clientesRepository->all();

        return view('clientes.index')
            ->with('clientes', $clientes);
    }

    /**
     * Show the form for creating a new Clientes.
     *
     * @return Response
     */
    public function create()
    {
        $provincias = Estado::where('country_id', 10)->orderBy('name', 'asc')->get();
        $usuarioscliente = DB::select(
           DB::raw("
           SELECT
            role_user.id,
            role_user.role_id,
            role_user.user_id,
            users.nombre
            FROM
            role_user
            INNER JOIN users ON role_user.user_id = users.id
            WHERE
            role_user.user_id = 9
           "));
        return view('clientes.create', compact('provincias','usuarioscliente'));
    }

    /**
     * Store a newly created Clientes in storage.
     *
     * @param CreateClientesRequest $request
     *
     * @return Response
     */
    public function store(CreateClientesRequest $request)
    {
        $input = $request->all();

        $clientes = $this->clientesRepository->create($input);

        Flash::success('Clientes guardado satisfactoriamente.');

        return redirect(route('clientes.index'));
    }

    /**
     * Display the specified Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clientes = $this->clientesRepository->findWithoutFail($id);

        $recibos = DB::table('recibos')
                            ->select('id AS idoperacion','created_at AS fecha', 'tipo','id AS orden','pago_suborden AS monto','suborden_id')
                            ->where('cliente_id', $id)
                            ->orderBy('created_at')
                            ->orderBy('id')
                            ->get();

        $subordenes = DB::table('ordenes_suborden AS S')
                                ->select('S.id AS idoperacion','S.created_at AS fecha','S.tipo','O.id AS orden',  'S.id AS suborden_id', DB::raw('SUM(SD.subtotal) as monto'))
                                ->leftJoin('ordenes AS O', 'S.orden_id', '=', 'O.id')
                                ->leftJoin('ordenes_suborden_detalle AS SD', 'S.id', '=', 'SD.suborden_id')
                                ->where('O.cliente_id', $id)
                                ->where('S.solicitud_id', 6) //6 Es el id del Tipo de Solicitud ALTA
                                ->groupBy('SD.suborden_id')
                                ->get();

        $merged = $subordenes->merge($recibos)->sortBy('fecha');

        $acumulando = 0;
        foreach ($merged AS $item){
            if($item->tipo=='Orden'){
                $acumulando += $item->monto;
            }else{
                $acumulando -= $item->monto;
            }
            $item->total = $acumulando;
        }

        $subordenes = array_reverse($merged->toArray());

        if (empty($clientes)) {
            Flash::error('Clientes not found');

            return redirect(route('clientes.index'));
        }

        return view('clientes.show', compact('clientes','subordenes'));
    }

    /**
     * Show the form for editing the specified Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clientes = $this->clientesRepository->findWithoutFail($id);
        $provincias = Estado::where('country_id', 10)->orderBy('name', 'asc')->get();
        $usuarioscliente = DB::select(
           DB::raw("
           SELECT
            role_user.id,
            role_user.role_id,
            role_user.user_id,
            users.nombre
            FROM
            role_user
            INNER JOIN users ON role_user.user_id = users.id
            WHERE
            role_user.user_id = 9
           "));

        if (empty($clientes)) {
            Flash::error('Clientes not found');

            return redirect(route('clientes.index'));
        }

        return view('clientes.edit', compact('clientes','provincias','usuarioscliente'));
    }

    /**
     * Update the specified Clientes in storage.
     *
     * @param  int              $id
     * @param UpdateClientesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientesRequest $request)
    {
        $clientes = $this->clientesRepository->findWithoutFail($id);

        if (empty($clientes)) {
            Flash::error('Clientes not found');

            return redirect(route('clientes.index'));
        }

        $clientes = $this->clientesRepository->update($request->all(), $id);

        Flash::success('Clientes guardado satisfactoriamente.');

        return redirect(route('clientes.index'));
    }

    /**
     * Remove the specified Clientes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clientes = $this->clientesRepository->findWithoutFail($id);

        if (empty($clientes)) {
            Flash::error('Clientes not found');

            return redirect(route('clientes.index'));
        }

        $this->clientesRepository->delete($id);

        Flash::success('Clientes eliminado satisfactoriamente.');

        return redirect(route('clientes.index'));
    }

    public function estadoFinanciero(){
        $clientes = Clientes::orderBy('cliente')->get();

        return view('clientes.estadoFinanciero', compact('clientes'));
    }


}
