<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdicionalesChoferRequest;
use App\Http\Requests\UpdateAdicionalesChoferRequest;
use App\Models\AdicionalesChofer;
use App\Repositories\AdicionalesChoferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AdicionalesChoferController extends AppBaseController
{
    /** @var  AdicionalesChoferRepository */
    private $adicionalesChoferRepository;

    public function __construct(AdicionalesChoferRepository $adicionalesChoferRepo)
    {
        $this->adicionalesChoferRepository = $adicionalesChoferRepo;
    }

    public function adicionalesChofer()
    {
        $adicional = AdicionalesChofer::where('id',1)->first();
        return view('adicionales_chofers.edit', compact('adicional'));
    }

    /**
     * Update the specified AdicionalesEficiencia in storage.
     *
     * @param  int              $id
     * @param UpdateAdicionalesEficienciaRequest $request
     *
     * @return Response
     */
    public function guardarChofer(Request $request)
    {

        $datos = $request->input();

        $data = [
            'adicional_costo' => $datos['adicional_costo']
        ];

        $adicional = AdicionalesChofer::where('id', $datos['adicional_id'])->update($data);

        Flash::success('Adicionales Chofer updated successfully.');

        return redirect(route('adicionalesChofer'));
    }
}
