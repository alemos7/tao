<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCajasProductosRequest;
use App\Http\Requests\UpdateCajasProductosRequest;
use App\Repositories\CajasProductosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CajasProductosController extends AppBaseController
{
    /** @var  CajasProductosRepository */
    private $cajasProductosRepository;

    public function __construct(CajasProductosRepository $cajasProductosRepo)
    {
        $this->cajasProductosRepository = $cajasProductosRepo;
    }

    /**
     * Display a listing of the CajasProductos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cajasProductosRepository->pushCriteria(new RequestCriteria($request));
        $cajasProductos = $this->cajasProductosRepository->all();

        return view('cajas_productos.index')
            ->with('cajasProductos', $cajasProductos);
    }

    /**
     * Show the form for creating a new CajasProductos.
     *
     * @return Response
     */
    public function create()
    {
        return view('cajas_productos.create');
    }

    /**
     * Store a newly created CajasProductos in storage.
     *
     * @param CreateCajasProductosRequest $request
     *
     * @return Response
     */
    public function store(CreateCajasProductosRequest $request)
    {
        $input = $request->all();

        $cajasProductos = $this->cajasProductosRepository->create($input);

        Flash::success('Cajas Productos saved successfully.');

        return redirect(route('cajasProductos.index'));
    }

    /**
     * Display the specified CajasProductos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cajasProductos = $this->cajasProductosRepository->findWithoutFail($id);

        if (empty($cajasProductos)) {
            Flash::error('Cajas Productos not found');

            return redirect(route('cajasProductos.index'));
        }

        return view('cajas_productos.show')->with('cajasProductos', $cajasProductos);
    }

    /**
     * Show the form for editing the specified CajasProductos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cajasProductos = $this->cajasProductosRepository->findWithoutFail($id);

        if (empty($cajasProductos)) {
            Flash::error('Cajas Productos not found');

            return redirect(route('cajasProductos.index'));
        }

        return view('cajas_productos.edit')->with('cajasProductos', $cajasProductos);
    }

    /**
     * Update the specified CajasProductos in storage.
     *
     * @param  int              $id
     * @param UpdateCajasProductosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCajasProductosRequest $request)
    {
        $cajasProductos = $this->cajasProductosRepository->findWithoutFail($id);

        if (empty($cajasProductos)) {
            Flash::error('Cajas Productos not found');

            return redirect(route('cajasProductos.index'));
        }

        $cajasProductos = $this->cajasProductosRepository->update($request->all(), $id);

        Flash::success('Cajas Productos updated successfully.');

        return redirect(route('cajasProductos.index'));
    }

    /**
     * Remove the specified CajasProductos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cajasProductos = $this->cajasProductosRepository->findWithoutFail($id);

        if (empty($cajasProductos)) {
            Flash::error('Cajas Productos not found');

            return redirect(route('cajasProductos.index'));
        }

        $this->cajasProductosRepository->delete($id);

        Flash::success('Cajas Productos deleted successfully.');

        return redirect(route('cajasProductos.index'));
    }
}
