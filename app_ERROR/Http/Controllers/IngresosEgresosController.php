<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIngresosEgresosRequest;
use App\Http\Requests\UpdateIngresosEgresosRequest;
use App\Models\IngresosEgresos;
use App\Repositories\IngresosEgresosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DateTime;
use Carbon\Carbon;

class IngresosEgresosController extends AppBaseController
{
    /** @var  IngresosEgresosRepository */
    private $ingresosEgresosRepository;

    public function __construct(IngresosEgresosRepository $ingresosEgresosRepo)
    {
        $this->ingresosEgresosRepository = $ingresosEgresosRepo;
    }

    /**
     * Display a listing of the IngresosEgresos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ingresosEgresosRepository->pushCriteria(new RequestCriteria($request));
        $ingresosEgresos = $this->ingresosEgresosRepository->all();

        return view('ingresos_egresos.index')
            ->with('ingresosEgresos', $ingresosEgresos);
    }

    /**
     * Show the form for creating a new IngresosEgresos.
     *
     * @return Response
     */
    public function create()
    {
        return view('ingresos_egresos.create');
    }

    /**
     * Store a newly created IngresosEgresos in storage.
     *
     * @param CreateIngresosEgresosRequest $request
     *
     * @return Response
     */
    public function store(CreateIngresosEgresosRequest $request)
    {
        $input = $request->all();

        $ingresosEgresos = $this->ingresosEgresosRepository->create($input);

        Flash::success('Ingresos Egresos saved successfully.');

        return redirect(route('ingresosEgresos.index'));
    }

    /**
     * Display the specified IngresosEgresos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ingresosEgresos = $this->ingresosEgresosRepository->findWithoutFail($id);

        if (empty($ingresosEgresos)) {
            Flash::error('Ingresos Egresos not found');

            return redirect(route('ingresosEgresos.index'));
        }

        return view('ingresos_egresos.show')->with('ingresosEgresos', $ingresosEgresos);
    }

    /**
     * Show the form for editing the specified IngresosEgresos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ingresosEgresos = $this->ingresosEgresosRepository->findWithoutFail($id);

        if (empty($ingresosEgresos)) {
            Flash::error('Ingresos Egresos not found');

            return redirect(route('ingresosEgresos.index'));
        }

        return view('ingresos_egresos.edit')->with('ingresosEgresos', $ingresosEgresos);
    }

    /**
     * Update the specified IngresosEgresos in storage.
     *
     * @param  int              $id
     * @param UpdateIngresosEgresosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIngresosEgresosRequest $request)
    {
        $ingresosEgresos = $this->ingresosEgresosRepository->findWithoutFail($id);

        if (empty($ingresosEgresos)) {
            Flash::error('Ingresos Egresos not found');

            return redirect(route('ingresosEgresos.index'));
        }

        $ingresosEgresos = $this->ingresosEgresosRepository->update($request->all(), $id);

        Flash::success('Ingresos Egresos updated successfully.');

        return redirect(route('ingresosEgresos.index'));
    }

    /**
     * Remove the specified IngresosEgresos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ingresosEgresos = $this->ingresosEgresosRepository->findWithoutFail($id);

        if (empty($ingresosEgresos)) {
            Flash::error('Ingresos Egresos not found');

            return redirect(route('ingresosEgresos.index'));
        }

        $this->ingresosEgresosRepository->delete($id);

        Flash::success('Ingresos Egresos deleted successfully.');

        return redirect(route('ingresosEgresos.index'));
    }

    public function proyeccion()
    {
        $ultimoDiaMes = Carbon::now()->endOfMonth()->format('d');

        $ingresoSemana1 = IngresosEgresos::where('tipo','I')
                                         ->where('periocidad','>=',1)
                                         ->where('periocidad','<=',7)
                                         ->get();
        $egresoSemana1 = IngresosEgresos::where('tipo','E')
                                         ->where('periocidad','>=',1)
                                         ->where('periocidad','<=',7)
                                         ->get();

        $ingresoSemana2 = IngresosEgresos::where('tipo','I')
                                         ->where('periocidad','>=',8)
                                         ->where('periocidad','<=',14)
                                         ->get();
        $egresoSemana2 = IngresosEgresos::where('tipo','E')
                                         ->where('periocidad','>=',8)
                                         ->where('periocidad','<=',14)
                                         ->get();

        $ingresoSemana3 = IngresosEgresos::where('tipo','I')
                                         ->where('periocidad','>=',15)
                                         ->where('periocidad','<=',21)
                                         ->get();
        $egresoSemana3 = IngresosEgresos::where('tipo','E')
                                         ->where('periocidad','>=',15)
                                         ->where('periocidad','<=',21)
                                         ->get();

        $ingresoSemana4 = IngresosEgresos::where('tipo','I')
                                         ->where('periocidad','>=',22)
                                         ->where('periocidad','<=',$ultimoDiaMes)
                                         ->get();
        $egresoSemana4 = IngresosEgresos::where('tipo','E')
                                         ->where('periocidad','>=',22)
                                         ->where('periocidad','<=',$ultimoDiaMes)
                                         ->get();


        $ie = [
            'ingresos' => [
                            'semana1' => $ingresoSemana1,
                            'semana2' => $ingresoSemana2,
                            'semana3' => $ingresoSemana3,
                            'semana4' => $ingresoSemana4,
            ],
            'egresos' => [
                            'semana1' => $egresoSemana1,
                            'semana2' => $egresoSemana2,
                            'semana3' => $egresoSemana3,
                            'semana4' => $egresoSemana4,
            ]
        ];

        return view('ingresos_egresos.proyeccion', compact('ultimoDiaMes', 'ie'));
    }
}
