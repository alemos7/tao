<?php

namespace App\Console\Commands;

use App\Models\SubOrden;
use App\User;
use Illuminate\Console\Command;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;

class WorksDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'works:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para enviar correo a los tecnicos que tengan Obras programadas para el dia de hoy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subordenes = SubOrden::where('fecha_inicio',date("Y-m-d"))->get();

        foreach ($subordenes AS $suborden){

            if($suborden->tecnicos1->email){
                $email = $suborden->tecnicos1->email;
                Mail::to($email)->send(new SendMailable($suborden));
            }

            if($suborden->tecnicos2->email){
                $email = $suborden->tecnicos2->email;
                Mail::to($email)->send(new SendMailable($suborden));
            }

            if($suborden->tecnicos3->email){
                $email = $suborden->tecnicos3->email;
                Mail::to($email)->send(new SendMailable($suborden));
            }

            if($suborden->tecnicos4->email){
                $email = $suborden->tecnicos4->email;
                Mail::to($email)->send(new SendMailable($suborden));
            }
        }

    }
}
