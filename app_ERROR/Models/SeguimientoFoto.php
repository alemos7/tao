<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeguimientoFoto extends Model
{
    public $table = 'ordenes_suborden_seguimiento_fotos';

    public $fillable = [
        'seguimiento_id',
        'foto'
    ];

}
