<?php

namespace App\Models;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Seguimiento
 * @package App\Models
 * @version February 28, 2020, 11:42 pm EST
 *
 * @property integer suborden_id
 * @property integer user_id
 * @property time hora
 * @property string fecha
 * @property boolean tipo
 * @property string observacion
 * @property string latitud
 * @property string longitud
 */
class Seguimiento extends Model
{
    use SoftDeletes;

    public $table = 'ordenes_suborden_seguimiento';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'suborden_id',
        'user_id',
        'hora',
        'fecha',
        'tipo',
        'observacion',
        'latitud',
        'longitud'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'suborden_id' => 'integer',
        'user_id' => 'integer',
        'fecha' => 'date',
        'tipo' => 'string',
        'observacion' => 'string',
        'latitud' => 'string',
        'longitud' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'suborden_id' => 'required',
        'user_id' => 'required',
        'hora' => 'required',
        'fecha' => 'required',
        'tipo' => 'required'
    ];

    public function usuarios(){

        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
