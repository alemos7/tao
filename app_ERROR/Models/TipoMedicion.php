<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMedicion extends Model
{
    public $table = 'tipo_medicion';

    public $fillable = [
        'tipo_medicion'
    ];
}
