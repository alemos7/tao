<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Productos
 * @package App\Models
 * @version January 7, 2020, 9:48 pm EST
 *
 * @property integer categoria_id
 * @property string codigo
 * @property string producto
 */
class Productos extends Model
{
    use SoftDeletes;

    public $table = 'productos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'categoria_id',
        'codigo',
        'producto',
        'coeficiente'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'categoria_id' => 'integer',
        'codigo' => 'string',
        'producto' => 'string',
        'coeficiente' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'categoria_id' => 'required',
        'codigo' => 'required',
        'producto' => 'required',
        'coeficiente' => 'required'
    ];

    #Funciones para las Relaciones con Tablas Foraneas
    public function categorias(){
        return $this->belongsTo('App\Models\ProductosCategorias', 'categoria_id', 'id');
    }

}
