<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cajas
 * @package App\Models
 * @version January 28, 2020, 4:06 pm EST
 *
 * @property integer id
 * @property string caja
 */
class Cajas extends Model
{
    use SoftDeletes;

    public $table = 'cajas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'caja',
        'color'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'caja' => 'string',
        'color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'caja' => 'required',
        'color' => 'required'
    ];

}
