<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenesArchivos extends Model
{
    public $table = 'ordenes_archivos';

    public $fillable = [
        'orden_id',
        'archivo'
    ];
}
