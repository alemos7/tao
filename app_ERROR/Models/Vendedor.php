<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vendedor
 * @package App\Models
 * @version January 5, 2020, 8:49 am EST
 *
 * @property integer cliente_id
 * @property string vendedor
 */
class Vendedor extends Model
{
    use SoftDeletes;

    public $table = 'vendedor';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cliente_id',
        'vendedor',
        'correo',
        'telefono',
        'userid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'vendedor' => 'string',
        'correo' => 'string',
        'telefono' => 'string',
        'userid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente_id' => 'required',
        'vendedor' => 'required',
        'correo' => 'required',
        'userid' => 'required'
    ];

    #Funciones para las Relaciones con Tablas Foraneas
    public function clientes(){
        return $this->belongsTo('App\Models\Clientes', 'cliente_id', 'id');
    }
}
