<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Clientes
 * @package App\Models
 * @version January 5, 2020, 8:31 am EST
 *
 * @property string cuit
 * @property string dni
 * @property string cliente
 * @property string contacto
 * @property string telefono
 * @property string email
 * @property integer provincia_id
 * @property integer localidad_id
 * @property string direccion
 * @property string observacion
 * @property float monto_financiar
 * @property integer periodo_facturarcion
 */
class Clientes extends Model
{
    use SoftDeletes;

    public $table = 'clientes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cuit',
        'dni',
        'cliente',
        'contacto',
        'telefono',
        'email',
        'provincia_id',
        'localidad_id',
        'direccion',
        'observacion',
        'monto_financiar',
        'periodo_facturarcion',
        'userid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cuit' => 'string',
        'dni' => 'string',
        'cliente' => 'string',
        'contacto' => 'string',
        'telefono' => 'string',
        'email' => 'string',
        'provincia_id' => 'integer',
        'localidad_id' => 'integer',
        'direccion' => 'string',
        'observacion' => 'string',
        'monto_financiar' => 'float',
        'periodo_facturarcion' => 'integer',
        'userid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente' => 'required',
        'contacto' => 'required',
        'telefono' => 'required',
        'monto_financiar' => 'required',
        'periodo_facturarcion' => 'required',
        'email' => 'required'
    ];


    #Funciones para las Relaciones con Tablas Foraneas
    public function provincias(){
        return $this->belongsTo('App\Estado', 'provincia_id', 'id');
    }

    public function localidades(){
        return $this->belongsTo('App\Ciudad', 'localidad_id', 'id');
    }

    public function getTotalCuentaCorriente($id)
    {
        $recibos = DB::table('recibos')
            ->select('id AS idoperacion','created_at AS fecha', 'tipo','id AS orden','pago_suborden AS monto','suborden_id')
            ->where('cliente_id', $id)
            ->orderBy('created_at')
            ->orderBy('id')
            ->get();

        $subordenes = DB::table('ordenes_suborden AS S')
            ->select('S.id AS idoperacion','S.created_at AS fecha','S.tipo','O.id AS orden',  'S.id AS suborden_id', DB::raw('SUM(SD.subtotal) as monto'))
            ->leftJoin('ordenes AS O', 'S.orden_id', '=', 'O.id')
            ->leftJoin('ordenes_suborden_detalle AS SD', 'S.id', '=', 'SD.suborden_id')
            ->where('O.cliente_id', $id)
            ->where('S.solicitud_id', 6) //6 Es el id del Tipo de Solicitud ALTA
            ->groupBy('SD.suborden_id')
            ->get();

        $merged = $subordenes->merge($recibos)->sortBy('fecha');

        if(count($merged)>0) {
            $acumulando = 0;
            foreach ($merged AS $item) {
                if ($item->tipo == 'Orden') {
                    $acumulando += $item->monto;
                } else {
                    $acumulando -= $item->monto;
                }
                $item->total = $acumulando;
            }

            $subordenes = array_reverse($merged->toArray());

            return $subordenes[0]->total;
        }else{
            return 0;
        }

    }
}
