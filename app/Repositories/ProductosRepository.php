<?php

namespace App\Repositories;

use App\Models\Productos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductosRepository
 * @package App\Repositories
 * @version January 7, 2020, 9:48 pm EST
 *
 * @method Productos findWithoutFail($id, $columns = ['*'])
 * @method Productos find($id, $columns = ['*'])
 * @method Productos first($columns = ['*'])
*/
class ProductosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'categoria_id',
        'codigo',
        'producto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Productos::class;
    }
}
