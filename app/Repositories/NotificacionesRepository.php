<?php

namespace App\Repositories;

use App\Models\Notificaciones;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotificacionesRepository
 * @package App\Repositories
 * @version March 17, 2020, 8:34 pm EDT
 *
 * @method Notificaciones findWithoutFail($id, $columns = ['*'])
 * @method Notificaciones find($id, $columns = ['*'])
 * @method Notificaciones first($columns = ['*'])
*/
class NotificacionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'comentario',
        'fecha_desde',
        'fecha_hasta',
        'permission_role_id',
        'estatus'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notificaciones::class;
    }
}
