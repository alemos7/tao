<?php

namespace App\Repositories;

use App\Models\TipoObras;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TipoObrasRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:47 am EST
 *
 * @method TipoObras findWithoutFail($id, $columns = ['*'])
 * @method TipoObras find($id, $columns = ['*'])
 * @method TipoObras first($columns = ['*'])
*/
class TipoObrasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo_obra'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoObras::class;
    }
}
