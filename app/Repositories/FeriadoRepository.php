<?php

namespace App\Repositories;

use App\Models\Feriado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FeriadoRepository
 * @package App\Repositories
 * @version March 19, 2020, 8:37 pm EDT
 *
 * @method Feriado findWithoutFail($id, $columns = ['*'])
 * @method Feriado find($id, $columns = ['*'])
 * @method Feriado first($columns = ['*'])
*/
class FeriadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feriado::class;
    }
}
