<?php

namespace App\Repositories;

use App\Models\SubOrdenDetalle;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubOrdenDetalleRepository
 * @package App\Repositories
 * @version January 10, 2020, 11:20 pm EST
 *
 * @method SubOrdenDetalle findWithoutFail($id, $columns = ['*'])
 * @method SubOrdenDetalle find($id, $columns = ['*'])
 * @method SubOrdenDetalle first($columns = ['*'])
*/
class SubOrdenDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_id',
        'producto_id',
        'cantidad',
        'precio'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubOrdenDetalle::class;
    }
}
