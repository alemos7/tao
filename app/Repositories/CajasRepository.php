<?php

namespace App\Repositories;

use App\Models\Cajas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CajasRepository
 * @package App\Repositories
 * @version January 28, 2020, 4:06 pm EST
 *
 * @method Cajas findWithoutFail($id, $columns = ['*'])
 * @method Cajas find($id, $columns = ['*'])
 * @method Cajas first($columns = ['*'])
*/
class CajasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'caja'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cajas::class;
    }
}
