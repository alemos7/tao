<?php

namespace App\Repositories;

use App\Models\Prestamos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PrestamosRepository
 * @package App\Repositories
 * @version March 18, 2020, 9:19 pm EDT
 *
 * @method Prestamos findWithoutFail($id, $columns = ['*'])
 * @method Prestamos find($id, $columns = ['*'])
 * @method Prestamos first($columns = ['*'])
*/
class PrestamosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'monto',
        'fecha',
        'cuotas',
        'pendientes'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prestamos::class;
    }
}
