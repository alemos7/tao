<?php

namespace App\Repositories;

use App\Models\AdicionalesChofer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AdicionalesChoferRepository
 * @package App\Repositories
 * @version February 28, 2020, 9:53 pm EST
 *
 * @method AdicionalesChofer findWithoutFail($id, $columns = ['*'])
 * @method AdicionalesChofer find($id, $columns = ['*'])
 * @method AdicionalesChofer first($columns = ['*'])
*/
class AdicionalesChoferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'adicional_costo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AdicionalesChofer::class;
    }
}
