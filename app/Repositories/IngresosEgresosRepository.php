<?php

namespace App\Repositories;

use App\Models\IngresosEgresos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IngresosEgresosRepository
 * @package App\Repositories
 * @version February 29, 2020, 9:25 pm EST
 *
 * @method IngresosEgresos findWithoutFail($id, $columns = ['*'])
 * @method IngresosEgresos find($id, $columns = ['*'])
 * @method IngresosEgresos first($columns = ['*'])
*/
class IngresosEgresosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'concepto',
        'monto',
        'periocidad',
        'tipo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IngresosEgresos::class;
    }
}
