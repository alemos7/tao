<?php

namespace App\Repositories;

use App\Models\Herramientas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HerramientasRepository
 * @package App\Repositories
 * @version February 28, 2020, 7:07 am EST
 *
 * @method Herramientas findWithoutFail($id, $columns = ['*'])
 * @method Herramientas find($id, $columns = ['*'])
 * @method Herramientas first($columns = ['*'])
*/
class HerramientasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'herramienta'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Herramientas::class;
    }
}
