<?php

namespace App\Repositories;

use App\Models\Barrios;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BarriosRepository
 * @package App\Repositories
 * @version April 9, 2020, 1:24 pm EDT
 *
 * @method Barrios findWithoutFail($id, $columns = ['*'])
 * @method Barrios find($id, $columns = ['*'])
 * @method Barrios first($columns = ['*'])
*/
class BarriosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'barrio',
        'kilometro',
        'adjunto_1',
        'adjunto_2',
        'adjunto_3',
        'adjunto_4',
        'adjunto_5',
        'adjunto_6',
        'adjunto_7',
        'adjunto_8',
        'adjunto_9',
        'adjunto_10',
        'adjunto_11'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Barrios::class;
    }
}
