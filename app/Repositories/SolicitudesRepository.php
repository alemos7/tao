<?php

namespace App\Repositories;

use App\Models\Solicitudes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SolicitudesRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:46 am EST
 *
 * @method Solicitudes findWithoutFail($id, $columns = ['*'])
 * @method Solicitudes find($id, $columns = ['*'])
 * @method Solicitudes first($columns = ['*'])
*/
class SolicitudesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'solicitud'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Solicitudes::class;
    }
}
