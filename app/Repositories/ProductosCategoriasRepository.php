<?php

namespace App\Repositories;

use App\Models\ProductosCategorias;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductosCategoriasRepository
 * @package App\Repositories
 * @version January 7, 2020, 9:48 pm EST
 *
 * @method ProductosCategorias findWithoutFail($id, $columns = ['*'])
 * @method ProductosCategorias find($id, $columns = ['*'])
 * @method ProductosCategorias first($columns = ['*'])
*/
class ProductosCategoriasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'categoria'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductosCategorias::class;
    }
}
