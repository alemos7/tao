<?php

namespace App\Repositories;

use App\Models\Liquidaciones;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LiquidacionesRepository
 * @package App\Repositories
 * @version March 17, 2020, 8:41 pm EDT
 *
 * @method Liquidaciones findWithoutFail($id, $columns = ['*'])
 * @method Liquidaciones find($id, $columns = ['*'])
 * @method Liquidaciones first($columns = ['*'])
*/
class LiquidacionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'periodo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Liquidaciones::class;
    }
}
