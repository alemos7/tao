<?php

namespace App\Repositories;

use App\Models\ProductosTiposObras;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductosTiposObrasRepository
 * @package App\Repositories
 * @version January 7, 2020, 9:48 pm EST
 *
 * @method ProductosTiposObras findWithoutFail($id, $columns = ['*'])
 * @method ProductosTiposObras find($id, $columns = ['*'])
 * @method ProductosTiposObras first($columns = ['*'])
*/
class ProductosTiposObrasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'producto_id',
        'tipo_obra_id',
        'cantidad'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductosTiposObras::class;
    }
}
