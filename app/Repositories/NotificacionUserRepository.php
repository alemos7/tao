<?php

namespace App\Repositories;

use App\Models\NotificacionUser;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotificacionUserRepository
 * @package App\Repositories
 * @version March 17, 2020, 8:38 pm EDT
 *
 * @method NotificacionUser findWithoutFail($id, $columns = ['*'])
 * @method NotificacionUser find($id, $columns = ['*'])
 * @method NotificacionUser first($columns = ['*'])
*/
class NotificacionUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'notificaciones_id',
        'clientes_id',
        'estatus'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NotificacionUser::class;
    }
}
