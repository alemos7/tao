<?php

namespace App\Repositories;

use App\Models\OrdenesDetalles;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrdenesDetallesRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:38 am EST
 *
 * @method OrdenesDetalles findWithoutFail($id, $columns = ['*'])
 * @method OrdenesDetalles find($id, $columns = ['*'])
 * @method OrdenesDetalles first($columns = ['*'])
*/
class OrdenesDetallesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_id',
        'precio_id',
        'cantidad'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenesDetalles::class;
    }
}
