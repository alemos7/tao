<?php

namespace App\Repositories;

use App\Models\Vendedor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VendedorRepository
 * @package App\Repositories
 * @version January 5, 2020, 8:49 am EST
 *
 * @method Vendedor findWithoutFail($id, $columns = ['*'])
 * @method Vendedor find($id, $columns = ['*'])
 * @method Vendedor first($columns = ['*'])
*/
class VendedorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'vendedor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vendedor::class;
    }
}
