<?php

namespace App\Repositories;

use App\Models\Recibos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RecibosRepository
 * @package App\Repositories
 * @version February 1, 2020, 11:07 am EST
 *
 * @method Recibos findWithoutFail($id, $columns = ['*'])
 * @method Recibos find($id, $columns = ['*'])
 * @method Recibos first($columns = ['*'])
*/
class RecibosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'fecha',
        'monto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recibos::class;
    }
}
