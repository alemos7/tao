<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Ordenes;

class CreateOrdenesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Ordenes::$rules;
    }

    public function messages()
    {
        return [
            'cliente_id.required' => 'Debe Seleccionar un Cliente',
            'vendedor_id.required' => 'Debe Seleccionar un Vendedor',
            'tipo_obra_id.required' => 'Debe Seleccionar un Tipo de Obra',
            'consumidor_nombre.required' => 'Debe indicar un Nombre para el Consumidor',
            'consumidor_tlf.required' => 'Debe indicar un Telefono para el Consumidor',
            'consumidor_email.required' => 'Debe indicar un Email para el Consumidor',
            'consumidor_provincia_id.required' => 'Debe indicar una Provincia para el Consumidor',
            'consumidor_localidad_id.required' => 'Debe indicar una Localidad para el Consumidor',
            'consumidor_address.required' => 'Debe indicar una Dirección para el Consumidor'
        ];
    }
}
