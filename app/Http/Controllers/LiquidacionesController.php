<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLiquidacionesRequest;
use App\Http\Requests\UpdateLiquidacionesRequest;
use App\Models\Liquidaciones;
use App\Repositories\LiquidacionesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LiquidacionesController extends AppBaseController
{
    /** @var  LiquidacionesRepository */
    private $liquidacionesRepository;

    public function __construct(LiquidacionesRepository $liquidacionesRepo)
    {
        $this->liquidacionesRepository = $liquidacionesRepo;
    }

    /**
     * Display a listing of the Liquidaciones.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->liquidacionesRepository->pushCriteria(new RequestCriteria($request));
        $liquidaciones = Liquidaciones::where('user_id', auth()->user()->id)->get();

        return view('liquidaciones.index')
            ->with('liquidaciones', $liquidaciones);
    }

    /**
     * Show the form for creating a new Liquidaciones.
     *
     * @return Response
     */
    public function create()
    {
        return view('liquidaciones.create');
    }

    /**
     * Store a newly created Liquidaciones in storage.
     *
     * @param CreateLiquidacionesRequest $request
     *
     * @return Response
     */
    public function store(CreateLiquidacionesRequest $request)
    {
        $input = $request->all();

        $liquidaciones = $this->liquidacionesRepository->create($input);

        Flash::success('Liquidaciones saved successfully.');

        return redirect(route('liquidaciones.index'));
    }

    /**
     * Display the specified Liquidaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $liquidaciones = $this->liquidacionesRepository->findWithoutFail($id);

        if (empty($liquidaciones)) {
            Flash::error('Liquidaciones not found');

            return redirect(route('liquidaciones.index'));
        }

        return view('liquidaciones.show')->with('liquidaciones', $liquidaciones);
    }

    /**
     * Show the form for editing the specified Liquidaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $liquidaciones = $this->liquidacionesRepository->findWithoutFail($id);

        if (empty($liquidaciones)) {
            Flash::error('Liquidaciones not found');

            return redirect(route('liquidaciones.index'));
        }

        return view('liquidaciones.edit')->with('liquidaciones', $liquidaciones);
    }

    /**
     * Update the specified Liquidaciones in storage.
     *
     * @param  int              $id
     * @param UpdateLiquidacionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLiquidacionesRequest $request)
    {
        $liquidaciones = $this->liquidacionesRepository->findWithoutFail($id);

        if (empty($liquidaciones)) {
            Flash::error('Liquidaciones not found');

            return redirect(route('liquidaciones.index'));
        }

        $liquidaciones = $this->liquidacionesRepository->update($request->all(), $id);

        Flash::success('Liquidaciones updated successfully.');

        return redirect(route('liquidaciones.index'));
    }

    /**
     * Remove the specified Liquidaciones from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $liquidaciones = $this->liquidacionesRepository->findWithoutFail($id);

        if (empty($liquidaciones)) {
            Flash::error('Liquidaciones not found');

            return redirect(route('liquidaciones.index'));
        }

        $this->liquidacionesRepository->delete($id);

        Flash::success('Liquidaciones deleted successfully.');

        return redirect(route('liquidaciones.index'));
    }
}
