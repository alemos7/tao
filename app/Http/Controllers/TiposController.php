<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTiposRequest;
use App\Http\Requests\UpdateTiposRequest;
use App\Repositories\TiposRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TiposController extends AppBaseController
{
    /** @var  TiposRepository */
    private $tiposRepository;

    public function __construct(TiposRepository $tiposRepo)
    {
        $this->tiposRepository = $tiposRepo;
    }

    /**
     * Display a listing of the Tipos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tiposRepository->pushCriteria(new RequestCriteria($request));
        $tipos = $this->tiposRepository->all();

        return view('tipos.index')
            ->with('tipos', $tipos);
    }

    /**
     * Show the form for creating a new Tipos.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipos.create');
    }

    /**
     * Store a newly created Tipos in storage.
     *
     * @param CreateTiposRequest $request
     *
     * @return Response
     */
    public function store(CreateTiposRequest $request)
    {
        $input = $request->all();

        $tipos = $this->tiposRepository->create($input);

        Flash::success('Tipos guardado satisfactoriamente.');

        return redirect(route('tipos.index'));
    }

    /**
     * Display the specified Tipos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipos = $this->tiposRepository->findWithoutFail($id);

        if (empty($tipos)) {
            Flash::error('Tipos not found');

            return redirect(route('tipos.index'));
        }

        return view('tipos.show')->with('tipos', $tipos);
    }

    /**
     * Show the form for editing the specified Tipos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipos = $this->tiposRepository->findWithoutFail($id);

        if (empty($tipos)) {
            Flash::error('Tipos not found');

            return redirect(route('tipos.index'));
        }

        return view('tipos.edit')->with('tipos', $tipos);
    }

    /**
     * Update the specified Tipos in storage.
     *
     * @param  int              $id
     * @param UpdateTiposRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTiposRequest $request)
    {
        $tipos = $this->tiposRepository->findWithoutFail($id);

        if (empty($tipos)) {
            Flash::error('Tipos not found');

            return redirect(route('tipos.index'));
        }

        $tipos = $this->tiposRepository->update($request->all(), $id);

        Flash::success('Tipos guardado satisfactoriamente.');

        return redirect(route('tipos.index'));
    }

    /**
     * Remove the specified Tipos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipos = $this->tiposRepository->findWithoutFail($id);

        if (empty($tipos)) {
            Flash::error('Tipos not found');

            return redirect(route('tipos.index'));
        }

        $this->tiposRepository->delete($id);

        Flash::success('Tipos eliminado satisfactoriamente.');

        return redirect(route('tipos.index'));
    }

    #Buscar Datos de un Servicio
    public function searchServicio($servicio_id)
    {
        return Precios::where('id','=',$servicio_id)->first();
    }
}
