<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTipoObrasRequest;
use App\Http\Requests\UpdateTipoObrasRequest;
use App\Repositories\TipoObrasRepository;
use App\Http\Controllers\AppBaseController;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TipoObrasController extends AppBaseController
{
    /** @var  TipoObrasRepository */
    private $tipoObrasRepository;

    public function __construct(TipoObrasRepository $tipoObrasRepo)
    {
        $this->tipoObrasRepository = $tipoObrasRepo;
    }

    /**
     * Display a listing of the TipoObras.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tipoObrasRepository->pushCriteria(new RequestCriteria($request));
        $tipoObras = $this->tipoObrasRepository->all();

        return view('tipo_obras.index')
            ->with('tipoObras', $tipoObras);
    }

    /**
     * Show the form for creating a new TipoObras.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_obras.create');
    }

    /**
     * Store a newly created TipoObras in storage.
     *
     * @param CreateTipoObrasRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoObrasRequest $request)
    {
        $input = $request->all();

        $tipoObras = $this->tipoObrasRepository->create($input);

        Flash::success('Tipo Obras guardado satisfactoriamente.');

        return redirect(route('tipoObras.index'));
    }

    /**
     * Display the specified TipoObras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoObras = $this->tipoObrasRepository->findWithoutFail($id);

        if (empty($tipoObras)) {
            Flash::error('Tipo Obras not found');

            return redirect(route('tipoObras.index'));
        }

        return view('tipo_obras.show')->with('tipoObras', $tipoObras);
    }

    /**
     * Show the form for editing the specified TipoObras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoObras = $this->tipoObrasRepository->findWithoutFail($id);

        if (empty($tipoObras)) {
            Flash::error('Tipo Obras not found');

            return redirect(route('tipoObras.index'));
        }

        return view('tipo_obras.edit')->with('tipoObras', $tipoObras);
    }

    /**
     * Update the specified TipoObras in storage.
     *
     * @param  int              $id
     * @param UpdateTipoObrasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoObrasRequest $request)
    {
        $tipoObras = $this->tipoObrasRepository->findWithoutFail($id);

        if (empty($tipoObras)) {
            Flash::error('Tipo Obras not found');

            return redirect(route('tipoObras.index'));
        }

        $tipoObras = $this->tipoObrasRepository->update($request->all(), $id);

        Flash::success('Tipo Obras guardado satisfactoriamente.');

        return redirect(route('tipoObras.index'));
    }

    /**
     * Remove the specified TipoObras from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoObras = $this->tipoObrasRepository->findWithoutFail($id);

        if (empty($tipoObras)) {
            Flash::error('Tipo Obras not found');

            return redirect(route('tipoObras.index'));
        }

        $this->tipoObrasRepository->delete($id);

        Flash::success('Tipo Obras eliminado satisfactoriamente.');

        return redirect(route('tipoObras.index'));
    }
}
