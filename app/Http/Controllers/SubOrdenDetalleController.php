<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubOrdenDetalleRequest;
use App\Http\Requests\UpdateSubOrdenDetalleRequest;
use App\Models\Ordenes;
use App\Models\Productos;
use App\Repositories\SubOrdenDetalleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SubOrdenDetalleController extends AppBaseController
{
    /** @var  SubOrdenDetalleRepository */
    private $ordenesSubordenRepository;

    public function __construct(SubOrdenDetalleRepository $ordenesSubordenRepo)
    {
        $this->ordenesSubordenRepository = $ordenesSubordenRepo;
    }

    /**
     * Display a listing of the SubOrdenDetalle.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ordenesSubordenRepository->pushCriteria(new RequestCriteria($request));
        $ordenesSubordens = $this->ordenesSubordenRepository->all();

        return view('ordenes_subordens.index')
            ->with('ordenesSubordens', $ordenesSubordens);
    }

    /**
     * Show the form for creating a new SubOrdenDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('ordenes_subordens.create');
    }

    /**
     * Store a newly created SubOrdenDetalle in storage.
     *
     * @param CreateSubOrdenDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateSubOrdenDetalleRequest $request)
    {
        $input = $request->all();

        $ordenesSuborden = $this->ordenesSubordenRepository->create($input);

        Flash::success('Ordenes Suborden saved successfully.');

        return redirect(route('ordenesSubordens.index'));
    }

    /**
     * Display the specified SubOrdenDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenesSuborden = $this->ordenesSubordenRepository->findWithoutFail($id);

        if (empty($ordenesSuborden)) {
            Flash::error('Ordenes Suborden not found');

            return redirect(route('ordenesSubordens.index'));
        }

        return view('ordenes_subordens.show')->with('ordenesSuborden', $ordenesSuborden);
    }

    /**
     * Show the form for editing the specified SubOrdenDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenesSuborden = $this->ordenesSubordenRepository->findWithoutFail($id);

        if (empty($ordenesSuborden)) {
            Flash::error('Ordenes Suborden not found');

            return redirect(route('ordenesSubordens.index'));
        }

        return view('ordenes_subordens.edit')->with('ordenesSuborden', $ordenesSuborden);
    }

    /**
     * Update the specified SubOrdenDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateSubOrdenDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $ordenesSuborden = $this->ordenesSubordenRepository->findWithoutFail($id);

        if (empty($ordenesSuborden)) {
            Flash::error('Ordenes Suborden not found');

            return redirect(route('ordenesSubordens.index'));
        }

        $ordenesSuborden = $this->ordenesSubordenRepository->update($request->all(), $id);

        return $ordenesSuborden;
    }

    /**
     * Remove the specified SubOrdenDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenesSuborden = $this->ordenesSubordenRepository->findWithoutFail($id);

        if (empty($ordenesSuborden)) {
            Flash::error('Ordenes Suborden not found');

            return redirect(route('ordenesSubordens.index'));
        }

        $this->ordenesSubordenRepository->delete($id);

        Flash::success('Item Eliminado Satisfactoriamente.');

        return back();
    }
}
