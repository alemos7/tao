<?php

namespace App\Http\Controllers;

use App\Models\SubOrden;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Pais;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**
         * Just for testing Vue components
         */
        $this->middleware('auth');
    }

    public function dashboard(){

        if(Auth::user()->password_admin){
            $password_admin = 1;
            $user = Auth::user();
        }else{
            $password_admin = 0;
            $user = '';
        }

        $completeData = Auth::user()->completeData;
        $sincard = 1;

        #Si el usuario es un tecnico
        if(in_array(4, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(5, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(6, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(7, Auth::user()->perfiles->pluck('role_id')->toArray())){


//            $subordenesDelDia = SubOrden::select('ordenes_suborden.*', 'S.tipo')
//                                        ->leftJoin('ordenes_suborden_seguimiento AS S', 'ordenes_suborden.id', '=', 'S.suborden_id')
//                                        ->where('fecha_inicio', date("Y-m-d"))
//                                        ->where(function ($query) {
//                                            $query->orwhere('tecnico_id1', auth()->user()->id)
//                                                ->orWhere('tecnico_id2', auth()->user()->id)
//                                                ->orWhere('tecnico_id3', auth()->user()->id)
//                                                ->orWhere('tecnico_id4', auth()->user()->id);
//                                        })
//                                        ->groupBy('ordenes_suborden.id')
//                                        ->get();
//
//

            $subordenesDelDia = SubOrden::where('fecha_inicio', date("Y-m-d"))
                                        ->where(function ($query) {
                                            $query->orwhere('tecnico_id1', auth()->user()->id)
                                                  ->orWhere('tecnico_id2', auth()->user()->id)
                                                  ->orWhere('tecnico_id3', auth()->user()->id)
                                                  ->orWhere('tecnico_id4', auth()->user()->id);
                                        })
                                        ->get();


            return view('home-one', compact('password_admin', 'user', 'sincard', 'completeData', 'subordenesDelDia'));
        }else{
            return view('home-one', compact('password_admin', 'user', 'sincard', 'completeData'));
        }

    }

    public function perfil()
    {
        $paises = Pais::all();
        $user = User::where('id',Auth::user()->id)->first();
        $sincard = 1;

        return view('perfil',compact('user','sincard','paises'));
    }

    public function storeperfil(Request $data)
    {
        $user = User::where('id',Auth::user()->id)->first();
        $user->fill([
            'nombre' => $data['nombre'],
            'pais_id' => $data['pais_id'],
            'estado_id' => $data['estado_id'],
            'ciudad_id' => $data['ciudad_id'],
            'telefono' => $data['telefono'],
            'fax' => $data['fax'],
            'direccion' => $data['direccion'],
            'completeData' => 0
        ]);

        $password = $data->input('password');

        if($password) {
            $user['password'] = md5($password);
        }

        $user->save();

        return back()->with('info', 'Datos Guardados Satisfactoriamente');

    }
}
