<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAutosRequest;
use App\Http\Requests\UpdateAutosRequest;
use App\Repositories\AutosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AutosController extends AppBaseController
{
    /** @var  AutosRepository */
    private $autosRepository;

    public function __construct(AutosRepository $autosRepo)
    {
        $this->autosRepository = $autosRepo;
    }

    /**
     * Display a listing of the Autos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->autosRepository->pushCriteria(new RequestCriteria($request));
        $autos = $this->autosRepository->all();

        return view('autos.index')
            ->with('autos', $autos);
    }

    /**
     * Show the form for creating a new Autos.
     *
     * @return Response
     */
    public function create()
    {
        return view('autos.create');
    }

    /**
     * Store a newly created Autos in storage.
     *
     * @param CreateAutosRequest $request
     *
     * @return Response
     */
    public function store(CreateAutosRequest $request)
    {
        $input = $request->all();

        for ($i = 1; $i <= 10; $i++) {
            if ($request->file('adjunto_'.$i)) {
                $file = $request->file('adjunto_' . $i);
                $nombreArchivo['adjunto_' . $i] = rand(0, 1000) .'-'. $file->getClientOriginalName();
                \Storage::disk('public')->put($nombreArchivo['adjunto_' . $i], \File::get($file));
            }
        }
        $input['adjunto_1'] = (isset($nombreArchivo['adjunto_1'])) ? $nombreArchivo['adjunto_1'] : null;
        $input['adjunto_2'] = (isset($nombreArchivo['adjunto_2'])) ? $nombreArchivo['adjunto_2'] : null;
        $input['adjunto_3'] = (isset($nombreArchivo['adjunto_3'])) ? $nombreArchivo['adjunto_3'] : null;
        $input['adjunto_4'] = (isset($nombreArchivo['adjunto_4'])) ? $nombreArchivo['adjunto_4'] : null;

        $autos = $this->autosRepository->create($input);

        Flash::success('Autos saved successfully.');

        return redirect(route('autos.index'));
    }

    /**
     * Display the specified Autos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $autos = $this->autosRepository->findWithoutFail($id);

        if (empty($autos)) {
            Flash::error('Autos not found');

            return redirect(route('autos.index'));
        }

        return view('autos.show')->with('autos', $autos);
    }

    /**
     * Show the form for editing the specified Autos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $autos = $this->autosRepository->findWithoutFail($id);

        if (empty($autos)) {
            Flash::error('Autos not found');

            return redirect(route('autos.index'));
        }

        return view('autos.edit')->with('autos', $autos);
    }

    /**
     * Update the specified Autos in storage.
     *
     * @param  int              $id
     * @param UpdateAutosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAutosRequest $request)
    {
        $autos = $this->autosRepository->findWithoutFail($id);

        if (empty($autos)) {
            Flash::error('Autos not found');

            return redirect(route('autos.index'));
        }

        for ($i = 1; $i <= 10; $i++) {
            if ($request->file('adjunto_'.$i)) {
                $file = $request->file('adjunto_' . $i);
                $nombreArchivo['adjunto_' . $i] = rand(0, 1000) .'-'. $file->getClientOriginalName();
                \Storage::disk('public')->put($nombreArchivo['adjunto_' . $i], \File::get($file));
            }
        }
        $request['adjunto_1'] = (isset($nombreArchivo['adjunto_1'])) ? $nombreArchivo['adjunto_1'] : null;
        $request['adjunto_2'] = (isset($nombreArchivo['adjunto_2'])) ? $nombreArchivo['adjunto_2'] : null;
        $request['adjunto_3'] = (isset($nombreArchivo['adjunto_3'])) ? $nombreArchivo['adjunto_3'] : null;
        $request['adjunto_4'] = (isset($nombreArchivo['adjunto_4'])) ? $nombreArchivo['adjunto_4'] : null;

        $autos = $this->autosRepository->update($request->all(), $id);

        Flash::success('Autos updated successfully.');

        return redirect(route('autos.index'));
    }

    /**
     * Remove the specified Autos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $autos = $this->autosRepository->findWithoutFail($id);

        if (empty($autos)) {
            Flash::error('Autos not found');

            return redirect(route('autos.index'));
        }

        $this->autosRepository->delete($id);

        Flash::success('Autos deleted successfully.');

        return redirect(route('autos.index'));
    }
}
