<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNotificacionUserRequest;
use App\Http\Requests\UpdateNotificacionUserRequest;
use App\Models\NotificacionUser;
use App\Repositories\NotificacionUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class NotificacionUserController extends AppBaseController
{
    /** @var  NotificacionUserRepository */
    private $notificacionUserRepository;

    public function __construct(NotificacionUserRepository $notificacionUserRepo)
    {
        $this->notificacionUserRepository = $notificacionUserRepo;
    }

    /**
     * Display a listing of the NotificacionUser.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->notificacionUserRepository->pushCriteria(new RequestCriteria($request));
        $notificacionUsersLeer = NotificacionUser::where('clientes_id', auth()->user()->id)->where('estatus', '0')->get();
        $notificacionUsersLeidas = NotificacionUser::where('clientes_id', auth()->user()->id)->where('estatus', '1')->get();

        return view('notificacion_users.index')
            ->with('notificacionUsersLeer', $notificacionUsersLeer)
            ->with('notificacionUsersLeidas', $notificacionUsersLeidas);
    }

    /**
     * Show the form for creating a new NotificacionUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('notificacion_users.create');
    }

    /**
     * Store a newly created NotificacionUser in storage.
     *
     * @param CreateNotificacionUserRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificacionUserRequest $request)
    {
        $input = $request->all();

        $notificacionUser = $this->notificacionUserRepository->create($input);

        Flash::success('Notificacion User saved successfully.');

        return redirect(route('notificacionUsers.index'));
    }

    /**
     * Display the specified NotificacionUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notificacionUser = $this->notificacionUserRepository->findWithoutFail($id);

        if (empty($notificacionUser)) {
            Flash::error('Notificacion User not found');

            return redirect(route('notificacionUsers.index'));
        }

        return view('notificacion_users.show')->with('notificacionUser', $notificacionUser);
    }

    /**
     * Show the form for editing the specified NotificacionUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notificacionUser = $this->notificacionUserRepository->findWithoutFail($id);

        if (empty($notificacionUser)) {
            Flash::error('Notificacion User not found');

            return redirect(route('notificacionUsers.index'));
        }

        return view('notificacion_users.edit')->with('notificacionUser', $notificacionUser);
    }

    /**
     * Update the specified NotificacionUser in storage.
     *
     * @param  int              $id
     * @param UpdateNotificacionUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificacionUserRequest $request)
    {
        $notificacionUser = $this->notificacionUserRepository->findWithoutFail($id);

        if (empty($notificacionUser)) {
            Flash::error('Notificacion User not found');

            return redirect(route('notificacionUsers.index'));
        }

        $notificacionUser = $this->notificacionUserRepository->update($request->all(), $id);

        Flash::success('Notificacion User updated successfully.');

        return redirect(route('notificacionUsers.index'));
    }

    /**
     * Remove the specified NotificacionUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $request = array('estatus'=>1);
        $notificacionUser = $this->notificacionUserRepository->findWithoutFail($id);
        $notificacionUser = $this->notificacionUserRepository->update($request, $id);
        Flash::success('La notificación ha sido leida.');

        return redirect(route('notificacionUsers.index'));
//        die($id);
//        $notificacionUser = $this->notificacionUserRepository->findWithoutFail($id);
//
//        if (empty($notificacionUser)) {
//            Flash::error('Notificacion User not found');
//
//            return redirect(route('notificacionUsers.index'));
//        }
//
//        $this->notificacionUserRepository->delete($id);
//
//        Flash::success('Notificacion User deleted successfully.');
//
//        return redirect(route('notificacionUsers.index'));
    }
}
