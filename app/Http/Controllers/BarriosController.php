<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBarriosRequest;
use App\Http\Requests\UpdateBarriosRequest;
use App\Models\Barrios;
use App\Repositories\BarriosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stripe\File;

class BarriosController extends AppBaseController
{
    /** @var  BarriosRepository */
    private $barriosRepository;

    public function __construct(BarriosRepository $barriosRepo)
    {
        $this->barriosRepository = $barriosRepo;
    }

    /**
     * Display a listing of the Barrios.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->barriosRepository->pushCriteria(new RequestCriteria($request));
        $barrios = $this->barriosRepository->all();

        return view('barrios.index')
            ->with('barrios', $barrios);
    }

    /**
     * Show the form for creating a new Barrios.
     *
     * @return Response
     */
    public function create()
    {
        return view('barrios.create');
    }

    /**
     * Store a newly created Barrios in storage.
     *
     * @param CreateBarriosRequest $request
     *
     * @return Response
     */
    public function store(CreateBarriosRequest $request)
    {
        $input = $request->all();

        for ($i = 1; $i <= 10; $i++) {
            if ($request->file('adjunto_'.$i)) {
                $file = $request->file('adjunto_' . $i);
                $nombreArchivo['adjunto_' . $i] = rand(0, 1000) .'-'. $file->getClientOriginalName();
                \Storage::disk('public')->put($nombreArchivo['adjunto_' . $i], \File::get($file));
            }
        }

        $barrio = new Barrios;
        $barrio->barrio = $request->barrio;
        $barrio->kilometro = $request->kilometro;
        $barrio->adjunto_1 = (isset($nombreArchivo['adjunto_1'])) ? $nombreArchivo['adjunto_1'] : null;
        $barrio->adjunto_2 = (isset($nombreArchivo['adjunto_2'])) ? $nombreArchivo['adjunto_2'] : null;
        $barrio->adjunto_3 = (isset($nombreArchivo['adjunto_3'])) ? $nombreArchivo['adjunto_3'] : null;
        $barrio->adjunto_4 = (isset($nombreArchivo['adjunto_4'])) ? $nombreArchivo['adjunto_4'] : null;
        $barrio->adjunto_5 = (isset($nombreArchivo['adjunto_5'])) ? $nombreArchivo['adjunto_5'] : null;
        $barrio->adjunto_6 = (isset($nombreArchivo['adjunto_6'])) ? $nombreArchivo['adjunto_6'] : null;
        $barrio->adjunto_7 = (isset($nombreArchivo['adjunto_7'])) ? $nombreArchivo['adjunto_7'] : null;
        $barrio->adjunto_8 = (isset($nombreArchivo['adjunto_8'])) ? $nombreArchivo['adjunto_8'] : null;
        $barrio->adjunto_9 = (isset($nombreArchivo['adjunto_9'])) ? $nombreArchivo['adjunto_9'] : null;
        $barrio->adjunto_10 = (isset($nombreArchivo['adjunto10'])) ? $nombreArchivo['adjunto_10'] : null;
        $barrio->save();

        Flash::success('Barrios guardado correctamente.');

        return redirect(route('barrios.index'));
    }

    /**
     * Display the specified Barrios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $barrios = $this->barriosRepository->findWithoutFail($id);

        if (empty($barrios)) {
            Flash::error('Barrios not found');

            return redirect(route('barrios.index'));
        }

        return view('barrios.show')->with('barrios', $barrios);
    }

    /**
     * Show the form for editing the specified Barrios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $barrios = $this->barriosRepository->findWithoutFail($id);

        if (empty($barrios)) {
            Flash::error('Barrios not found');

            return redirect(route('barrios.index'));
        }

        return view('barrios.edit')->with('barrios', $barrios);
    }

    /**
     * Update the specified Barrios in storage.
     *
     * @param  int              $id
     * @param UpdateBarriosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBarriosRequest $request)
    {
        $barrios = $this->barriosRepository->findWithoutFail($id);

        if (empty($barrios)) {
            Flash::error('Barrios not found');

            return redirect(route('barrios.index'));
        }

        $barrios = $this->barriosRepository->update($request->all(), $id);

        Flash::success('Barrios updated successfully.');

        return redirect(route('barrios.index'));
    }

    /**
     * Remove the specified Barrios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $barrios = $this->barriosRepository->findWithoutFail($id);

        if (empty($barrios)) {
            Flash::error('Barrios not found');

            return redirect(route('barrios.index'));
        }

        $this->barriosRepository->delete($id);

        Flash::success('Barrios deleted successfully.');

        return redirect(route('barrios.index'));
    }
}
