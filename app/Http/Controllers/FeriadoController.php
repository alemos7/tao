<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeriadoRequest;
use App\Http\Requests\UpdateFeriadoRequest;
use App\Repositories\FeriadoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Carbon\Carbon;

class FeriadoController extends AppBaseController
{
    /** @var  FeriadoRepository */
    private $feriadoRepository;

    public function __construct(FeriadoRepository $feriadoRepo)
    {
        $this->feriadoRepository = $feriadoRepo;
    }

    /**
     * Display a listing of the Feriado.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->feriadoRepository->pushCriteria(new RequestCriteria($request));
        $feriados = $this->feriadoRepository->all();

        return view('feriados.index')
            ->with('feriados', $feriados);
    }

    /**
     * Show the form for creating a new Feriado.
     *
     * @return Response
     */
    public function create()
    {
        return view('feriados.create');
    }

    /**
     * Store a newly created Feriado in storage.
     *
     * @param CreateFeriadoRequest $request
     *
     * @return Response
     */
    public function store(CreateFeriadoRequest $request)
    {
        $input = $request->all();

        $fecha = Carbon::createFromFormat('d/m/Y', $input['fecha']);
        $input['fecha'] = $fecha->format('Y-m-d');

        $feriado = $this->feriadoRepository->create($input);

        Flash::success('Feriado saved successfully.');

        return redirect(route('feriados.index'));
    }

    /**
     * Display the specified Feriado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $feriado = $this->feriadoRepository->findWithoutFail($id);

        if (empty($feriado)) {
            Flash::error('Feriado not found');

            return redirect(route('feriados.index'));
        }

        return view('feriados.show')->with('feriado', $feriado);
    }

    /**
     * Show the form for editing the specified Feriado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $feriado = $this->feriadoRepository->findWithoutFail($id);

        if (empty($feriado)) {
            Flash::error('Feriado not found');

            return redirect(route('feriados.index'));
        }

        return view('feriados.edit')->with('feriado', $feriado);
    }

    /**
     * Update the specified Feriado in storage.
     *
     * @param  int              $id
     * @param UpdateFeriadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeriadoRequest $request)
    {
        $feriado = $this->feriadoRepository->findWithoutFail($id);

        if (empty($feriado)) {
            Flash::error('Feriado not found');

            return redirect(route('feriados.index'));
        }

        $feriado = $this->feriadoRepository->update($request->all(), $id);

        Flash::success('Feriado updated successfully.');

        return redirect(route('feriados.index'));
    }

    /**
     * Remove the specified Feriado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $feriado = $this->feriadoRepository->findWithoutFail($id);

        if (empty($feriado)) {
            Flash::error('Feriado not found');

            return redirect(route('feriados.index'));
        }

        $this->feriadoRepository->delete($id);

        Flash::success('Feriado deleted successfully.');

        return redirect(route('feriados.index'));
    }
}
