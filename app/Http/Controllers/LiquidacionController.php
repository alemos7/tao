<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAutosRequest;
use App\Http\Requests\UpdateAutosRequest;
use App\Models\AdicionalesEficiencia;
use App\Models\Convenios;
use App\Models\Liquidaciones;
use App\Models\Prestamos;
use App\Models\Seguimiento;
use App\Models\SubOrden;
use App\Repositories\AutosRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\Models\AdicionalesChofer;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Carbon\Carbon;

class LiquidacionController extends AppBaseController
{
    /** @var  AutosRepository */
    private $autosRepository;

    /**
     * Display a listing of the Autos.
     *
     * @param Request $request
     * @return Response
     */
    public function liquidar()
    {
        $usuarios = User::get();
        return view('rrhh.liquidar', compact(
            'usuarios'
        ));
    }

    public function liquidacion(Request $request)
    {
        $mes = $request->mes;
        $periodo = $request->periodo;

        $mes0 = str_pad($request->mes, 2, '0', STR_PAD_LEFT);

        switch ($request->periodo) {
            case 1:
                $ini = '2020-'.$mes0.'-01';
                $fin = '2020-'.$mes0.'-15';
                break;
            case 2:
                $ini = '2020-'.$mes0.'-15';
                $dt = new Carbon('2020-'.$mes0.'-01');
                $fin = $dt->endOfMonth()->format('Y-m-d');
                break;
            case 3:
                $ini = '2020-'.$mes0.'-01';
                $dt = new Carbon('2020-'.$mes0.'-01');
                $fin = $dt->endOfMonth()->format('Y-m-d');
                break;
        }

        $usuarios = User::get();
        $costoAC = AdicionalesChofer::select('adicional_costo')->first();


        $usuario = User::where('id', '=', $request->user)->first();

        $cantchofer = SubOrden::where('chofer_id', $request->user)
            ->where('fecha_inicio','>=',$ini)
            ->where('fecha_inicio','<=',$fin)
            ->count();

        $ordenes = Seguimiento::where('tipo', 1)
            ->where('user_id', $request->user)
            ->where('fecha','>=',$ini)
            ->where('fecha','<=',$fin)
            ->get();

        $llegadastarde = DB::table('ordenes_suborden_seguimiento AS S')
            ->join('ordenes_suborden AS SO', 'S.suborden_id', 'SO.id')
            ->where('S.tipo', 1)
            ->where('S.user_id', $request->user)
            ->where('S.fecha','>=',$ini)
            ->where('S.fecha','<=',$fin)
            ->whereRaw('(TIME(SO.hora_inicio)<S.hora)')
            ->get();

        $cantModulos = DB::table('ordenes_suborden_seguimiento AS S')
            ->select('SOD.cantidad')
            ->join('ordenes_suborden AS SO', 'S.suborden_id', 'SO.id')
            ->join('ordenes_suborden_detalle AS SOD', 'SO.id', 'SOD.suborden_id')
            ->where('S.tipo', 1)
            ->where('S.user_id', $request->user)
            ->where('SO.fecha_inicio','>=',$ini)
            ->where('SO.fecha_inicio','<=',$fin)
            ->sum('cantidad');

        $adicionalEficiencia = AdicionalesEficiencia::where('convenio_id', $usuario->convenio_id)
            ->where('minimo','<=',$cantModulos)
            ->where('maximo','>=',$cantModulos)
            ->first();

        $totalPrestamos = DB::table('prestamos AS P')
            ->select('id','fecha','cuotas','pendientes')
            ->selectRaw('(P.monto / cuotas) AS monto_cuota, ((cuotas - pendientes)+1) AS nro_cuota')
            ->where('user_id',$request->user)
            ->where('pendientes','>',0)
            ->get();

        $horasExtras = DB::table('ordenes_suborden_seguimiento AS S')
            ->select('C.hora_extra AS monto_hora_extra','C.salida', 'S.hora AS salio')
            ->selectRaw('HOUR(TIMEDIFF(S.hora, C.salida)) as horas_extras')
            ->join('users AS U', 'S.user_id', 'U.id')
            ->join('convenios AS C', 'U.convenio_id', 'C.id')
            ->where('S.tipo', 2)
            ->where('S.user_id', $request->user)
            ->where('S.fecha','>=',$ini)
            ->where('S.fecha','<=',$fin)
            ->whereRaw('(TIME(C.salida)<S.hora)')
            ->get();

        $trabajoFeriados = DB::table('ordenes_suborden_seguimiento AS S')
            ->selectRaw('(S.horas_trabajadas * C.hora_extra) as horas_extras')
            ->join('users AS U', 'S.user_id', 'U.id')
            ->join('convenios AS C', 'U.convenio_id', 'C.id')
            ->join('feriados AS F', 'S.fecha', 'F.fecha')
            ->where('S.tipo', 2)
            ->where('S.user_id', $request->user)
            ->where('S.fecha','>=',$ini)
            ->where('S.fecha','<=',$fin)
            ->get();


        return view('rrhh.liquidacion', compact('usuarios', 'usuario', 'costoAC', 'cantchofer', 'ordenes', 'llegadastarde', 'cantModulos', 'adicionalEficiencia', 'totalPrestamos', 'horasExtras', 'mes', 'periodo', 'trabajoFeriados'));
    }

    public function storeLiquidacion(Request $request){
        $input = $request->all();
        $input['fecha'] = date("Y-m-d");

        $prestamos = json_decode($input['idsPrestamos']);

        if(count($prestamos)>0){
            foreach ($prestamos AS $prestamo){
                $p = Prestamos::where('id',$prestamo->id)->first();
                $p->pendientes = $p->pendientes - 1;
                $p->save();
            }
        }

        Liquidaciones::create($input);

        Flash::success('Liquidaciones Guardadas.');

        return redirect(route('rrhh.liquidar'));
    }
}
