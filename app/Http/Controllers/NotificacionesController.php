<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNotificacionesRequest;
use App\Http\Requests\UpdateNotificacionesRequest;
use App\Models\NotificacionUser;
use App\Repositories\NotificacionesRepository;
use App\Http\Controllers\AppBaseController;
use App\Roles;
use App\RoleUser;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class NotificacionesController extends AppBaseController
{
    /** @var  NotificacionesRepository */
    private $notificacionesRepository;

    public function __construct(NotificacionesRepository $notificacionesRepo)
    {
        $this->notificacionesRepository = $notificacionesRepo;
    }

    /**
     * Display a listing of the Notificaciones.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->notificacionesRepository->pushCriteria(new RequestCriteria($request));
        $notificaciones = $this->notificacionesRepository->all();

        return view('notificaciones.index')
            ->with('notificaciones', $notificaciones);
    }

    /**
     * Show the form for creating a new Notificaciones.
     *
     * @return Response
     */
    public function create()
    {
        return view('notificaciones.create');
    }

    /**
     * Store a newly created Notificaciones in storage.
     *
     * @param CreateNotificacionesRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificacionesRequest $request)
    {
        $input = $request->all();
        $notificaciones = $this->notificacionesRepository->create($input);
        $this->insertNotificacionesUser($input['permission_role_id'], $notificaciones->id);
        Flash::success('Notificaciones saved successfully.');
        return redirect(route('notificaciones.index'));
    }

    public function insertNotificacionesUser($rol_id, $notificaciones_id)
    {
        if ($rol_id == "999") {
            $roles = Roles::all();
            foreach ($roles as $rol) {
                $rolUsers = RoleUser::where('role_id', $rol->id)->get();
                foreach ($rolUsers as $rolUser) {
                    $notificacionUser = new NotificacionUser;
                    $notificacionUser->notificaciones_id = $notificaciones_id;
                    $notificacionUser->clientes_id = $rolUser->user_id;
                    $notificacionUser->estatus = 1;
                    $notificacionUser->save();
                }
            }
        }else{
            $rolUsers = RoleUser::where('role_id', $rol_id)->get();
            foreach ($rolUsers as $rolUser) {
                $notificacionUser = new NotificacionUser;
                $notificacionUser->notificaciones_id = $notificaciones_id;
                $notificacionUser->clientes_id = $rolUser->user_id;
                $notificacionUser->estatus = 1;
                $notificacionUser->save();
            }
        }
    }

    /**
     * Display the specified Notificaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notificaciones = $this->notificacionesRepository->findWithoutFail($id);

        if (empty($notificaciones)) {
            Flash::error('Notificaciones not found');

            return redirect(route('notificaciones.index'));
        }

        return view('notificaciones.show')->with('notificaciones', $notificaciones);
    }

    /**
     * Show the form for editing the specified Notificaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notificaciones = $this->notificacionesRepository->findWithoutFail($id);

        if (empty($notificaciones)) {
            Flash::error('Notificaciones not found');

            return redirect(route('notificaciones.index'));
        }

        return view('notificaciones.edit')->with('notificaciones', $notificaciones);
    }

    /**
     * Update the specified Notificaciones in storage.
     *
     * @param  int              $id
     * @param UpdateNotificacionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificacionesRequest $request)
    {
        $notificaciones = $this->notificacionesRepository->findWithoutFail($id);

        if (empty($notificaciones)) {
            Flash::error('Notificaciones not found');

            return redirect(route('notificaciones.index'));
        }

        $notificaciones = $this->notificacionesRepository->update($request->all(), $id);

        Flash::success('Notificaciones updated successfully.');

        return redirect(route('notificaciones.index'));
    }

    /**
     * Remove the specified Notificaciones from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $notificaciones = $this->notificacionesRepository->findWithoutFail($id);

        if (empty($notificaciones)) {
            Flash::error('Notificaciones not found');

            return redirect(route('notificaciones.index'));
        }

        $this->notificacionesRepository->delete($id);

        Flash::success('Notificaciones deleted successfully.');

        return redirect(route('notificaciones.index'));
    }
}
