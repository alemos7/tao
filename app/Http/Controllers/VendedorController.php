<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVendedorRequest;
use App\Http\Requests\UpdateVendedorRequest;
use App\Models\Clientes;
use App\Models\Vendedor;
use App\Repositories\VendedorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class VendedorController extends AppBaseController
{
    /** @var  VendedorRepository */
    private $vendedorRepository;

    public function __construct(VendedorRepository $vendedorRepo)
    {
        $this->vendedorRepository = $vendedorRepo;
    }

    /**
     * Display a listing of the Vendedor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vendedorRepository->pushCriteria(new RequestCriteria($request));
        $vendedors = $this->vendedorRepository->all();

        return view('vendedors.index')
            ->with('vendedors', $vendedors);
    }

    /**
     * Show the form for creating a new Vendedor.
     *
     * @return Response
     */
    public function create()
    {
        $clientes = Clientes::all();

        return view('vendedors.create', compact('clientes'));
    }

    /**
     * Store a newly created Vendedor in storage.
     *
     * @param CreateVendedorRequest $request
     *
     * @return Response
     */
    public function store(CreateVendedorRequest $request)
    {
        $input = $request->all();

        $vendedor = $this->vendedorRepository->create($input);

        Flash::success('Vendedor guardado satisfactoriamente.');

        return redirect(route('vendedors.index'));
    }

    /**
     * Display the specified Vendedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor not found');

            return redirect(route('vendedors.index'));
        }

        return view('vendedors.show')->with('vendedor', $vendedor);
    }

    /**
     * Show the form for editing the specified Vendedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clientes = Clientes::all();
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor not found');

            return redirect(route('vendedors.index'));
        }

        return view('vendedors.edit', compact('vendedor','clientes'));
    }

    /**
     * Update the specified Vendedor in storage.
     *
     * @param  int              $id
     * @param UpdateVendedorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVendedorRequest $request)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor not found');

            return redirect(route('vendedors.index'));
        }

        $vendedor = $this->vendedorRepository->update($request->all(), $id);

        Flash::success('Vendedor guardado satisfactoriamente.');

        return redirect(route('vendedors.index'));
    }

    /**
     * Remove the specified Vendedor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor not found');

            return redirect(route('vendedors.index'));
        }

        $this->vendedorRepository->delete($id);

        Flash::success('Vendedor eliminado satisfactoriamente');

        return redirect(route('vendedors.index'));
    }

    #Busca los vendedores de un Cliente
    public function searchVendedores($cliente_id)
    {
        return Vendedor::where('cliente_id','=',$cliente_id)->orderBy('vendedor')->get();
    }
}
