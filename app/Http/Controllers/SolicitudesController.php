<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSolicitudesRequest;
use App\Http\Requests\UpdateSolicitudesRequest;
use App\Repositories\SolicitudesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SolicitudesController extends AppBaseController
{
    /** @var  SolicitudesRepository */
    private $solicitudesRepository;

    public function __construct(SolicitudesRepository $solicitudesRepo)
    {
        $this->solicitudesRepository = $solicitudesRepo;
    }

    /**
     * Display a listing of the Solicitudes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->solicitudesRepository->pushCriteria(new RequestCriteria($request));
        $solicitudes = $this->solicitudesRepository->all();

        return view('solicitudes.index')
            ->with('solicitudes', $solicitudes);
    }

    /**
     * Show the form for creating a new Solicitudes.
     *
     * @return Response
     */
    public function create()
    {
        return view('solicitudes.create');
    }

    /**
     * Store a newly created Solicitudes in storage.
     *
     * @param CreateSolicitudesRequest $request
     *
     * @return Response
     */
    public function store(CreateSolicitudesRequest $request)
    {
        $input = $request->all();

        $solicitudes = $this->solicitudesRepository->create($input);

        Flash::success('Solicitudes guardado satisfactoriamente.');

        return redirect(route('solicitudes.index'));
    }

    /**
     * Display the specified Solicitudes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $solicitudes = $this->solicitudesRepository->findWithoutFail($id);

        if (empty($solicitudes)) {
            Flash::error('Solicitudes not found');

            return redirect(route('solicitudes.index'));
        }

        return view('solicitudes.show')->with('solicitudes', $solicitudes);
    }

    /**
     * Show the form for editing the specified Solicitudes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $solicitudes = $this->solicitudesRepository->findWithoutFail($id);

        if (empty($solicitudes)) {
            Flash::error('Solicitudes not found');

            return redirect(route('solicitudes.index'));
        }

        return view('solicitudes.edit')->with('solicitudes', $solicitudes);
    }

    /**
     * Update the specified Solicitudes in storage.
     *
     * @param  int              $id
     * @param UpdateSolicitudesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSolicitudesRequest $request)
    {
        $solicitudes = $this->solicitudesRepository->findWithoutFail($id);

        if (empty($solicitudes)) {
            Flash::error('Solicitudes not found');

            return redirect(route('solicitudes.index'));
        }

        $solicitudes = $this->solicitudesRepository->update($request->all(), $id);

        Flash::success('Solicitudes guardado satisfactoriamente.');

        return redirect(route('solicitudes.index'));
    }

    /**
     * Remove the specified Solicitudes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $solicitudes = $this->solicitudesRepository->findWithoutFail($id);

        if (empty($solicitudes)) {
            Flash::error('Solicitudes not found');

            return redirect(route('solicitudes.index'));
        }

        $this->solicitudesRepository->delete($id);

        Flash::success('Solicitudes eliminado satisfactoriamente.');

        return redirect(route('solicitudes.index'));
    }
}
