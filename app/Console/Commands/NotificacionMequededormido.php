<?php

namespace App\Console\Commands;

use App\Mail\MailDormido;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NotificacionMequededormido extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notificacion_mequededormido';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificacion que se le envía al personal un día antes de una suborden';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subordenes = SubOrden::where('fecha_inicio',date("Y-m-d"))->get();

        foreach ($subordenes AS $suborden){

            if($suborden->tecnicos1->email){
                $email = $suborden->tecnicos1->email;
                Mail::to($email)->send(new MailDormido($suborden));
            }

            if($suborden->tecnicos2->email){
                $email = $suborden->tecnicos2->email;
                Mail::to($email)->send(new MailDormido($suborden));
            }

            if($suborden->tecnicos3->email){
                $email = $suborden->tecnicos3->email;
                Mail::to($email)->send(new MailDormido($suborden));
            }

            if($suborden->tecnicos4->email){
                $email = $suborden->tecnicos4->email;
                Mail::to($email)->send(new MailDormido($suborden));
            }
        }
    }
}
