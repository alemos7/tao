<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Recibos
 * @package App\Models
 * @version February 1, 2020, 11:07 am EST
 *
 * @property integer cliente_id
 * @property string fecha
 * @property number monto
 */
class Recibos extends Model
{
    use SoftDeletes;

    public $table = 'recibos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cliente_id',
        'fecha',
        'monto',
        'suborden_id',
        'total_suborden',
        'pago_suborden'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'fecha' => 'date',
        'monto' => 'float',
        'suborden_id' => 'integer',
        'total_suborden' => 'float',
        'pago_suborden' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'cliente_id' => 'required',
        'monto' => 'required|min:0.1|float'
    ];

    public function clientes(){
        return $this->belongsTo(Clientes::class, 'cliente_id', 'id');
    }

}
