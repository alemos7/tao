<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdicionalesEficiencia
 * @package App\Models
 * @version February 28, 2020, 9:53 pm EST
 *
 * @property integer convenio_id
 * @property integer minimo
 * @property integer maximo
 * @property number adicional_costo
 */
class AdicionalesEficiencia extends Model
{
    use SoftDeletes;

    public $table = 'adicionales_eficiencia';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'convenio_id',
        'minimo',
        'maximo',
        'adicional_costo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'convenio_id' => 'integer',
        'minimo' => 'integer',
        'maximo' => 'integer',
        'adicional_costo' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'convenio_id' => 'required',
        'minimo' => 'required',
        'maximo' => 'required',
        'adicional_costo' => 'required'
    ];

    
}
