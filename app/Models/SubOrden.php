<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubOrden extends Model
{
    public $table = 'ordenes_suborden';

    public $fillable = [
        'orden_id',
        'solicitud_id',
        'estado_id',
        'tipo_id',
        'productos_categorias_id',
        'tecnico_id1',
        'tecnico_id2',
        'tecnico_id3',
        'tecnico_id4',
        'chofer_id',
        'caja_id',
        'fecha_inicio',
        'hora_inicio',
        'duracion',
        'observaciones',
        'pagada',
        'auto_id',
        'pendiente',
        'adjunto_1',
        'adjunto_2',
        'adjunto_3',
        'adjunto_4',
        'informe',
    ];

    #Funciones para dar formato a fechas
    public function getFechaInicioESPAttribute()
    {
        return \Carbon\Carbon::parse($this->fecha_inicio)->formatLocalized('%d/%m/%Y');
    }

    #Funciones para las Relaciones con Tablas Foraneas
    public function tipos(){
        return $this->belongsTo('App\Models\Tipos', 'tipo_id', 'id');
    }

    public function solicitudes(){
        return $this->belongsTo('App\Models\Solicitudes', 'solicitud_id', 'id');
    }

    public function estados(){
        return $this->belongsTo('App\Models\Estados', 'estado_id', 'id');
    }

    public function categoriasProductos(){
        return $this->belongsTo(ProductosCategorias::class, 'productos_categorias_id', 'id');
    }

    public function ordenes(){
        return $this->belongsTo(Ordenes::class, 'orden_id', 'id');
    }

    public function tecnicos1(){
        return $this->belongsTo(User::class, 'tecnico_id1', 'id');
    }
    public function tecnicos2(){
        return $this->belongsTo(User::class, 'tecnico_id2', 'id');
    }
    public function tecnicos3(){
        return $this->belongsTo(User::class, 'tecnico_id3', 'id');
    }
    public function tecnicos4(){
        return $this->belongsTo(User::class, 'tecnico_id4', 'id');
    }
    public function auto(){
        return $this->belongsTo(Autos::class, 'auto_id', 'id');
    }

    public static function totalSuborden($suborden_id)
    {

        $res = SubOrdenDetalle::select(DB::raw('SUM(subtotal) as totalSuborden'))
            ->where('suborden_id', $suborden_id)
            ->groupBy('suborden_id')
            ->first();
        if(isset($res->totalSuborden)){
            return $res->totalSuborden;
        }
        return 0;
    }

    public static function searchUltSeguimiento($suborden_id){

        $seguimientoStatus =  Seguimiento::where('suborden_id', $suborden_id)
                                        ->orderBy('id', 'DESC')
                                        ->first();

        return $seguimientoStatus['tipo'];
    }
}
