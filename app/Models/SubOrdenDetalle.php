<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SubOrdenDetalle
 * @package App\Models
 * @version January 10, 2020, 11:20 pm EST
 *
 * @property integer suborden_id
 * @property integer producto_id
 * @property number cantidad
 * @property number valor
 */
class SubOrdenDetalle extends Model
{
    public $table = 'ordenes_suborden_detalle';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'suborden_id',
        'producto_id',
        'cantidad',
        'valor',
        'subtotal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'suborden_id' => 'integer',
        'producto_id' => 'integer',
        'cantidad' => 'float',
        'valor' => 'float',
        'subtotal' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'suborden_id' => 'required'
    ];

    #Funciones para las Relaciones con Tablas Foraneas
    public function productos(){
        return $this->belongsTo('App\Models\Productos', 'producto_id', 'id');
    }
}
