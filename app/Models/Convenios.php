<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Convenios
 * @package App\Models
 * @version February 28, 2020, 7:08 pm EST
 *
 * @property string convenio
 * @property number valor_cobrado
 * @property integer periodo
 * @property time entrada
 * @property time salida
 * @property time total_horas
 */
class Convenios extends Model
{
    use SoftDeletes;

    public $table = 'convenios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'convenio',
        'valor_cobrado',
        'hora_extra',
        'periodo',
        'entrada',
        'salida',
        'total_horas'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'convenio' => 'string',
        'valor_cobrado' => 'float',
        'hora_extra' => 'float',
        'periodo' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'convenio' => 'required',
        'valor_cobrado' => 'required',
        'hora_extra' => 'required',
        'periodo' => 'required',
        'entrada' => 'required',
        'salida' => 'required',
        'total_horas' => 'required'
    ];

    
}
