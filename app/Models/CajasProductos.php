<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CajasProductos
 * @package App\Models
 * @version January 28, 2020, 4:07 pm EST
 *
 * @property integer caja_id
 * @property string producto
 */
class CajasProductos extends Model
{
    use SoftDeletes;

    public $table = 'cajas_productos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'caja_id',
        'producto_id',
        'cantidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'caja_id' => 'integer',
        'producto_id' => 'integer',
        'cantidad' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'caja_id' => 'required',
        'producto_id' => 'required'
    ];


    public function productos(){
        return $this->belongsTo(Herramientas::class, 'producto_id', 'id');
    }
}
