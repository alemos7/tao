<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Autos
 * @package App\Models
 * @version January 28, 2020, 4:06 pm EST
 *
 * @property string autos
 */
class Autos extends Model
{
    use SoftDeletes;

    public $table = 'autos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'autos'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'autos' => 'string',
        'adjunto_1' => 'string',
        'adjunto_2' => 'string',
        'adjunto_3' => 'string',
        'adjunto_4' => 'string',
        'observacion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'autos' => 'required'
    ];


}
