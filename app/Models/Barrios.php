<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Barrios
 * @package App\Models
 * @version April 9, 2020, 1:24 pm EDT
 *
 * @property string barrio
 * @property string kilometro
 * @property string adjunto_1
 * @property string adjunto_2
 * @property string adjunto_3
 * @property string adjunto_4
 * @property string adjunto_5
 * @property string adjunto_6
 * @property string adjunto_7
 * @property string adjunto_8
 * @property string adjunto_9
 * @property string adjunto_10
 * @property string adjunto_11
 */
class Barrios extends Model
{
    use SoftDeletes;

    public $table = 'barrios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'barrio',
        'kilometro',
        'adjunto_1',
        'adjunto_2',
        'adjunto_3',
        'adjunto_4',
        'adjunto_5',
        'adjunto_6',
        'adjunto_7',
        'adjunto_8',
        'adjunto_9',
        'adjunto_10',
        'adjunto_11'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'barrio' => 'string',
        'kilometro' => 'string',
        'adjunto_1' => 'string',
        'adjunto_2' => 'string',
        'adjunto_3' => 'string',
        'adjunto_4' => 'string',
        'adjunto_5' => 'string',
        'adjunto_6' => 'string',
        'adjunto_7' => 'string',
        'adjunto_8' => 'string',
        'adjunto_9' => 'string',
        'adjunto_10' => 'string',
        'adjunto_11' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'barrio' => 'required'
    ];

    
}
