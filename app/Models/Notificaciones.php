<?php

namespace App\Models;

//use App\Roles;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Notificaciones
 * @package App\Models
 * @version March 17, 2020, 8:34 pm EDT
 *
 * @property string comentario
 * @property string|\Carbon\Carbon fecha_desde
 * @property string|\Carbon\Carbon fecha_hasta
 * @property integer permission_role_id
 * @property integer estatus
 */
class Notificaciones extends Model
{
    use SoftDeletes;

    public $table = 'notificaciones';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'comentario',
        'fecha_desde',
        'fecha_hasta',
        'permission_role_id',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'comentario' => 'string',
        'fecha_desde' => 'datetime',
        'fecha_hasta' => 'datetime',
        'permission_role_id' => 'integer',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function rol()
    {
        return $this->belongsTo('App\Roles', 'permission_role_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'estatus');
    }

}
