<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ordenes
 * @package App\Models
 * @version January 5, 2020, 8:36 am EST
 *
 * @property integer cliente_id
 * @property integer tipos_id
 * @property string consumidor_nombre
 * @property string consumidor_tlf
 * @property string consumidor_email
 * @property string consumidor_address
 * @property string latitud
 * @property string longitud
 * @property integer consumidor_provincia_id
 * @property integer consumidor_localidad_id
 * @property integer barrio_id
 * @property integer auto_id
 * @property integer vendedor_id
 * @property integer tipo_obra_id
 * @property string observaciones
 * @property integer user_create_id

 */
class Ordenes extends Model
{
    use SoftDeletes;

    public $table = 'ordenes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'cliente_id',
        'tipos_id',
        'consumidor_nombre',
        'consumidor_tlf',
        'consumidor_email',
        'consumidor_address',
        'consumidor_provincia_id',
        'consumidor_localidad_id',
        'barrio_id',
        'latitud',
        'longitud',
        'auto_id',
        'vendedor_id',
        'tipo_obra_id',
        'observaciones',
        'user_create_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'tipos_id' => 'integer',
        'consumidor_nombre' => 'string',
        'consumidor_tlf' => 'string',
        'consumidor_email' => 'string',
        'consumidor_address' => 'string',
        'latitud' => 'string',
        'longitud' => 'string',
        'consumidor_provincia_id' => 'integer',
        'consumidor_localidad_id' => 'integer',
        'barrio_id' => 'integer',
        'auto_id' => 'integer',
        'vendedor_id' => 'integer',
        'tipo_obra_id' => 'integer',
        'observaciones' => 'string',
        'user_create_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'cliente_id' => 'required',
        'vendedor_id' => 'required',
        'tipo_obra_id' => 'required',
        'consumidor_nombre' => 'required',
        'consumidor_tlf' => 'required',
        'consumidor_email' => 'required',
        'consumidor_provincia_id' => 'required',
        'consumidor_localidad_id' => 'required',
        'barrio_id' => 'required',
        'consumidor_address' => 'required'
    ];

    #Funciones para las Relaciones con Tablas Foraneas
    public function clientes(){
        return $this->belongsTo('App\Models\Clientes', 'cliente_id', 'id');
    }

    public function provincias(){
        return $this->belongsTo('App\Estado', 'consumidor_provincia_id', 'id');
    }

    public function localidades(){
        return $this->belongsTo('App\Ciudad', 'consumidor_localidad_id', 'id');
    }

    public function barrios(){
        return $this->belongsTo('App\Barrios', 'barrio_id', 'id');
    }

    public function vendedores(){
        return $this->belongsTo('App\Models\Vendedor', 'vendedor_id', 'id');
    }

    public function tiposObras(){

        return $this->belongsTo('App\Models\TipoObras', 'tipo_obra_id', 'id');
    }

    public function subOrdenes(){
        return $this->hasMany('App\Models\SubOrden', 'orden_id', 'id');
    }

}
