<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prestamos
 * @package App\Models
 * @version March 18, 2020, 9:19 pm EDT
 *
 * @property integer user_id
 * @property number monto
 * @property string fecha
 * @property integer cuotas
 * @property integer pendientes
 */
class Prestamos extends Model
{
    use SoftDeletes;

    public $table = 'prestamos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'monto',
        'fecha',
        'cuotas',
        'pendientes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'monto' => 'float',
        'fecha' => 'date',
        'cuotas' => 'integer',
        'pendientes' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'monto' => 'required',
        'cuotas' => 'required',
    ];

    #Funciones para dar formato a fechas
    public function getFechaESPAttribute()
    {
        return \Carbon\Carbon::parse($this->fecha)->formatLocalized('%d/%m/%Y');
    }
}
