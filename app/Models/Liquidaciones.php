<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Liquidaciones
 * @package App\Models
 * @version March 17, 2020, 8:41 pm EDT
 *
 * @property string|\Carbon\Carbon fecha
 * @property string periodo
 * @property number monto
 * @property number premio
 * @property number extra
 */
class Liquidaciones extends Model
{
    use SoftDeletes;

    public $table = 'liquidaciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'fecha',
        'periodo',
        'mes',
        'sueldoBasico',
        'extraChofer',
        'extrasFeriados',
        'eficiencia',
        'cuotaPrestamos',
        'adicionalHorasExtras',
        'totalLiquidacion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
