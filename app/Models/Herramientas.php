<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Herramientas
 * @package App\Models
 * @version February 28, 2020, 7:07 am EST
 *
 * @property string herramienta
 */
class Herramientas extends Model
{
    use SoftDeletes;

    public $table = 'productos_herramientas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'herramienta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'herramienta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'herramienta' => 'required'
    ];

}
