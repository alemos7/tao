<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TipoObras
 * @package App\Models
 * @version January 5, 2020, 8:47 am EST
 *
 * @property string tipo_obra
 */
class TipoObras extends Model
{
    use SoftDeletes;

    public $table = 'tipo_obras';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tipo_obra',
        'coeficienteConstante'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo_obra' => 'string',
        'coeficienteConstante' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo_obra' => 'required',
        'coeficienteConstante' => 'required'
    ];

}
