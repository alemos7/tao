<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class IngresosEgresos
 * @package App\Models
 * @version February 29, 2020, 9:25 pm EST
 *
 * @property string concepto
 * @property number monto
 * @property integer periocidad
 * @property string tipo
 */
class IngresosEgresos extends Model
{
    use SoftDeletes;

    public $table = 'ingreso_egreso';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'concepto',
        'monto',
        'periocidad',
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'concepto' => 'string',
        'monto' => 'float',
        'periocidad' => 'integer',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'concepto' => 'required',
        'monto' => 'required',
        'periocidad' => 'required',
        'tipo' => 'required'
    ];

    
}
