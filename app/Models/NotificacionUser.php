<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NotificacionUser
 * @package App\Models
 * @version March 17, 2020, 8:38 pm EDT
 *
 * @property integer notificaciones_id
 * @property integer clientes_id
 * @property integer estatus
 */
class NotificacionUser extends Model
{
    use SoftDeletes;

    public $table = 'notificacion_user';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'notificaciones_id',
        'clientes_id',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'notificaciones_id' => 'integer',
        'clientes_id' => 'integer',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function cliente()
    {
        return $this->belongsTo('App\User', 'clientes_id');
    }

    public function notificacion()
    {
        return $this->belongsTo('App\Models\Notificaciones', 'notificaciones_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'estatus');
    }

}
