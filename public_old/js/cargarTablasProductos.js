function fetchData() {
    "use strict"
    const subOrdenId = $('#subOrden_id').val();
    const categoriaId = $('#productos_categorias_id').val();
    const coeficienteConstante = $('#coeficienteConstante').val();
    const url = `/searchProductosXCategoria`;

    if(categoriaId) {
        $.ajax({
            type: 'GET',
            url: url,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                categoriaId: categoriaId,
                coeficienteConstante: coeficienteConstante,
                subOrdenId: subOrdenId
            },
            success: function (data) {
                $('#divTablaProductos').html(data);
            }
        });
    }
}

$(function () {
    "use strict"
    fetchData();

    $('#productos_categorias_id').change(fetchData);
});