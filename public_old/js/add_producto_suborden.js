function redondeo(numero, decimales) {
    var flotante = parseFloat(numero);
    var resultado = Math.round(flotante * Math.pow(10, decimales)) / Math.pow(10, decimales);
    return resultado;
}

$("#tablaProductos").on("click", ".btnDelItem", function () {
    $('#totalItems').val(parseInt($('#totalItems').val()) - 1);
    $(this).closest('tr').remove();
    recalcularTotales();
});

function recalcularTotales() {
    let totalInstalacion = 0;
    let totalCantidad = 0;

    $(".itemsJSON").each(function () {

        let jsonItem = JSON.parse($(this).val());
        totalInstalacion += jsonItem.subtotal;
        totalCantidad += jsonItem.cantidad;
    });

    let totalDuracion = Math.ceil(totalCantidad / 24);
    $('#duracion').val(totalDuracion);

    // Dibujando en Vista
    $('#tdTotalInstalacion').html(`<h3>${totalInstalacion}</h3>`);
    $('#totalInstalacion').val(totalInstalacion);
}

function verificarTotal(totalInstalacion) {
    $(".itemsJSON").each(function () {
        let jsonItem = JSON.parse($(this).val());
        totalInstalacion += parseFloat(jsonItem.subtotal);
    });

    return totalInstalacion;
}

$(document).ready(function () {

    $("#tablaProductos").on("click", ".btnAddProducto", function () {

        let montoFinanciado = parseFloat($('#montoFinanciado').val());
        let productoId = $(this).data('productoid');
        let cantidad = parseInt($('#cantidad' + productoId).val());

        if (montoFinanciado <= 0) {
            $('#cantidad' + productoId).val('')
            $('#msg').html('<div class="alert alert-danger text-center">El monto de Financiamiento para este Cliente es Insuficiente.</div>');

            $('#estado_id').val('1');

        }
        if (productoId != '' && cantidad > 0) {
            $('#totalItems').val(parseInt($('#totalItems').val()) + 1);
            $('#msg').html('');

            let codigo = $(this).data('codigo');
            let producto = $(this).data('producto');
            let valor = $(this).data('valorproducto');
            let subTotal = redondeo(cantidad * valor, 2);

            let saldo = parseFloat(verificarTotal(subTotal));

            if (montoFinanciado <= saldo) {
                $('#cantidad' + productoId).val('')
                $('#msg').html('<div class="alert alert-danger text-center">El monto de Financiamiento para este Cliente es Insuficiente.</div>');
                $('#estado_id').val('1');

            }
            let items = new Object();
            items.productoId = productoId;
            items.cantidad = cantidad;
            items.valor = valor;
            items.subtotal = subTotal;

            let filaItem = `<tr>
                                <td>${codigo}</td>
                                <td>${producto}</td>
                                <td align="right">${cantidad}</td>
                                <td align="right">${valor}</td>
                                <td align="right">${subTotal}</td>
                                <td align="center">
                                    <input type="hidden" name="items[]" class="itemsJSON" value='${JSON.stringify(items)}'>
                                    <button class="btn btn-outline-danger btn-sm btnDelItem" type="button" title="Quitar Item">
                                        <i class="fas fa-trash-alt fa-2x"></i>
                                    </button>                                            
                                </td>
                            </tr>`;

            $('#tablaDetalles tbody tr:last').after(filaItem);

            recalcularTotales();


        } else {
            $('#msg').html('<div class="alert alert-danger text-center">Debe indicar la cantidad para el producto deseado.</div>');
        }

    });
});