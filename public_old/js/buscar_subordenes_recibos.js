$('#cliente_id').change(function () {
    let cliente_id = $('#cliente_id').val();
    // let fecha = moment($('#fecha').val().split("/").reverse().join("-")).format('YYYY-MM-DD');

    if (cliente_id) {
        const url = `/searchXfechaXclienteSinVencer/${cliente_id}`;
        $.get(url, data => {

            if (data.sinperiodo) {
                $('#tablaSubOrdenes tbody').html('');
                $('#monto').val(0);
                $('#msg').html('<div class="alert alert-info">Este Cliente no posee un Perdiodo de Facturación establecido.</div>');
            } else {
                if (data.tabla) {
                    $('#tablaSubOrdenes tbody').html(data.tabla);
                    $('#monto').val(data.totalOrdenes);
                    $('#msg').html('');
                } else {
                    $('#tablaSubOrdenes tbody').html('');
                    $('#monto').val(0);
                    $('#msg').html('<div class="alert alert-info">Este Cliente no posee SubOrdenes Pendientes.</div>');
                }
            }
        });
    } else {
        $('#tablaSubOrdenes tbody').html('');
        $('#monto').val(0);
        $('#msg').html('<div class="alert alert-danger">Debe escoger un Cliente</div>');
    }

});