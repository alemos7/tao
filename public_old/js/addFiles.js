$("#btnAddFile").click(function () {
    let contadorFile = parseInt($('#contadorFile').val()) + 1;
    $('#contadorFile').val(contadorFile);
    let fila = `<tr>
                    <td>
                        <input type="file" name="file[]" class="form-control pb-5">
                    </td>
                    <td style="vertical-align: middle">
                        <button class="btn btn-outline-danger btn-sm btnDelFile" type="button">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                </tr>`;

    $('#tableFiles tbody tr:last').after(fila);
});

$("#tableFiles").on("click", ".btnDelFile", function () {
    let contadorFile = parseInt($('#contadorFile').val()) - 1;
    $('#contadorFile').val(contadorFile);
    $(this).closest('tr').remove();
});