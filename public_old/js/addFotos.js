$("#btnAddFoto").click(function () {
    let contadorFoto = parseInt($('#contadorFoto').val()) + 1;
    $('#contadorFoto').val(contadorFoto);
    let fila = `<tr>
                            <td>
                                <input type="file" name="foto${contadorFoto}" accept="image/png,image/jpeg,image/gif" class="form-control pb-5">
                            </td>
                            <td style="vertical-align: middle">
                                <button class="btn btn-outline-danger btn-sm btnDelFoto" type="button">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>`;

    $('#tableFotos tbody tr:last').after(fila);
});

$("#tableFotos").on("click", ".btnDelFoto", function () {
    $(this).closest('tr').remove();
});