function searchAgenda() {
    "use strict"
    const selectMes = $('#selectMes').val();
    const tipoId = $('#tipo_id').val();
    const url = `/searchAgenda/${selectMes}/${tipoId}`;

    if(selectMes && tipoId) {
        $.get(url, data => {
            $('#divAgenda').html(data);
        });
    }
}

$(function () {
    "use strict"

    searchAgenda();

    $('#btnFiltrar').click(searchAgenda);
});
