-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-02-2020 a las 22:40:51
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.30-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tao`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso_egreso`
--

CREATE TABLE `ingreso_egreso` (
  `id` int(11) NOT NULL,
  `concepto` varchar(150) NOT NULL,
  `monto` float(10,2) NOT NULL,
  `periocidad` int(2) NOT NULL,
  `tipo` char(1) NOT NULL COMMENT 'E=Entrada/S=Salidad',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingreso_egreso`
--

INSERT INTO `ingreso_egreso` (`id`, `concepto`, `monto`, `periocidad`, `tipo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Personal Limpieza', 500.00, 6, 'E', '2020-02-29 21:35:09', '2020-02-29 21:37:35', NULL),
(2, 'Servicio Electrico', 100.00, 30, 'E', '2020-02-29 21:37:53', '2020-02-29 21:37:53', NULL),
(3, 'Servicio de Agua', 120.00, 25, 'E', '2020-02-29 21:38:13', '2020-02-29 21:38:13', NULL),
(4, 'Pago Nomina', 5000.00, 15, 'E', '2020-02-29 21:38:37', '2020-02-29 21:38:37', NULL),
(5, 'Mantenimiento Mueble', 700.00, 12, 'I', '2020-02-29 21:39:28', '2020-02-29 21:39:28', NULL),
(6, 'Venta de Insumos', 1500.00, 15, 'I', '2020-02-29 21:39:59', '2020-02-29 21:39:59', NULL),
(7, 'Servicio de Asesoria', 250.00, 10, 'I', '2020-02-29 21:40:30', '2020-02-29 21:40:30', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ingreso_egreso`
--
ALTER TABLE `ingreso_egreso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ingreso_egreso`
--
ALTER TABLE `ingreso_egreso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
