ALTER TABLE `ordenes` ADD `barrio_id` INT NULL AFTER `longitud`;
ALTER TABLE `ordenes_suborden` ADD `adjunto_1` VARCHAR(250) NULL AFTER `auto_id`, ADD `adjunto_2` VARCHAR(250) NULL AFTER `adjunto_1`, ADD `adjunto_3` VARCHAR(250) NULL AFTER `adjunto_2`, ADD `adjunto_4` VARCHAR(250) NULL AFTER `adjunto_3`;
ALTER TABLE `ordenes_suborden` ADD `informe` TEXT NULL AFTER `adjunto_4`;
ALTER TABLE `autos` ADD `adjunto_1` VARCHAR(250) NULL AFTER `deleted_at`, ADD `adjunto_2` VARCHAR(250) NULL AFTER `adjunto_1`, ADD `adjunto_3` VARCHAR(250) NULL AFTER `adjunto_2`, ADD `adjunto_4` VARCHAR(250) NULL AFTER `adjunto_3`, ADD `observacion` TEXT NULL AFTER `adjunto_4`; 
