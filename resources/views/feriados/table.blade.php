<div class="table-responsive">
    <table class="table" id="feriados-table">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Nombre del Feriado</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($feriados as $feriado)
            <tr>
                <td>{{ $feriado->fecha }}</td>
                <td>{{ $feriado->nombre }}</td>
                <td class="td-actions text-center">
                    @can('feriados.show')
                        <a href="{{ route('feriados.show', [$feriado->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('feriados.edit')
                        <a href="{{ route('feriados.edit', [$feriado->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('feriados.destroy')
                        {!! Form::open(['route' => ['feriados.destroy', $feriado->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
