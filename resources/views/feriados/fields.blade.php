@section('css')
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

<div class="row">
    <!-- Fecha Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fecha', 'Fecha:') !!}
        {!! Form::text('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
    </div>

    <!-- Fecha Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nombre', 'Nombre del Feriado:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control','id'=>'nombre']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('feriados.index') }}" class="btn btn-default">Cancel</a>
    </div>
</div>


@section('js')
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/moment/moment.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>

    <script>

        $('#fecha').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            default: 'now',
            time: false
        });
    </script>

@endsection