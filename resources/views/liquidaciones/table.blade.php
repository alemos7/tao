<div class="table-responsive">
    <table class="table" id="liquidaciones-table">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Periodo</th>
            <th>Sueldo Basico</th>
            <th>Extra Chofer</th>
            <th>Eficiencia</th>
            <th>Cuota Prestamos</th>
            <th>Horas Extras</th>
            <th>Total Liquidacion</th>
        </tr>
        </thead>
        <tbody>
        @foreach($liquidaciones as $liquidacion)
            <tr>
                <td>{{ Carbon\Carbon::parse($liquidacion->fecha)->format('d/m/Y')  }}</td>
                <td>
                    @switch($liquidacion->periodo)
                        @case(1)
                        Primera Quincena
                        @break

                        @case(2)
                        Segunda Quincena
                        @break

                        @case(3)
                        Mes
                        @break

                    @endswitch
                    </td>
                <td>${{ $liquidacion->sueldoBasico }}</td>
                <td>${{ $liquidacion->extraChofer }}</td>
                <td>${{ $liquidacion->eficiencia }}</td>
                <td>${{ $liquidacion->cuotaPrestamos }}</td>
                <td>${{ $liquidacion->adicionalHorasExtras }}</td>
                <td>${{ $liquidacion->totalLiquidacion }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
