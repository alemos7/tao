<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $liquidaciones->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $liquidaciones->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $liquidaciones->updated_at }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $liquidaciones->fecha }}</p>
</div>

<!-- Periodo Field -->
<div class="form-group">
    {!! Form::label('periodo', 'Periodo:') !!}
    <p>{{ $liquidaciones->periodo }}</p>
</div>

<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{{ $liquidaciones->monto }}</p>
</div>

<!-- Premio Field -->
<div class="form-group">
    {!! Form::label('premio', 'Premio:') !!}
    <p>{{ $liquidaciones->premio }}</p>
</div>

<!-- Extra Field -->
<div class="form-group">
    {!! Form::label('extra', 'Extra:') !!}
    <p>{{ $liquidaciones->extra }}</p>
</div>

