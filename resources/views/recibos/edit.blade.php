@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Recibos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($recibos, ['route' => ['recibos.update', $recibos->id], 'method' => 'patch']) !!}

                        @include('recibos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection