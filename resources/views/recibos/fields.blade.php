@section('css')
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

<div class="row">
    <div class="col-sm-12">
        {!! Form::label('fecha', 'Fecha:') !!}
        <p>{{ date('d/m/Y') }}</p>
    </div>
</div>
<div class="row mt-4">
    <!-- Cliente Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('cliente_id', 'Cliente:') !!}
        <select class="form-control {{ $errors->has('cliente_id') ? ' is-invalid' : '' }}" name="cliente_id" id="cliente_id">
            <option value="">:: Seleccione::</option>
            @foreach($clientes AS $cliente)
                <option value="{{$cliente->id}}">{{$cliente->cliente}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover" id="tablaSubOrdenes">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Nº de Orden</th>
                <th>Nº de Suborden</th>
                <th>Monto de la Suborden</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-4">
        {!! Form::label('monto', 'Monto:') !!}
        <input type="text" name="monto" id="monto" class="form-control" value="{{ @old("monto") }}">
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <hr>
    {!! Form::submit('Guardar', ['class' => 'btn btn-info']) !!}
    <a href="{{ route('ordenes.index') }}" class="btn btn-default">Cancelar</a>
</div>

@section('js')
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/moment/moment.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/buscar_subordenes_recibos.js')}}"></script>

    <script>

        $('#fecha').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            'default': 'now',
            time: false
        });

    </script>
@endsection