@extends('layouts.app')
@section('nombre_modulo', 'Recibo de Pago')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active">Crear Recibo</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Crear Recibo de Pago
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'recibos.store', 'class' => 'w-100']) !!}
                        <div id="msg"></div>

                        @include('recibos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
