@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Recibos</h1>
{{--        <h1 class="pull-right">--}}
{{--           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('recibos.create') }}">Add New</a>--}}
{{--        </h1>--}}
        <a href="{{route('recibos.create')}}" class="btn btn-outline-success float-right">
            <i class="fas fa-plus"></i>
            Crear Recibos
        </a>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('recibos.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection
