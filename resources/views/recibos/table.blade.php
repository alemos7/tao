@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($recibos as $recibos)
            <tr>
                <td>{{ $recibos->clientes->cliente }}</td>
                <td>{{ Carbon\Carbon::parse($recibos->fecha)->format('d/m/Y') }}</td>
                <td>{{ number_format($recibos->pago_suborden,2,',','.') }}</td>
{{--                <td class="td-actions text-center">--}}
{{--                    @can('recibos.show')--}}
{{--                        <a href="{{ route('recibos.show', [$recibos->id]) }}" class="btn btn-outline-success btn-round btn-sm">--}}
{{--                            <i class="fas fa-eye"></i>--}}
{{--                        </a>--}}
{{--                    @endcan--}}
{{--                    --}}{{--@can('recibos.edit')--}}
{{--                        --}}{{--<a href="#" class="btn btn-outline-success btn-round btn-sm">--}}
{{--                            --}}{{--<i class="fas fa-edit"></i>--}}
{{--                        --}}{{--</a>--}}
{{--                    --}}{{--@endcan--}}
{{--                    --}}{{--@can('recibos.destroy')--}}
{{--                        --}}{{--{!! Form::open(['route' => ['recibos.destroy', $recibos->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}--}}
{{--                        --}}{{--<button type="button" class="btn btn-outline-success btn-round btn-sm">--}}
{{--                            --}}{{--<i class="fas fa-trash-alt"></i>--}}
{{--                        --}}{{--</button>--}}
{{--                        --}}{{--{!! Form::close() !!}--}}
{{--                    --}}{{--@endcan--}}
{{--                </td>--}}
                <td class="td-actions text-center">
                    <a href="{{ route('recibos.show', [$recibos->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('recibos.edit', [$recibos->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                        <i class="fas fa-edit"></i>
                    </a>
                    {!! Form::open(['route' => ['recibos.destroy', $recibos->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                    <button class="btn btn-outline-success btn-round btn-sm">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
