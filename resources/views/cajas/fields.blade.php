@section('css')
    <!-- Color picker plugins css -->
    <link href="{{asset('/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColorPicker-master/dist/css/asColorPicker.css')}}" rel="stylesheet">

    <style>
        .asColorPicker-trigger {
            display: none;
        }

        .asColorPicker-dropdown {
            max-width: max-content !important;
            top: 20px !important;
        }
    </style>
@endsection


<div class="row">
    <!-- Caja Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('caja', 'Caja:') !!}
        {!! Form::text('caja', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('color', 'Color:') !!}
        <div class="input-group asColorPicker-wrap">
            <input type="text" class="complex-colorpicker form-control asColorPicker-input" name="color" value="{{@old('color',$cajas->color)}}">
        </div>
    </div>
</div>

<h4 class="mt-4">Productos dentro de la Caja:</h4>
<hr class="hrgrueso mt-0">
<div id="msg"></div>
<table class="table table-hover" id="tablaCaja">
    <thead>
    <tr>
        <th class="col-sm-8">
            <div class="form-group">
                <label for="">Producto</label>
                <select name="producto_id" id="producto_id" class="form-control">
                    <option value="">:: Seleccione ::</option>
                    @foreach($herramientas AS $herramienta)
                        <option value="{{$herramienta->id}}">{{$herramienta->herramienta}}</option>
                    @endforeach
                </select>
            </div>
        </th>
        <th class="2">
            <div class="form-group">
                <label for="">Cant.</label>
                <input type="text" name="cantidad" id="cantidad" class="form-control">
            </div>
        </th>
        <th width="10%">
            <div class="form-group">
                <label for=""></label>
                <button class="btn btn-primary" type="button" id="btnAgregarPdto">Agregar</button>
            </div>
        </th>
    </tr>
    </thead>
    <tbody class="border-top-2">
    @if(isset($productosCaja))
        @foreach ($productosCaja as $producto)
            <?php
            $json = [
                'productoId' => $producto->producto_id,
                'cantidad' => $producto->cantidad
            ];
            ?>
            <tr>
                <td>{{$producto->productos->herramienta}}</td>
                <td>{{$producto->cantidad}}</td>
                <td align="center">
                    <input type="hidden" name="items[]" class="itemsJSON" value='{{json_encode($json)}}'>
                    <button class="btn btn-outline-danger btn-sm btnDelItem" type="button" title="Quitar Item">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                </td>
            </tr>
        @endforeach
    @else
        <tr></tr>
    @endif
    </tbody>
</table>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cajas.index') }}" class="btn btn-default">Cancel</a>
</div>

@section('js')

    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColor/dist/jquery-asColor.js')}}"></script>
    <script src="{{asset('/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asGradient/dist/jquery-asGradient.js')}}"></script>
    <script src="{{asset('/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>

    <script src="{{asset('js/add_producto_caja.js')}}"></script>

    <script>
        // Colorpicker
        $(".colorpicker").asColorPicker();
        $(".complex-colorpicker").asColorPicker({
            mode: 'complex'
        });
    </script>
@endsection

