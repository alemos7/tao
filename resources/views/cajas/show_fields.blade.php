<div class="row">

    <!-- Caja Field -->
    <div class="col-sm-6">
        <div class="form-group col-sm-12">
            {!! Form::label('caja', 'Caja:') !!}
        </div>

        <p>{{ $cajas->caja }}</p>
    </div>

    <!-- Id Field -->
    <div class="col-sm-6">
        <div class="form-group col-sm-12">
            {!! Form::label('color', 'Color:') !!}
        </div>

        <p>
        <table>
            <tr>
                <td>{{ $cajas->color }}</td>
                <td>
                    <span style="background-color: {{ $cajas->color }}; width: 30px; display: block">&nbsp;</span>
                </td>
            </tr>
        </table>
        </p>
    </div>

</div>