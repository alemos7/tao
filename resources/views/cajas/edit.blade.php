@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cajas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cajas, ['route' => ['cajas.update', $cajas->id], 'method' => 'patch', 'class' => 'w-100']) !!}

                        @include('cajas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection