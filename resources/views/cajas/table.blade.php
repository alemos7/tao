<div class="table-responsive">
    <table class="table" id="cajas-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Caja</th>
            <th>Color</th>
            <th>Productos</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cajas as $cajas)
            <tr>
                <td>{{ $cajas->id }}</td>
                <td>{{ $cajas->caja }}</td>
                <td>
                    <table>
                        <tr>
                            <td>{{ $cajas->color }}</td>
                            <td>
                                <span style="background-color: {{ $cajas->color }}; width: 30px; display: block">&nbsp;</span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>

                </td>
                <td class="td-actions text-center">
                    @can('cajas.show')
                        <a href="{{ route('cajas.show', [$cajas->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('cajas.edit')
                        <a href="{{ route('cajas.edit', [$cajas->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('cajas.destroy')
                        {!! Form::open(['route' => ['cajas.destroy', $cajas->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
