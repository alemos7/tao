@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cajas
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @include('cajas.show_fields')
                </div>
                    <div class="row">
                        <h4 class="mt-4">Productos dentro de la Caja:</h4>
                        <hr class="hrgrueso mt-0">
                        <div id="msg"></div>
                        <table class="table table-hover" id="tablaCaja">
                            <thead>
                            <tr>
                                <th class="col-sm-8">
                                    <div class="form-group">
                                        <label for="">Producto</label>
                                    </div>
                                </th>
                                <th class="2">
                                    <div class="form-group">
                                        <label for="">Cant.</label>

                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="border-top-2">
                            @foreach($herramientas AS $herramienta)
                                <tr>
                                    <td>
                                        {{$herramienta->productos->herramienta}}
                                    </td>
                                    <td>
                                        {{$herramienta->cantidad}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <a href="{{ route('cajas.index') }}" class="btn btn-default">Back</a>

            </div>
        </div>
    </div>
@endsection
