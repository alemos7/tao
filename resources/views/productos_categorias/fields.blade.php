<div class="row">
    <div class="col-sm-6">
        <!-- Codigo Field -->
        <div class="form-group">
            {!! Form::label('codigo', 'Codigo:') !!}
            {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <div class="col-sm-6">
        <!-- Categoria Field -->
        <div class="form-group">
            {!! Form::label('categoria', 'Categoria:') !!}
            {!! Form::text('categoria', null, ['class' => 'form-control']) !!}
        </div>

    </div>
</div>
<hr>
<div class="row mt-4 float-right">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('productosCategorias.index') }}" class="btn btn-success">Cancelar</a>
    </div>

</div>

