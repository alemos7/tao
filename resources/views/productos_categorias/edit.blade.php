@extends('layouts.app')
@section('nombre_modulo', 'Editar Categorias de Productos')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('productosCategorias.index')}}">Categorias de Productos</a></li>
    <li class="breadcrumb-item active">Editar Categorias de Productos</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Categorias de Producto# <b>{{str_pad($productosCategorias->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productosCategorias, ['route' => ['productosCategorias.update', $productosCategorias->id], 'class'=>'w-100', 'method' => 'patch']) !!}

                        @include('productos_categorias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection