@extends('layouts.app')
@section('nombre_modulo', 'Categorias de Productos')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('productosCategorias.index')}}">Categorias de Productos</a></li>
    <li class="breadcrumb-item active">Categoria de Producto</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-tag"></i>
            Categoria de Producto# <b>{{str_pad($productosCategorias->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="col-sm-12">
                        @include('productos_categorias.show_fields')
                        <hr>
                        <div class="row float-right">
                            <div class="col-sm-12">
                                <a href="{{ route('productosCategorias.index') }}" class="btn btn-success">Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
