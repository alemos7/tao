<!-- Codigo Field -->
<div class="form-group">
    <b>{!! Form::label('codigo', 'Codigo:') !!}</b>
    <p>{{ $productosCategorias->codigo }}</p>
</div>

<!-- Categoria Field -->
<div class="form-group">
    <b>{!! Form::label('categoria', 'Categoria:') !!}</b>
    <p>{{ $productosCategorias->categoria }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <b>{!! Form::label('created_at', 'Created At:') !!}</b>
    <p>{{ $productosCategorias->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <b>{!! Form::label('updated_at', 'Updated At:') !!}</b>
    <p>{{ $productosCategorias->updated_at }}</p>
</div>
