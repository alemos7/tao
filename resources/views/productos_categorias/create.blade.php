@extends('layouts.app')
@section('nombre_modulo', 'Categorias de Productos')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('productosCategorias.index')}}">Categorias de Productos</a></li>
    <li class="breadcrumb-item active"><a href="{{route('productosCategorias.create')}}">Crear Categorias de Productos</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-tag"></i>
            Crear Categorias de Productos
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'productosCategorias.store', 'class'=>'w-100']) !!}

                        @include('productos_categorias.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
