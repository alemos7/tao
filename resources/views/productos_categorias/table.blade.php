@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
        <tr>
            <th>Codigo</th>
            <th>Categoria</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($productosCategorias as $productosCategorias)
            <tr>
                <td>{{ $productosCategorias->codigo }}</td>
                <td>{{ $productosCategorias->categoria }}</td>

                <td class="td-actions text-center">
                    @can('productosCategorias.show')
                        <a href="{{ route('productosCategorias.show', [$productosCategorias->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('productosCategorias.edit')
                        <a href="{{ route('productosCategorias.edit', [$productosCategorias->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('productosCategorias.destroy')
                        {!! Form::open(['route' => ['productosCategorias.destroy', $productosCategorias->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection