<!-- Cliente Id Field -->
<div class="form-group">
    <b>{!! Form::label('cliente_id', 'Cliente:') !!}</b>
    <p>{{ $vendedor->clientes->cliente }}</p>
</div>

<!-- Vendedor Field -->
<div class="form-group">
    <b>{!! Form::label('vendedor', 'Vendedor:') !!}</b>
    <p>{{ $vendedor->vendedor }}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    <b>{!! Form::label('correo', 'Correo:') !!}</b>
    <p>{{ $vendedor->correo }}</p>
</div>

<!-- Teléfono Field -->
<div class="form-group">
    <b>{!! Form::label('telefono', 'Teléfono:') !!}</b>
    <p>{{ $vendedor->telefono }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <b>{!! Form::label('created_at', 'Creado:') !!}</b>
    <p>{{ $vendedor->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <b>{!! Form::label('updated_at', 'Actualizado:') !!}</b>
    <p>{{ $vendedor->updated_at }}</p>
</div>