@extends('layouts.app')
@section('nombre_modulo', 'Vendedores')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item active"><a href="{{route('vendedors.index')}}">Vendedores</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Vendedores
            @can('vendedors.create')
                <a href="{{route('vendedors.create')}}" class="btn btn-outline-success float-right"> <i class="fas fa-plus"></i> Crear</a>
            @endcan
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('vendedors.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
