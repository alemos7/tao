@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Vendedor</th>
            <th>Correo</th>
            <th>Teléfono</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($vendedors as $vendedor)
            <tr>
                <td>{{ ($vendedor->clientes)? $vendedor->clientes->cliente : '' }}</td>
                <td>{{ $vendedor->vendedor }}</td>
                <td>{{ $vendedor->correo }}</td>
                <td>{{ $vendedor->telefono }}</td>
                <td class="td-actions text-center">
                    @can('vendedors.show')
                        <a href="{{ route('vendedors.show', [$vendedor->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('vendedors.edit')
                        <a href="{{ route('vendedors.edit', [$vendedor->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('vendedors.destroy')
                        {!! Form::open(['route' => ['vendedors.destroy', $vendedor->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection