@extends('layouts.app')
@section('nombre_modulo', 'Vendedores')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('vendedors.index')}}">Vendedores</a></li>
    <li class="breadcrumb-item active">Vendedor</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Vendedor# <b>{{str_pad($vendedor->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="col-sm-12">
                        @include('vendedors.show_fields')
                        <hr>
                        <div class="row float-right">
                            <div class="col-sm-12">
                                <a href="{{ route('vendedors.index') }}" class="btn btn-success">Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
