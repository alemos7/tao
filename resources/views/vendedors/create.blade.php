@extends('layouts.app')
@section('nombre_modulo', 'Vendedores')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('vendedors.index')}}">Vendedores</a></li>
    <li class="breadcrumb-item active"><a href="{{route('vendedors.create')}}">Crear Vendedor</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Vendedores
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'vendedors.store', 'class'=>'w-100']) !!}

                        @include('vendedors.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
