<div class="row">
    <div class="col-sm-6">
        {!! Form::label('cliente_id', 'Cliente:') !!}
        <select class="form-control {{ $errors->has('cliente_id') ? ' is-invalid' : '' }}" name="cliente_id" id="cliente_id">
            <option value="">:: Seleccione ::</option>
            @foreach($clientes AS $cliente)
                <option value="{{$cliente->id}}" @isset($vendedor) @if($cliente->id==$vendedor->cliente_id) selected @endif @endisset @if(@old("cliente_id")==$cliente->id) selected @endif>{{$cliente->cliente}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-6">
        {!! Form::label('vendedor', 'Vendedor:') !!}
        {!! Form::text('vendedor', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-6">
        {!! Form::label('correo', 'Correo:') !!}
        {!! Form::text('correo', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('telefono', 'Teléfono:') !!}
        {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>
<div class="row float-right">
    <!-- Submit Field -->
    <div class="col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('vendedors.index') }}" class="btn btn-success">Cancelar</a>
    </div>

</div>
