<li class="{{ Request::is('tipos*') ? 'active' : '' }}">
    <a href="{{ route('tipos.index') }}"><i class="fa fa-edit"></i><span>Tipos</span></a>
</li>

<li class="{{ Request::is('subTipos*') ? 'active' : '' }}">
    <a href="{{ route('subTipos.index') }}"><i class="fa fa-edit"></i><span>Sub Tipos</span></a>
</li>

<li class="{{ Request::is('preciosCategorias*') ? 'active' : '' }}">
    <a href="{{ route('preciosCategorias.index') }}"><i class="fa fa-edit"></i><span>Precios Categorias</span></a>
</li>

<li class="{{ Request::is('precios*') ? 'active' : '' }}">
    <a href="{{ route('precios.index') }}"><i class="fa fa-edit"></i><span>Precios</span></a>
</li>

<li class="{{ Request::is('vendedors*') ? 'active' : '' }}">
    <a href="{{ route('vendedors.index') }}"><i class="fa fa-edit"></i><span>Vendedors</span></a>
</li>

<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>

<li class="{{ Request::is('solicitudes*') ? 'active' : '' }}">
    <a href="{{ route('solicitudes.index') }}"><i class="fa fa-edit"></i><span>Solicitudes</span></a>
</li>

<li class="{{ Request::is('tipoObras*') ? 'active' : '' }}">
    <a href="{{ route('tipoObras.index') }}"><i class="fa fa-edit"></i><span>Tipo Obras</span></a>
</li>

<li class="{{ Request::is('ordenes*') ? 'active' : '' }}">
    <a href="{{ route('ordenes.index') }}"><i class="fa fa-edit"></i><span>Ordenes</span></a>
</li>

<li class="{{ Request::is('ordenesDetalles*') ? 'active' : '' }}">
    <a href="{{ route('ordenesDetalles.index') }}"><i class="fa fa-edit"></i><span>Ordenes Detalles</span></a>
</li>

<li class="{{ Request::is('subtipos*') ? 'active' : '' }}">
    <a href="{{ route('subtipos.index') }}"><i class="fa fa-edit"></i><span>Subtipos</span></a>
</li>

<li class="{{ Request::is('productos*') ? 'active' : '' }}">
    <a href="{{ route('productos.index') }}"><i class="fa fa-edit"></i><span>Productos</span></a>
</li>

<li class="{{ Request::is('productosCategorias*') ? 'active' : '' }}">
    <a href="{{ route('productosCategorias.index') }}"><i class="fa fa-edit"></i><span>Productos Categorias</span></a>
</li>

<li class="{{ Request::is('productosTiposObras*') ? 'active' : '' }}">
    <a href="{{ route('productosTiposObras.index') }}"><i class="fa fa-edit"></i><span>Productos Tipos Obras</span></a>
</li>

<li class="{{ Request::is('ordenesSubordens*') ? 'active' : '' }}">
    <a href="{{ route('ordenesSubordens.index') }}"><i class="fa fa-edit"></i><span>Ordenes Subordens</span></a>
</li>

<li class="{{ Request::is('autos*') ? 'active' : '' }}">
    <a href="{{ route('autos.index') }}"><i class="fa fa-edit"></i><span>Autos</span></a>
</li>

<li class="{{ Request::is('cajas*') ? 'active' : '' }}">
    <a href="{{ route('cajas.index') }}"><i class="fa fa-edit"></i><span>Cajas</span></a>
</li>

<li class="{{ Request::is('cajasProductos*') ? 'active' : '' }}">
    <a href="{{ route('cajasProductos.index') }}"><i class="fa fa-edit"></i><span>Cajas Productos</span></a>
</li>

<li class="{{ Request::is('recibos*') ? 'active' : '' }}">
    <a href="{{ route('recibos.index') }}"><i class="fa fa-edit"></i><span>Recibos</span></a>
</li>

<li class="{{ Request::is('herramientas*') ? 'active' : '' }}">
    <a href="{{ route('herramientas.index') }}"><i class="fa fa-edit"></i><span>Herramientas</span></a>
</li>

<li class="{{ Request::is('convenios*') ? 'active' : '' }}">
    <a href="{{ route('convenios.index') }}"><i class="fa fa-edit"></i><span>Convenios</span></a>
</li>

<li class="{{ Request::is('adicionalesEficiencias*') ? 'active' : '' }}">
    <a href="{{ route('adicionalesEficiencias.index') }}"><i class="fa fa-edit"></i><span>Adicionales Eficiencias</span></a>
</li>

<li class="{{ Request::is('adicionalesChofers*') ? 'active' : '' }}">
    <a href="{{ route('adicionalesChofers.index') }}"><i class="fa fa-edit"></i><span>Adicionales Chofers</span></a>
</li>

<li class="{{ Request::is('seguimientos*') ? 'active' : '' }}">
    <a href="{{ route('seguimientos.index') }}"><i class="fa fa-edit"></i><span>Seguimientos</span></a>
</li>

<li class="{{ Request::is('ingresosEgresos*') ? 'active' : '' }}">
    <a href="{{ route('ingresosEgresos.index') }}"><i class="fa fa-edit"></i><span>Ingresos Egresos</span></a>
</li>

<li class="{{ Request::is('notificaciones*') ? 'active' : '' }}">
    <a href="{{ route('notificaciones.index') }}"><i class="fa fa-edit"></i><span>Notificaciones</span></a>
</li>

<li class="{{ Request::is('notificacionUsers*') ? 'active' : '' }}">
    <a href="{{ route('notificacionUsers.index') }}"><i class="fa fa-edit"></i><span>Notificacion Users</span></a>
</li>

<li class="{{ Request::is('liquidaciones*') ? 'active' : '' }}">
    <a href="{{ route('liquidaciones.index') }}"><i class="fa fa-edit"></i><span>Liquidaciones</span></a>
</li>

<li class="{{ Request::is('statuses*') ? 'active' : '' }}">
    <a href="{{ route('statuses.index') }}"><i class="fa fa-edit"></i><span>Statuses</span></a>
</li>

<li class="{{ Request::is('prestamos*') ? 'active' : '' }}">
    <a href="{{ route('prestamos.index') }}"><i class="fa fa-edit"></i><span>Prestamos</span></a>
</li>

<li class="{{ Request::is('feriados*') ? 'active' : '' }}">
    <a href="{{ route('feriados.index') }}"><i class="fa fa-edit"></i><span>Feriados</span></a>
</li>

<li class="{{ Request::is('barrios*') ? 'active' : '' }}">
    <a href="{{ route('barrios.index') }}"><i class="fa fa-edit"></i><span>Barrios</span></a>
</li>

