@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Subordenes de Tipo Reclamos</li>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-cash-register"></i>
            Vencimientos
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div id="msgInfo" class="alert alert-success" style="display: none"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table"  id="data-table">
                        <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Nº de Orden</th>
                            <th>Nº de Suborden</th>
{{--                            <th>Perdiodo de Facturación</th>--}}
                            <th>Monto SubOrden</th>
                            <th>Creación</th>
                            <th>Vencimiento</th>
                            <th>Días Vencida</th>
                            {{--<th>Monto de la Suborden</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        <?php
//                        $subOrdenTotal = \App\Models\SubOrden::totalSuborden()
                        ?>
                        @foreach($subordenes AS $item)
                            <tr>
                                <td>{{$item->cliente}}</td>
                                <td>{{str_pad($item->orden_id, 6, '0', STR_PAD_LEFT)}}</td>
                                <td>{{($item->suborden_id)? str_pad($item->suborden_id, 6, '0', STR_PAD_LEFT) : '' }}</td>
                                <td>
                                    <?php
                                    echo \App\Models\SubOrden::totalSuborden($item->suborden_id);
                                    ?>
                                </td>
{{--                                <td>{{$item->periodo_facturarcion}} Días</td>--}}
                                <td>{{ Carbon\Carbon::parse($item->fecha)->format('d/m/Y') }}</td>
                                <td>{{ Carbon\Carbon::parse($item->fecha_vencimiento)->format('d/m/Y') }}</td>
                                <td>{{($item->dias_vencidos>0)? $item->dias_vencidos : 0 }} Días</td>
                                {{--<td>{{number_format(($item->pendiente) ? $item->pendiente : App\Models\SubOrden::totalSuborden($item->suborden_id), 2, ',', '.')}}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
