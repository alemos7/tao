@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Subordenes de Trabajo</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="far fa-newspaper"></i>
            Subordenes de la Orden: <b>#{{str_pad($orden_id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
        <h4>
            <i class="fas fa-user-tag"></i>&nbsp;
            <b>Cliente </b> {{ ($order->clientes)? $order->clientes->cliente : ''}} -
            <b>Consumidor </b>{{ ($order->clientes)? $order->consumidor_nombre : ''}}
        </h4>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div id="msgInfo" class="alert alert-success" style="display: none"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('subOrden.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
