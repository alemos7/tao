@extends('layouts.app')
@section('nombre_modulo', 'Agenda')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active">Agenda</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <div class="row">
                <div class="col-sm-12">
                    <i class="fas fa-calendar-alt"></i>
                    Agenda
                </div>
            </div>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="">Seleccione el Mes de su interés:</label>
                            <select name="selectMes" id="selectMes" class="form-control">
                                @foreach($meses AS $indice=>$mes)
                                    <option value="{{$indice}}" @if(date('n')==$indice) selected @endif>{{$mes}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="" id="tipo_id" value="0">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary position-absolute" style="bottom: 25px" type="button" id="btnFiltrar">Buscar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <div id="divAgenda"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="{{asset('js/searchMyAgenda.js')}}"></script>
@endsection
