@extends('layouts.app')
@section('nombre_modulo', 'Agenda')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active">Agenda</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <div class="row">
                <div class="col-sm-12">
                    <i class="fas fa-calendar-alt"></i>
                    Agenda
                </div>
            </div>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="">Seleccione el Mes de su interés:</label>
                            <select name="selectMes" id="selectMes" class="form-control">
                                @foreach($meses AS $indice=>$mes)
                                    <option value="{{$indice}}" @if(date('n')==$indice) selected @endif>{{$mes}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="">Seleccione el Tipo de Suborden:</label>
                            <select name="tipo_id" id="tipo_id" class="form-control">
                                <option value="0">Todos</option>
                                <option value="1">INSTALACION</option>
                                <option value="2">MEDICION</option>
                                <option value="3">SOPORTE TÉCNICO</option>
                                <option value="4">RECLAMO</option>
                                <option value="5">VERIFICACION</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary position-absolute" style="bottom: 25px" type="button" id="btnFiltrar">Buscar</button>
                    </div>
                    <div class="col-sm-4">
                        <p>
                            <i class="fa fa-circle text-info"></i> Instalación &nbsp;
                            <i class="fa fa-circle text-success"></i> Medición &nbsp;
                            <i class="fa fa-circle text-danger"></i> Servicio Técnico &nbsp;
                        </p>
                        <p>
                            <i class="fa fa-circle text-muted"></i> Reclamo &nbsp;
                            <i class="fa fa-circle text-warning"></i> Verificación &nbsp;
                            <i class="fa fa-circle text-primary"></i> Más de 1 Suborden
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <div id="divAgenda"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="{{asset('js/searchAgenda.js')}}"></script>
@endsection
