@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}"> Mis Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">ordenes de trabajo</li>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-exclamation-triangle"></i>
             Mis Ordenes de Trabajo:
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div id="msgInfo" class="alert alert-success" style="display: none"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table" id="data-table">
                        <thead>
                        <tr>
                            <th>Orden n°</th>
                            <th>Suborden n°</th>
                            <th>Cliente</th>
                            <th>Trabajo</th>
                            <th>Fecha Inicio</th>
                            <th>Hora Inicio</th>
                            <th>Consumidor</th>
                            <th>Telefono</th>
                            <th>Direccion</th>
                            <th>Ver</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subordenes as $suborden)
                            <tr>
                                <td>{{str_pad($suborden->orderid, 6, '0', STR_PAD_LEFT)}}</td>
                                <td>{{str_pad($suborden->suborderid, 6, '0', STR_PAD_LEFT)}}</td>
                                <td>{{$suborden->cliente}}</td>
                                <td>{{$suborden->tipo }}</td>
                                <td>{{$suborden->fecha_inicio }}</td>
                                <td>{{$suborden->hora_inicio }}</td>
                                <td>{{$suborden->consumidor_nombre }}</td>
                                <td>{{$suborden->consumidor_tlf }}</td>
                                <td><a href="https://www.google.com.ar/maps/search/{{$suborden->direccion }}+{{$suborden->localidad }}" target="_blank">{{$suborden->direccion }} {{$suborden->localidad }}</a></td>
                                <th><a href="{{route('subOrden.show', $suborden->suborderid)}}"><i class="fa fa-eye"></i> </a> </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="text-center">

        </div>



    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
