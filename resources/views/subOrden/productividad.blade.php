@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Subordenes de Tipo Reclamos</li>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-exclamation-triangle"></i>
            Productividad:
        </h1>
    </section>
    <div class="content m-t-40">
        <div class="clearfix"></div>

        @include('flash::message')
        <div id="msgInfo" class="alert alert-success" style="display: none"></div>

        <div class="clearfix"></div>
        <div class="row">
          <div class="col-lg-6">
            <h4>Detalle por N° de orden</h4>
            <table class="table" id="">
                <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Orden n°</th>
                    <th>Cantidad</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productividaddetail as $productividaddetailItem)
                    <tr>
                        <td>{{$productividaddetailItem->fecha_inicio}}</td>
                        <td>{{$productividaddetailItem->id}}</td>
                        <td>{{$productividaddetailItem->cantidad}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
          </div>
          <div class="col-lg-6">
            <h4>Detalle por Mes</h4>
                        <table class="table" id="">
                            <thead>
                            <tr>
                                <th>Año</th>
                                <th>Mes</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($productividad as $productividadItem)
                                <tr>
                                    <td>{{$productividadItem->year}}</td>
                                    <td>
                                      <?php
                                      $monthName = date("F", mktime(0, 0, 0, $productividadItem->month, 10));
                                      echo $monthName; // Output: May ?>
                                    </td>
                                    <td>{{$productividadItem->cantidad}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
          </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
