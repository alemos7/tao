@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Subordenes de Tipo Reclamos</li>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-exclamation-triangle"></i>
            Subordenes de Tipo Reclamos:
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div id="msgInfo" class="alert alert-success" style="display: none"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table" id="data-table">
                        <thead>
                            <tr>
                                <th>Fecha Inicio</th>
                                <th>Hora Inicio</th>
                                <th>Estado</th>
                                <th>Solicitud</th>
                                <th>Categoría Productos</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($subordenes as $suborden)
                            <tr>
                                <td>{{ $suborden->getFechaInicioESPAttribute() }}</td>
                                <td>{{ $suborden->hora_inicio }}</td>
                                <td>{{ $suborden->solicitudes->solicitud }}</td>
                                <td>{{ $suborden->estados->estado }}</td>
                                <td>{{ $suborden->categoriasProductos->categoria }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
