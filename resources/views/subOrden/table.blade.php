@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table-suborden">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Fecha Inicio</th>
                <th>Hora Inicio</th>
                <th>Categoría Productos</th>
                <th>Estatus</th>
                <th width="10%">Acción</th>
            </tr>
        </thead>
        <tbody>
        @foreach($subordenes as $suborden)
            <tr>
                <td>{{ $suborden->tipos->tipo }}</td>
                <td>{{ $suborden->getFechaInicioESPAttribute() }}</td>
                <td>{{ $suborden->hora_inicio }}</td>
                <td>
                    @if ($suborden->productos_categorias_id && $suborden->productos_categorias_id != 0)
                        {{ $suborden->categoriasProductos->categoria }}
                        @else
                        <i>No aplica</i>
                    @endif
                </td>
                <td>{{ $suborden->estados->estado }}</td>
                <td class="td-actions text-center">
                    @can('subOrden.edit')
                        <a href="{{route('subOrden.edit', [$suborden->id])}}" class="btn btn-outline-success btn-round btn-sm btnSave" title="Guardar Cantidad">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    @endcan
                    @can('subOrden.destroy')
                        {!! Form::open(['route' => ['subOrden.destroy', $suborden->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4" class="text-right">Total de la Orden:</th>
                <th id="tdTotalInstalacion" class="text-right"><h3>{{number_format($total_orden,2,',','.')}}</h3></th>
            </tr>
        </tfoot>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
