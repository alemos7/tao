@extends('layouts.app')
@section('nombre_modulo', 'Sub Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Sub Orden de Trabajo</li>
@endsection

@section('css')
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <div class="row">
                <div class="col-sm-12">
                    <i class="far fa-newspaper"></i>
                    Sub Orden de Trabajo
                </div>
            </div>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Monto Restante a Financiar: <b>{{number_format(($montoFinanciado = $orden->clientes->monto_financiar - $orden->clientes->getTotalCuentaCorriente($orden->cliente_id)),2,',','.')}}</b></h4>
                    </div>
                    <div class="col-sm-2">
                        <h4>Nº de Orden: <b>{{$codOrden = str_pad($orden->id, 6, '0', STR_PAD_LEFT)}}</b></h4>
                    </div>
                    <div class="col-sm-4 text-right">
                        <h4>Cliente: <b>{{$orden->clientes->cliente}}</b></h4>
                    </div>
                </div>

                {!! Form::model($subOrden, ['route' => ['subOrden.update', $subOrden->id], 'method'=>'PUT', 'style'=>'width: 100%', "enctype"=>"multipart/form-data"]) !!}

                <div class="row">
                    <div class="form-group col-sm-3">
                        {!! Form::label('solicitud_id', 'Solicitud:') !!}
                        <select class="form-control {{ $errors->has('solicitud_id') ? ' is-invalid' : '' }}" name="solicitud_id" id="solicitud_id">
                            <option value="">:: Seleccione ::</option>
                            @foreach($solicitudes AS $solicitud)
                                <option value="{{$solicitud->id}}" @isset($subOrden) @if($solicitud->id==$subOrden->solicitud_id) selected
                                        @endif @endisset @if(@old("solicitud_id")==$solicitud->id) selected @endif>{{$solicitud->solicitud}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        {!! Form::label('estado_id', 'Estado:') !!}
                        <select class="form-control {{ $errors->has('estado_id') ? ' is-invalid' : '' }}" name="estado_id" id="estado_id">
                            <option value="">:: Seleccione ::</option>
                            @foreach($estados AS $estado)
                                <option value="{{$estado->id}}"
                                        @isset($subOrden)
                                        @if($estado->id==$subOrden->estado_id)
                                        selected
                                        @endif
                                        @endisset
                                        @if(@old("estado_id")==$estado->id)
                                        selected
                                        @endif
                                        @if($montoFinanciado<=0 and $estado->id==1)
                                        selected
                                        @endif>
                                    {{$estado->estado}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        {!! Form::label('chofer_id', 'Chofer:') !!}
                        <select class="form-control {{ $errors->has('chofer_id') ? ' is-invalid' : '' }}" name="chofer_id" id="chofer_id">
                            <option value="">:: Seleccione ::</option>
                            @foreach($choferes AS $chofer)
                                <option value="{{$chofer->id}}" @isset($subOrden) @if($chofer->id==$subOrden->chofer_id) selected
                                        @endif @endisset @if(@old("chofer_id")==$chofer->id) selected @endif>{{$chofer->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div style="display: block" class="form-group col-sm-3">
                        {!! Form::label('auto_id', 'Auto:') !!}
                        <select class="form-control" name="auto_id" id="auto_id">
                            <option value="">:: Seleccione ::</option>
                            @foreach($autos AS $auto)
                                <option value="{{$auto->id}}" @isset($ordenes) @if($auto->id==$ordenes->auto_id) selected @endif @endisset @if(@old("auto_id")==$auto->id) selected @endif>{{$auto->autos}}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback" role="alert" id="auto_msg">
                            <strong>Recuerde que puede seleccionar un Auto</strong>
                        </span>
                    </div>


                </div>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            {!! Form::label('tecnico_id1', $nombre_tecnico.' #1:') !!}
                            <select class="form-control {{ $errors->has('tecnico_id1') ? ' is-invalid' : '' }}" name="tecnico_id1" id="tecnico_id1">
                                <option value="">:: Seleccione ::</option>
                                @foreach($tecnicos AS $tecnico)
                                    <option value="{{$tecnico->id}}" @isset($subOrden) @if($tecnico->id==$subOrden->tecnico_id1) selected
                                            @endif @endisset @if(@old("tecnico_id1")==$tecnico->id) selected @endif>{{$tecnico->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('tecnico_id2', $nombre_tecnico.' #2:') !!}
                            <select class="form-control {{ $errors->has('tecnico_id2') ? ' is-invalid' : '' }}" name="tecnico_id2" id="tecnico_id2">
                                <option value="">:: Seleccione ::</option>
                                @foreach($tecnicos AS $tecnico)
                                    <option value="{{$tecnico->id}}" @isset($subOrden) @if($tecnico->id==$subOrden->tecnico_id2) selected
                                            @endif @endisset @if(@old("tecnico_id2")==$tecnico->id) selected @endif>{{$tecnico->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('tecnico_id3', $nombre_tecnico.' #3:') !!}
                            <select class="form-control {{ $errors->has('tecnico_id3') ? ' is-invalid' : '' }}" name="tecnico_id3" id="tecnico_id3">
                                <option value="">:: Seleccione ::</option>
                                @foreach($tecnicos AS $tecnico)
                                    <option value="{{$tecnico->id}}" @isset($subOrden) @if($tecnico->id==$subOrden->tecnico_id3) selected
                                            @endif @endisset @if(@old("tecnico_id3")==$tecnico->id) selected @endif>{{$tecnico->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('tecnico_id4', $nombre_tecnico.' #4:') !!}
                            <select class="form-control {{ $errors->has('tecnico_id4') ? ' is-invalid' : '' }}" name="tecnico_id4" id="tecnico_id4">
                                <option value="">:: Seleccione ::</option>
                                @foreach($tecnicos AS $tecnico)
                                    <option value="{{$tecnico->id}}" @isset($subOrden) @if($tecnico->id==$subOrden->tecnico_id4) selected
                                            @endif @endisset @if(@old("tecnico_id4")==$tecnico->id) selected @endif>{{$tecnico->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            {!! Form::label('caja_id', 'Caja de Productos:') !!}
                            <select class="form-control {{ $errors->has('caja_id') ? ' is-invalid' : '' }}" name="caja_id" id="caja_id">
                                <option value="">:: Seleccione ::</option>
                                @foreach($cajas AS $caja)
                                    <option value="{{$caja->id}}" @isset($subOrden) @if($caja->id==$subOrden->caja_id) selected
                                            @endif @endisset @if(@old("caja_id")==$caja->id) selected @endif>{{$caja->caja}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('fecha_inicio', 'Fecha de Inicio:') !!}
                            <input type="text" name="fecha_inicio" id="fecha_inicio" class="form-control" value="{{ @old("fecha_inicio", ($subOrden)? $subOrden->getFechaInicioESPAttribute() : '') }}">
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('hora_inicio', 'Hora de Inicio:') !!}
                            <input type="text" name="hora_inicio" id="hora_inicio" class="form-control" value="{{ @old("hora_inicio", ($subOrden)? $subOrden->hora_inicio : '') }}">
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('duracion', 'Duración:') !!}
                            <input type="text" name="duracion" id="duracion" class="form-control" value="{{ @old("duracion", ($subOrden)? $subOrden->duracion : '') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            {!! Form::label('productos_categorias_id', 'Categoria de los Productos:') !!}
                            <select class="form-control {{ $errors->has('productos_categorias_id') ? ' is-invalid' : '' }}" name="productos_categorias_id" id="productos_categorias_id">
                                <option value="">:: Seleccione ::</option>
                                @foreach($productos_categorias AS $categoria)
                                    <option value="{{$categoria->id}}" @isset($subOrden) @if($categoria->id==$subOrden->productos_categorias_id) selected
                                            @endif @endisset @if(@old("productos_categorias_id")==$categoria->id) selected @endif>{{$categoria->categoria}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="row">
                    <div class="form-group col-sm-12">
                        {!! Form::label('observaciones', 'Observaciones:') !!}
                        <textarea name="observaciones" id="observaciones" class="form-control">{{ @old("observaciones", ($subOrden)? $subOrden->observaciones : '') }}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-12">
                        {!! Form::label('informe', 'Informe de finiquito:') !!}
                        <textarea name="informe" id="informe" class="form-control">{{ @old("informe", ($subOrden)? $subOrden->informe : '') }}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <!-- Adjunto 2 Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('adjunto_1', 'Adjunto 1:') !!}
                            {!! Form::file('adjunto_1', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- Adjunto 2 Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('adjunto_2', 'Adjunto 2:') !!}
                            {!! Form::file('adjunto_2', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- Adjunto 3 Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('adjunto_3', 'Adjunto 3:') !!}
                            {!! Form::file('adjunto_3', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- Adjunto 4 Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('adjunto_4', 'Adjunto 4:') !!}
                            {!! Form::file('adjunto_4', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>


                    <?php
                    $seguimiento = \App\Models\Seguimiento::where('suborden_id', $subOrden->id)->where('tipo', 2)->first();
                    if ($seguimiento) {
                        ?>
                        <div id="detailReport">
                            <h3>Informe</h3><hr class="hrgrueso mt-0">
                            <?php
                            echo $seguimiento->created_at."<br>".$seguimiento->observacion;
                            ?>
                        </div>
                        <?php
                    }
                    ?>



                    <h4 class="mt-4">Productos:</h4>
                    <hr class="hrgrueso mt-0">
                    <div class="row" id="tablaProductos">

                            <div id="divTablaProductos" class="w-100"></div>

                    </div>

                    <div class="form-group col-sm-12">
                        <input type="hidden" name="totalItems" id="totalItems" value="0">
                        <input type="hidden" name="tipo_id" id="tipo_id" value="{{$tipo_id}}">
                        <input type="hidden" name="orden_id" id="orden_id" value="{{$orden->id}}">
                        <input type="hidden" name="subOrden_id" id="subOrden_id" value="{{$subOrden->id}}">
                        <input type="hidden" name="coeficienteConstante" id="coeficienteConstante" value="{{$orden->tiposObras->coeficienteConstante}}">

                        {!! Form::submit('Guardar', ['class' => 'btn btn-info']) !!}
                        <a href="{{ route('ordenes.index') }}" class="btn btn-info">Cancelar</a>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/moment/moment.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>


    <script src="{{asset('js/cargarTablasProductos.js')}}"></script>
    <script src="{{asset('js/add_producto_suborden.js')}}"></script>

    <script>

        $('#fecha_inicio').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            minDate: new Date(),
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            'default': 'now',
            time: false
        });

        $('#hora_inicio').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            time: true,
            date: false,
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            'default': 'now',
        });
    </script>

@endsection
