@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Adicionales Chofer
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        @include('flash::message')
                    </div>

                    <form action="guardarChofer" method="post">
                        @csrf

                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" id="adicional_id" name="adicional_id" value="1">
                                <input type="text" id="adicional_costo" name="adicional_costo" class="form-control" value="{{$adicional->adicional_costo}}">
                            </div>
                        </div>

                        <hr>
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{{ route('adicionalesEficiencias.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection