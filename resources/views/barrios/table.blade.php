@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
            <tr>
                <th>Barrio</th>
                <th>Kilometro</th>
                <th>Adjuntos</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($barrios as $barrios)
            <tr>
                <td>{{ $barrios->barrio }}</td>
                <td>{{ $barrios->kilometro }}</td>
{{--                <td>--}}
{{--                    {!! Form::open(['route' => ['barrios.destroy', $barrios->id], 'method' => 'delete']) !!}--}}
{{--                    <div class='btn-group'>--}}
{{--                        <a href="{{ route('barrios.show', [$barrios->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>--}}
{{--                        <a href="{{ route('barrios.edit', [$barrios->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
{{--                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
{{--                    </div>--}}
{{--                    {!! Form::close() !!}--}}
{{--                </td>--}}

                <td class="td-actions text-center">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Seleccione un adjunto
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @if($barrios->adjunto_1) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_1}}">{{$barrios->adjunto_1}}</a> @endif
                            @if($barrios->adjunto_2) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_2}}">{{$barrios->adjunto_2}}</a> @endif
                            @if($barrios->adjunto_3) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_3}}">{{$barrios->adjunto_3}}</a> @endif
                            @if($barrios->adjunto_4) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_4}}">{{$barrios->adjunto_4}}</a> @endif
                            @if($barrios->adjunto_5) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_5}}">{{$barrios->adjunto_5}}</a> @endif
                            @if($barrios->adjunto_6) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_6}}">{{$barrios->adjunto_6}}</a> @endif
                            @if($barrios->adjunto_7) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_7}}">{{$barrios->adjunto_7}}</a> @endif
                            @if($barrios->adjunto_8) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_8}}">{{$barrios->adjunto_8}}</a> @endif
                            @if($barrios->adjunto_9) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_9}}">{{$barrios->adjunto_9}}</a> @endif
                            @if($barrios->adjunto_10) <a target="_blank" class="dropdown-item" href="{{ URL::to('/').'/storage/'.$barrios->adjunto_10}}">{{$barrios->adjunto_10}}</a> @endif
                        </div>
                    </div>
                </td>

                <td class="td-actions text-center">
                        <a href="{{ route('barrios.show', [$barrios->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                        <a href="{{ route('barrios.edit', [$barrios->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                        {!! Form::open(['route' => ['barrios.destroy', $barrios->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
