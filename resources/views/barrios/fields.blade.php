<div class="row">
    <div class="col-sm-4">
        <!-- Barrio Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('barrio', 'Barrio:') !!}
            {!! Form::text('barrio', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Kilometro Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('kilometro', 'Kilometro:') !!}
            {!! Form::text('kilometro', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 1 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_1', 'Adjunto 1:') !!}
            {!! Form::file('adjunto_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <!-- Adjunto 2 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_2', 'Adjunto 2:') !!}
            {!! Form::file('adjunto_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 3 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_3', 'Adjunto 3:') !!}
            {!! Form::file('adjunto_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 4 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_4', 'Adjunto 4:') !!}
            {!! Form::file('adjunto_4', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <!-- Adjunto 5 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_5', 'Adjunto 5:') !!}
            {!! Form::file('adjunto_5', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 6 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_6', 'Adjunto 6:') !!}
            {!! Form::file('adjunto_6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 7 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_7', 'Adjunto 7:') !!}
            {!! Form::file('adjunto_7', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <!-- Adjunto 8 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_8', 'Adjunto 8:') !!}
            {!! Form::file('adjunto_8', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 9 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_9', 'Adjunto 9:') !!}
            {!! Form::file('adjunto_9', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Adjunto 10 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_10', 'Adjunto 10:') !!}
            {!! Form::file('adjunto_10', null, ['class' => 'form-control']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <!-- Adjunto 11 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_11', 'Adjunto 11:') !!}
            {!! Form::file('adjunto_11', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('barrios.index') }}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
</div>
