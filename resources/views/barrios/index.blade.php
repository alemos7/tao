@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-map-marked-alt"></i>
            Barrios
            <a href="{{route('barrios.create')}}" class="btn btn-outline-success float-right">
                <i class="fas fa-plus"></i>
                Crear Barrio
            </a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('barrios.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection
