<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $barrios->id }}</p>
</div>

<!-- Barrio Field -->
<div class="form-group">
    {!! Form::label('barrio', 'Barrio:') !!}
    <p>{{ $barrios->barrio }}</p>
</div>

<!-- Kilometro Field -->
<div class="form-group">
    {!! Form::label('kilometro', 'Kilometro:') !!}
    <p>{{ $barrios->kilometro }}</p>
</div>

<!-- Adjunto 1 Field -->
<div class="form-group">
    {!! Form::label('adjunto_1', 'Adjunto 1:') !!}
    <p>{{ $barrios->adjunto_1 }}</p>
</div>

<!-- Adjunto 2 Field -->
<div class="form-group">
    {!! Form::label('adjunto_2', 'Adjunto 2:') !!}
    <p>{{ $barrios->adjunto_2 }}</p>
</div>

<!-- Adjunto 3 Field -->
<div class="form-group">
    {!! Form::label('adjunto_3', 'Adjunto 3:') !!}
    <p>{{ $barrios->adjunto_3 }}</p>
</div>

<!-- Adjunto 4 Field -->
<div class="form-group">
    {!! Form::label('adjunto_4', 'Adjunto 4:') !!}
    <p>{{ $barrios->adjunto_4 }}</p>
</div>

<!-- Adjunto 5 Field -->
<div class="form-group">
    {!! Form::label('adjunto_5', 'Adjunto 5:') !!}
    <p>{{ $barrios->adjunto_5 }}</p>
</div>

<!-- Adjunto 6 Field -->
<div class="form-group">
    {!! Form::label('adjunto_6', 'Adjunto 6:') !!}
    <p>{{ $barrios->adjunto_6 }}</p>
</div>

<!-- Adjunto 7 Field -->
<div class="form-group">
    {!! Form::label('adjunto_7', 'Adjunto 7:') !!}
    <p>{{ $barrios->adjunto_7 }}</p>
</div>

<!-- Adjunto 8 Field -->
<div class="form-group">
    {!! Form::label('adjunto_8', 'Adjunto 8:') !!}
    <p>{{ $barrios->adjunto_8 }}</p>
</div>

<!-- Adjunto 9 Field -->
<div class="form-group">
    {!! Form::label('adjunto_9', 'Adjunto 9:') !!}
    <p>{{ $barrios->adjunto_9 }}</p>
</div>

<!-- Adjunto 10 Field -->
<div class="form-group">
    {!! Form::label('adjunto_10', 'Adjunto 10:') !!}
    <p>{{ $barrios->adjunto_10 }}</p>
</div>

<!-- Adjunto 11 Field -->
<div class="form-group">
    {!! Form::label('adjunto_11', 'Adjunto 11:') !!}
    <p>{{ $barrios->adjunto_11 }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $barrios->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $barrios->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $barrios->deleted_at }}</p>
</div>

