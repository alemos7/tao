<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $notificacionUser->id }}</p>
</div>

<!-- Notificaciones Id Field -->
<div class="form-group">
    {!! Form::label('notificaciones_id', 'Notificaciones Id:') !!}
    <p>{{ $notificacionUser->notificaciones_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $notificacionUser->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $notificacionUser->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $notificacionUser->deleted_at }}</p>
</div>

<!-- Clientes Id Field -->
<div class="form-group">
    {!! Form::label('clientes_id', 'Clientes Id:') !!}
    <p>{{ $notificacionUser->clientes_id }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $notificacionUser->estatus }}</p>
</div>

