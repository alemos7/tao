<!-- Notificaciones Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('notificaciones_id', 'Notificaciones Id:') !!}
    {!! Form::number('notificaciones_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Clientes Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clientes_id', 'Clientes Id:') !!}
    {!! Form::number('clientes_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::number('estatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('notificacionUsers.index') }}" class="btn btn-default">Cancel</a>
</div>
