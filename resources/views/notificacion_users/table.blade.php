@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">

    <br><h3>Por leer</h3><hr>
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr style="align-content: center">
                <th>Notificaciones Id</th>
                <th>Clientes Id</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notificacionUsersLeer as $notificacionUser)
            <tr>
                <td>{{ $notificacionUser->notificacion->comentario }}</td>
                <td>{{ $notificacionUser->cliente->nombre }}</td>
{{--                <td>{{ $notificacionUser->status->name }}</td>--}}
                    <td>
                    {!! Form::open(['route' => ['notificacionUsers.destroy', $notificacionUser->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
{{--                        <a href="{{ route('notificacionUsers.show', [$notificacionUser->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>--}}
{{--                        <a href="{{ route('notificacionUsers.edit', [$notificacionUser->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
                        {!! Form::button('<i class="fas fa-eye-slash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro de pasar a visto?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <br><h3>Leidas</h3><hr>

    <table class="table table-bordered" id="data-table-2">
        <thead>
        <tr style="align-content: center">
            <th>Notificaciones Id</th>
            <th>Clientes Id</th>
        </tr>
        </thead>
        <tbody>
        @foreach($notificacionUsersLeidas as $notificacionUser)
            <tr>
                <td>{{ $notificacionUser->notificacion->comentario }}</td>
                <td>{{ $notificacionUser->cliente->nombre }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
