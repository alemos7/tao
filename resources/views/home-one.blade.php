@extends('templates.material.main')
@section('jquery') {{-- Including this section to override it empty. Using jQuery from webpack build --}} @endsection
@section('css')


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/toast-master/css/jquery.toast.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css"/>
    <link rel="stylesheet" href="{{asset('css/introjs.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <style>
        .ticket {
            width: 40%;

        }

        @media only screen and (max-width: 600px) {
            .ticket {
                width: 30%;
                padding: 15px;
            }
        }

        .dtp > .dtp-content > .dtp-date-view > header.dtp-header {
            background: #1e88e5;
            color: #fff;
            text-align: center;
            padding: 0.3em;
        }

        .dtp div.dtp-date, .dtp div.dtp-time {
            background: #1e88e5;
            text-align: center;
            color: #fff;
            padding: 10px;
        }

        .dtp .p10 > a {
            color: #fff;
            text-decoration: none;
        }

        .dtp table.dtp-picker-days tr > td > a.selected {
            background: #1e88e5;
            color: #fff;
        }

        #dtp_UpXtk {
            overflow-y: inherit;
            overflow-x: hidden;
        }

        .dropzone {
            border: none;
            background: none !important;
        }

        .dropzone-style {
            border: 1px dashed #ccc;
            border-radius: 10px;
        }

        .dz-message {
            height: 70px;
        }

        table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > th:first-child:before {
            top: 15px;
            background-color: #26c6da;
        }
    </style>
@endsection
@push('before-scripts')
    <script src="{{ mix('/js/home-one.js') }}"></script>
@endpush
@section('content')

    @if(session('claveCambiada'))
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="alert alert-success text-center">{{session('claveCambiada')}}</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif
    @if ($password_admin)
        <div class="card">
            <div class="card-body">
                @include('users.change_password')
            </div>
        </div>
    @endif
    @if ($completeData)
        <div class="card">
            <div class="card-body">
                @include('users.complete_data')
            </div>
        </div>
    @endif

    @if(in_array(4, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(5, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(6, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(7, Auth::user()->perfiles->pluck('role_id')->toArray()))

        @include('flash::message')

        <div class="row">
            @foreach($subordenesDelDia AS $i=>$item)

                <div class="col-md-4">
                    <div class="card card-outline-{{$item->tipos->color}}">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">{{$item->tipos->tipo}}</h4></div>
                        <div class="card-body text-center">
                            <h2>
                                Hora de Inicio: <b>{{Carbon\Carbon::parse($item->hora_inicio)->format('g:i A')}}</b>
                            </h2>
                            <div class="row text-left">
                                <div class="col-sm-12">
                                    <b>Consumidor:</b> {{strtoupper($item->ordenes->consumidor_nombre)}}
                                </div>
                                <div class="col-sm-12">
                                    <b>Categoría:</b> {{$item->categoriasProductos->categoria}}
                                </div>
                                <div class="col-sm-12">
                                    <b>Provincia:</b> {{$item->ordenes->provincias->name}}
                                </div>
                                <div class="col-sm-12">
                                    <b>Localidad:</b> {{$item->ordenes->localidades->name}}
                                </div>
                                <div class="col-sm-12">
                                    <b>Dirección:</b> {{$item->ordenes->consumidor_address}}
                                </div>
                                <div class="col-sm-12">
                                    <b>Teléfono:</b> {{$item->ordenes->consumidor_tlf}}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">

                            @if(!$seguimiento=\App\Models\SubOrden::searchUltSeguimiento($item->id))
                                <form action="{{route('seguimiento.store')}}" method="post" id="formSeguimiento<?=$i?>">
                                    @csrf
                                    <input type="hidden" id="latitud<?=$i?>" name="latitud" value="">
                                    <input type="hidden" id="longitud<?=$i?>" name="longitud" value="">
                                    <input type="hidden" name="suborden_id" value="{{$item->id}}">
                                    <input type="hidden" name="tipo" value="1">
                                    <button class="btn btn-success btn-block btnLlegue" type="button" data-id="<?=$i?>">
                                        <i class="fas fa-door-open fa-2x"></i>
                                        <h3 class="text-white">LLEGUE</h3>
                                    </button>
                                </form>
                            @else
                                @if(\App\Models\SubOrden::searchUltSeguimiento($item->id)=='1')
                                        <button type="button" class="btn btn-danger btn-block btnMeFui"
                                                data-target="#ModalMeFui" data-toggle="modal"
                                                data-subordenid="{{$item->id}}">
                                                <i class="fas fa-door-closed fa-2x"></i>
                                                <h3 class="text-white">ME FUI</h3>
                                        </button>
                                @else
                                    <div class="alert alert-warning">ATENDIDO</div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        @include('modalSeguimiento')

    @else
        <div class="jumbotron bg-white">
            <h3 class="">Hola {{Auth::user()->nombre}}!</h3>
            <hr class="my-4">
            <p>Navega a traves de las opciones presentadas en el panel lateral izquierdo.</p>
        </div>
    @endif



@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    @if(in_array(4, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(5, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(6, Auth::user()->perfiles->pluck('role_id')->toArray()) || in_array(7, Auth::user()->perfiles->pluck('role_id')->toArray()))
        <script>

            let latitud;
            let longitud;

        function getGeolocation() {
            navigator.geolocation.getCurrentPosition(drawMap);
        }
        function drawMap(geoPos) {
            geolocate = new google.maps.LatLng(geoPos.coords.latitude, geoPos.coords.longitude);
            latitud = geoPos.coords.latitude;
            longitud = geoPos.coords.longitude;

            console.log( 'latitud', latitud );
        }

        getGeolocation();
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASRlQkkJPSVOJOQhgNZru739GwiVKaK9o"></script>

        <script>
            $('.btnLlegue').click(function () {
                let id=$(this).data('id');
                $('#latitud'+id).val(latitud);
                $('#longitud'+id).val(longitud);
                $('#formSeguimiento'+id).submit();
            });
        </script>
        <script>
            $('.btnMeFui').click(function () {

                $('#latitud').val(latitud);
                $('#longitud').val(longitud);
                $('#suborden_id').val($(this).data('subordenid'));
                $('#observacion').val('');
                $('#fotos').val('');
            });
        </script>
@endif


@endsection
