@extends('templates.material.main')
@section('jquery') {{-- Including this section to override it empty. Using jQuery from webpack build --}} @endsection
@push('before-scripts')
    <script src="{{ mix('/js/home-one.js') }}"></script>
@endpush
@section('nombre_modulo', __('Permisos'))
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">{{ __('Inicio') }}</a></li>
    <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">{{ __('Permisos') }}</a></li>
    <li class="breadcrumb-item active">{{ __('Detalle') }}</li>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            {{ __('Permiso') }}# <b>{{str_pad($permission->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="card-body">
                    <p><strong>{{ __('Nombre del Permiso') }}:</strong> {{$permission->name}}</p>
                    <p><strong>{{ __('Permiso') }}:</strong> {{$permission->slug}}</p>
                    <p><strong>{{ __('Descripción') }}:</strong> {{$permission->description}}</p>
                    <hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-success float-right">{{__('Regresar')}}</a>
                </div>
            </div>
        </div>

@endsection