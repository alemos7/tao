<!--/*************************************************************
**      MODAL Seguimiento Me Fui                                     **
**************************************************************/-->
<div id="ModalMeFui" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <form action="{{route('seguimiento.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Resumen de Obra</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                                <table class="table table-striped table-bordered" id="tableFotos">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="file" name="foto[]" accept="image/png,image/jpeg,image/gif" class="form-control pb-5">
                                        </td>
                                        <td style="vertical-align: middle">
                                            <button class="btn btn-outline-danger btn-sm btnDelFoto" type="button">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="observacion">Observación:</label>
                                <textarea name="observacion" id="observacion" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="latitud" value="" id="latitud">
                    <input type="hidden" name="longitud" value="" id="longitud">
                    <input type="hidden" id="contadorFoto" name="contadorFoto" value="1">
                    <input type="hidden" name="suborden_id" id="suborden_id" value="">
                    <input type="hidden" name="tipo" value="2">

                    <button class="btn btn-primary" type="button" id="btnAddFoto">Agregar Otra Foto</button>
                    <button type="submit" class="btn btn-danger waves-effect text-left">Me Fui</button>
                    <button type="button" class="btn btn-success waves-effect text-left" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--************************************************************-->

@section('scripts')
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/addFotos.js')}}"></script>

@endsection