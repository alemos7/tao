@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection


<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Codigo</th>
                <th>Producto</th>
                <th>Coeficiente</th>
                @foreach($tiposObras AS $tipoObra)
                    <th>{{ $tipoObra->tipo_obra }}</th>
                @endforeach
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        @foreach($productos as $producto)
            <tr>
                <td>{{ $producto->categorias->categoria }}</td>
                <td>{{ $producto->codigo }}</td>
                <td>{{ $producto->producto }}</td>
                <td>{{ $producto->coeficiente }}</td>

                @foreach($tiposObras AS $tipoObra)
                    <td align="right">
                        {{$tipoObra->coeficienteConstante * $producto->coeficiente}}
                    </td>
                @endforeach

                <td class="td-actions text-center">
                    @can('productos.show')
                        <a href="{{ route('productos.show', [$producto->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('productos.edit')
                        <a href="{{ route('productos.edit', [$producto->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('productos.destroy')
                        {!! Form::open(['route' => ['productos.destroy', $producto->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection
