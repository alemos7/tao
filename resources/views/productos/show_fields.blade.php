<!-- Categoria Id Field -->
<div class="form-group">
    <b>{!! Form::label('categoria_id', 'Categoria:') !!}</b>
    <p>{{ $productos->categorias->categoria }}</p>
</div>

<!-- Codigo Field -->
<div class="form-group">
    <b>{!! Form::label('codigo', 'Codigo:') !!}</b>
    <p>{{ $productos->codigo }}</p>
</div>

<!-- Producto Field -->
<div class="form-group">
    <b>{!! Form::label('producto', 'Producto:') !!}</b>
    <p>{{ $productos->producto }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <b>{!! Form::label('created_at', 'Created At:') !!}</b>
    <p>{{ $productos->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <b>{!! Form::label('updated_at', 'Updated At:') !!}</b>
    <p>{{ $productos->updated_at }}</p>
</div>

