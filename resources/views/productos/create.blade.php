@extends('layouts.app')
@section('nombre_modulo', 'Productos')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('productos.index')}}">Productos</a></li>
    <li class="breadcrumb-item active"><a href="{{route('productos.create')}}">Crear Producto</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-tags"></i>
            Crear Producto
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'productos.store', 'class'=>'w-100']) !!}

                        @include('productos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
