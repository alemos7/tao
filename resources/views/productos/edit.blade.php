@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-tags"></i>
            Producto# <b>{{str_pad($productos->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productos, ['route' => ['productos.update', $productos->id], 'class'=>'w-100', 'method' => 'patch']) !!}

                        @include('productos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection