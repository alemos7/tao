@extends('layouts.app')
@section('nombre_modulo', 'Productos')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('productos.index')}}">Productos</a></li>
    <li class="breadcrumb-item active">Producto</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-tags"></i>
            Producto# <b>{{str_pad($productos->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="col-sm-12">
                        @include('productos.show_fields')
                        <hr>
                        <div class="row float-right">
                            <div class="col-sm-12">
                                <a href="{{ route('productos.index') }}" class="btn btn-success">Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
