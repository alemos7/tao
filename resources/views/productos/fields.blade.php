<div class="row">
    <div class="col-sm-12">
        <!-- Categoria Id Field -->
        <div class="form-group">
            {!! Form::label('categoria_id', 'Categoria:') !!}
            <select class="form-control {{ $errors->has('categoria_id') ? ' is-invalid' : '' }}" name="categoria_id" id="categoria_id">
                <option value="">:: Seleccione ::</option>
                @foreach($categoriasProductos AS $categoriasProducto)
                    <option value="{{$categoriasProducto->id}}" @isset($productos) @if($categoriasProducto->id==$productos->categoria_id) selected @endif @endisset @if(@old("categoria_id")==$categoriasProducto->id) selected @endif>{{$categoriasProducto->categoria}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-4">
        <!-- Codigo Field -->
        <div class="form-group">
            {!! Form::label('codigo', 'Código:') !!}
            {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <div class="col-sm-4">

        <!-- Producto Field -->
        <div class="form-group">
            {!! Form::label('producto', 'Producto:') !!}
            {!! Form::text('producto', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <div class="col-sm-4">
        <!-- Producto Field -->
        <div class="form-group">
            {!! Form::label('coeficiente', 'Coeficiente:') !!}
            {!! Form::text('coeficiente', null, ['class' => 'form-control']) !!}
        </div>

    </div>
</div>

<hr>
<div class="row float-right">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('productos.index') }}" class="btn btn-success">Cancelar</a>
    </div>

</div>