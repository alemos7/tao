@extends('templates.material.main')
@section('jquery') {{-- Including this section to override it empty. Using jQuery from webpack build --}} @endsection
@push('before-scripts')
    <script src="{{ mix('/js/home-one.js') }}"></script>
@endpush
@section('nombre_modulo', __('Menu'))
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">{{ __('Inicio') }}</a></li>
    <li class="breadcrumb-item"><a href="{{route('menus.index')}}">{{ __('Menu') }}</a></li>
    <li class="breadcrumb-item active">{{ __('Detalle') }}</li>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            {{ __('Menu') }}# <b>{{str_pad($menu->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <p><strong>{{ __('Nombre del Menu') }}:</strong> {{$menu->menu}}</p>
                <p><strong>{{ __('Ruta') }}:</strong> {{$menu->ruta}}</p>
                <p><strong>{{ __('Padre') }}:</strong> {{$menu->padre}}</p>
                <p><strong>{{ __('Nivel') }}:</strong> {{$menu->nivel}}</p>

                <hr>
                <a href="{{ URL::previous() }}" class="btn btn-outline-success float-right">{{__('Regresar')}}</a>
            </div>
        </div>
    </div>

@endsection