<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $solicitudes->id }}</p>
</div>

<!-- Solicitud Field -->
<div class="form-group">
    {!! Form::label('solicitud', 'Solicitud:') !!}
    <p>{{ $solicitudes->solicitud }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $solicitudes->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $solicitudes->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $solicitudes->deleted_at }}</p>
</div>

