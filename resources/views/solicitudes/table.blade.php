<div class="table-responsive">
    <table class="table" id="solicitudes-table">
        <thead>
            <tr>
                <th>Solicitud</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($solicitudes as $solicitudes)
            <tr>
                <td>{{ $solicitudes->solicitud }}</td>
                <td>
                    {!! Form::open(['route' => ['solicitudes.destroy', $solicitudes->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('solicitudes.show', [$solicitudes->id]) }}" class='btn btn-default btn-xs'><i class="fas fa-eye"></i></a>
                        <a href="{{ route('solicitudes.edit', [$solicitudes->id]) }}" class='btn btn-default btn-xs'><i class="fas fa-edit"></i></a>
                        {!! Form::button('<i class="fas fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
