<!-- Tipo Obra Field -->
<div class="form-group">
    <b>{!! Form::label('tipo_obra', 'Tipo Obra:') !!}</b>
    <p>{{ $tipoObras->tipo_obra }}</p>
</div>

<!-- Tipo Obra Field -->
<div class="form-group">
    <b>{!! Form::label('tipo_obra', 'Coeficiente Constante:') !!}</b>
    <p>{{ $tipoObras->coeficiente }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <b>{!! Form::label('created_at', 'Created At:') !!}</b>
    <p>{{ $tipoObras->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <b>{!! Form::label('updated_at', 'Updated At:') !!}</b>
    <p>{{ $tipoObras->updated_at }}</p>
</div>
