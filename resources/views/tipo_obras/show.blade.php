@extends('layouts.app')
@section('nombre_modulo', 'Tipo Obras')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('tipoObras.index')}}">Tipo Obras</a></li>
    <li class="breadcrumb-item active">Tipo de Obra</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Tipo de Obra# <b>{{str_pad($tipoObras->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    @include('tipo_obras.show_fields')
                    <hr>
                    <div class="row float-right">
                        <div class="col-sm-12">
                            <a href="{{ route('tipoObras.index') }}" class="btn btn-success">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
