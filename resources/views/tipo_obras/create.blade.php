@extends('layouts.app')
@section('nombre_modulo', 'Tipo Obras')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('tipoObras.index')}}">Tipo Obras</a></li>
    <li class="breadcrumb-item active"><a href="{{route('tipoObras.create')}}">Crear Tipo de Obra</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-campground"></i>
            Tipo de Obra
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tipoObras.store', 'class'=>'w-100']) !!}

                        @include('tipo_obras.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
