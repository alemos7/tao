@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
            <tr>
                <th>Tipo Obra</th>
                <th>Coeficiente Constante</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipoObras as $tipoObras)
            <tr>
                <td>{{ $tipoObras->tipo_obra }}</td>
                <td>{{ $tipoObras->coeficiente }}</td>
                <td class="td-actions text-center">
                    @can('tipoObras.show')
                        <a href="{{ route('tipoObras.show', [$tipoObras->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('tipoObras.edit')
                        <a href="{{ route('tipoObras.edit', [$tipoObras->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('tipoObras.destroy')
                        {!! Form::open(['route' => ['tipoObras.destroy', $tipoObras->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection