@extends('layouts.app')
@section('nombre_modulo', 'Tipo Obras')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Configuración</li>
    <li class="breadcrumb-item"><a href="{{route('tipoObras.index')}}">Tipo Obras</a></li>
    <li class="breadcrumb-item active">Editar Tipo Obras</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tie"></i>
            Tipo de Obra# <b>{{str_pad($tipoObras->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoObras, ['route' => ['tipoObras.update', $tipoObras->id], 'class'=>'w-100', 'method' => 'patch']) !!}

                        @include('tipo_obras.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection