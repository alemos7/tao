<div class="row">
    <div class="col-sm-6">
        <!-- Tipo Obra Field -->
            {!! Form::label('tipo_obra', 'Tipo Obra:') !!}
            {!! Form::text('tipo_obra', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
            {!! Form::label('coeficienteConstante', 'Coeficiente Constante:') !!}
            {!! Form::text('coeficienteConstante', null, ['class' => 'form-control']) !!}
    </div>

</div>

<hr>
<div class="row float-right">
    <!-- Submit Field -->
    <div class="col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('tipoObras.index') }}" class="btn btn-success">Cancelar</a>
    </div>

</div>