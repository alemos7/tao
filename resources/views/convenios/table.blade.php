<div class="table-responsive">
    <table class="table" id="convenios-table">
        <thead>
        <tr>
            <th>Convenio</th>
            <th>Valor Cobrado</th>
            <th>Periodo</th>
            <th>Entrada</th>
            <th>Salida</th>
            <th>Total Horas</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($convenios as $convenio)
            <tr>
                <td>{{ $convenio->convenio }}</td>
                <td>{{ $convenio->valor_cobrado }}</td>
                <td>{{ $convenio->periodo }}</td>
                <td>{{ $convenio->entrada }}</td>
                <td>{{ $convenio->salida }}</td>
                <td>{{ $convenio->total_horas }}</td>
                <td class="td-actions text-center">
                    @can('convenios.edit')
                        <a href="{{route('convenios.edit', [$convenio->id])}}" class="btn btn-outline-success btn-round btn-sm btnSave" title="Guardar">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    @endcan
                    @can('convenios.destroy')
                        {!! Form::open(['route' => ['convenios.destroy', $convenio->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
