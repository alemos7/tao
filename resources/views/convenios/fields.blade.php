@section('css')
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

<div class="row">
    <!-- Convenio Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('convenio', 'Convenio:') !!}
        <select name="convenio" id="convenio" class="form-control">
            <option value="">:: Seleccione ::</option>
            <option value="Oficial" @isset($convenios) @if('Oficial'==$convenios->convenio) selected @endif @endisset @if(@old("convenio")=='Oficial') selected @endif>Oficial</option>
            <option value="Medio Oficial" @isset($convenios) @if('Medio Oficial'==$convenios->convenio) selected @endif @endisset @if(@old("convenio")=='Medio Oficial') selected @endif>Medio Oficial</option>
            <option value="Ayudante" @isset($convenios) @if('Ayudante'==$convenios->convenio) selected @endif @endisset @if(@old("convenio")=='Ayudante') selected @endif>Ayudante</option>
        </select>
    </div>

    <!-- Valor Cobrado Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('valor_cobrado', 'Valor Cobrado:') !!}
        {!! Form::number('valor_cobrado', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('hora_extra', 'Hora Extra:') !!}
        {!! Form::number('hora_extra', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Periodo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('periodo', 'Periodo:') !!}
        {!! Form::number('periodo', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Entrada Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('entrada', 'Entrada:') !!}
        <input type="text" name="entrada" id="entrada" class="form-control" value="{{ @old("entrada", ($convenios)? $convenios->entrada : '') }}">
    </div>
    <!-- Salida Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('salida', 'Salida:') !!}
        <input type="text" name="salida" id="salida" class="form-control" value="{{ @old("salida", ($convenios)? $convenios->salida : '') }}">
    </div>

    <!-- Total Horas Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('total_horas', 'Total Horas:') !!}
        <h3 id="h3Total_horas"></h3>
        <input type="hidden" name="total_horas" id="total_horas">
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        <div id="msg"></div>
        <button id="btnCalcularHoras" class="btn btn-info" type="button"><i class="fas fa-redo-alt"></i> Calcular Horas Totales</button>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('convenios.index') }}" class="btn btn-default">Cancel</a>
    </div>

</div>

@section('js')
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/moment/moment.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>

    <script>

        $('#entrada').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            time: true,
            date: false,
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            'default': 'now',
        });

        $('#salida').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            time: true,
            date: false,
            lang: 'es',
            cancelText: 'Cancelar',
            okText: 'Siguiente',
            'default': 'now',
        });
    </script>
    <script>
        $(function () {
            function calcularHoras() {
                let entrada = moment($('#entrada').val(), 'HH:mm');
                let salida = moment($('#salida').val(), 'HH:mm');

                let totalHoras = moment.utc(moment.duration(salida - entrada).asMilliseconds()).format("HH:mm");

                if (totalHoras == 'Invalid date') {
                    $('#msg').html('<div class="alert alert-danger text-center">Error: Revise las horas de Entrada y Salida</div>');
                } else {
                    $('#h3Total_horas').html(totalHoras);
                    $('#total_horas').val(totalHoras);
                }
            }

            $('#btnCalcularHoras').click(function () {
                calcularHoras();
            });

            calcularHoras();
        });
    </script>
@endsection
