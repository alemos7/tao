<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $convenios->id }}</p>
</div>

<!-- Convenio Field -->
<div class="form-group">
    {!! Form::label('convenio', 'Convenio:') !!}
    <p>{{ $convenios->convenio }}</p>
</div>

<!-- Valor Cobrado Field -->
<div class="form-group">
    {!! Form::label('valor_cobrado', 'Valor Cobrado:') !!}
    <p>{{ $convenios->valor_cobrado }}</p>
</div>

<!-- Periodo Field -->
<div class="form-group">
    {!! Form::label('periodo', 'Periodo:') !!}
    <p>{{ $convenios->periodo }}</p>
</div>

<!-- Entrada Field -->
<div class="form-group">
    {!! Form::label('entrada', 'Entrada:') !!}
    <p>{{ $convenios->entrada }}</p>
</div>

<!-- Salida Field -->
<div class="form-group">
    {!! Form::label('salida', 'Salida:') !!}
    <p>{{ $convenios->salida }}</p>
</div>

<!-- Total Horas Field -->
<div class="form-group">
    {!! Form::label('total_horas', 'Total Horas:') !!}
    <p>{{ $convenios->total_horas }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $convenios->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $convenios->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $convenios->deleted_at }}</p>
</div>

