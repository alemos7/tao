@extends('layouts.app')
@section('nombre_modulo', 'Clientes')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="{{route('clientes.index')}}">Clientes</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tag"></i>
            Clientes
            @can('clientes.create')
                <a href="{{route('clientes.create')}}" class="btn btn-outline-success float-right">
                    <i class="fas fa-plus"></i>
                    Crear Cliente
                </a>
            @endcan
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('clientes.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

