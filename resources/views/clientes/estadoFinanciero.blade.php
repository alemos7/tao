@extends('layouts.app')
@section('nombre_modulo', 'Clientes')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item">Reportes</li>
    <li class="breadcrumb-item active"><a href="{{route('estadoFinanciero')}}">Estado Financiero</a></li>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-comment-dollar"></i>
            Estado Financiero
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table" id="data-table">
                        <thead>
                        <tr>
                            <th>Cliente</th>
{{--                            <th>Contacto</th>--}}
{{--                            <th>Teléfono</th>--}}
{{--                            <th>Email</th>--}}
                            <th>Monto a financiar</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->cliente }}</td>
                                <td>{{ $cliente->monto_financiar  }}</td>
{{--                                <td>{{ $cliente->telefono }}</td>--}}
{{--                                <td>{{ $cliente->email }}</td>--}}
{{--                                <td>{{ number_format($cliente->getTotalCuentaCorriente($cliente->id),2,'.',',') }}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection
