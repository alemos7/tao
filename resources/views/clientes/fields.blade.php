<div class="row">
    <!-- Cuit Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('cuit', 'CUIT:') !!}
        {!! Form::text('cuit', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Dni Field -->
{{--    <div class="form-group col-sm-4">--}}
{{--        {!! Form::label('dni', 'DNI:') !!}--}}
{{--        {!! Form::text('dni', null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}

    <!-- Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('cliente', 'Cliente:') !!}
        {!! Form::text('cliente', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Contacto Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('contacto', 'Contacto:') !!}
        {!! Form::text('contacto', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Telefono Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('telefono', 'Teléfono:') !!}
        {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Provincia Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('provincia_id', 'Provincia:') !!}
        <select class="form-control {{ $errors->has('provincia_id') ? ' is-invalid' : '' }}" name="provincia_id" id="provincia_id">
            <option value="">:: Seleccione ::</option>
            @foreach($provincias AS $provincia)
                <option value="{{$provincia->id}}" @isset($ordenes) @if($provincia->id==$ordenes->provincia_id) selected
                        @endif @endisset @if(@old("provincia_id")==$provincia->id) selected @endif>{{$provincia->name}}</option>
            @endforeach
        </select>
    </div>
    <!-- Consumidor Localidad Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('localidad_id', 'Localidad:') !!}
        <select name="localidad_id" id="localidad_id" class="form-control">
            <option value="">:: Debe Selecionar una Provincia ::</option>
        </select>
    </div>

    <!-- Direccion Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Observacion Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('observacion', 'Observación:') !!}
        {!! Form::textarea('observacion', null, ['class' => 'form-control', 'rows' => 0]) !!}
    </div>

    <!-- Bonificacion Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('monto_financiar', 'Monto a Financiar:') !!}
        {!! Form::number('monto_financiar', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('periodo_facturarcion', 'Período de Facturación:') !!}
        {!! Form::text('periodo_facturarcion', null, ['class' => 'form-control']) !!}
    </div>

</div>
<hr>
<div class="row float-right">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('clientes.index') }}" class="btn btn-default">Cancel</a>
    </div>

</div>

@if(isset($ordenes))
    <input type="hidden" name="ciudad_select" id="ciudad_select" value="{{$ordenes->localidad_id}}">
@endif

@section('scripts')
    <script src="{{asset('js/geo_dependientes_2niveles.js')}}"></script>
@endsection
