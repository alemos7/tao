<div class="row mt-4">
    <!-- Id Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('id', 'Id:') !!}</b>
        <p>{{str_pad($clientes->id, 6, '0', STR_PAD_LEFT)}}</p>
    </div>

    <!-- Cuit Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('cuit', 'CUIT:') !!}</b>
        <p>{{ $clientes->cuit }}</p>
    </div>

    <!-- Dni Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('dni', 'DNI:') !!}</b>
        <p>{{ $clientes->dni }}</p>
    </div>

    <!-- Cliente Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('cliente', 'Cliente:') !!}</b>
        <p>{{ $clientes->cliente }}</p>
    </div>

    <!-- Contacto Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('contacto', 'Contacto:') !!}</b>
        <p>{{ $clientes->contacto }}</p>
    </div>

    <!-- Telefono Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('telefono', 'Teléfono:') !!}</b>
        <p>{{ $clientes->telefono }}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('email', 'Email:') !!}</b>
        <p>{{ $clientes->email }}</p>
    </div>

    <!-- Provincia Id Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('provincia_id', 'Provincia:') !!}</b>
        <p>{{ ($clientes->provincia_id)? $clientes->provincias->name : '' }}</p>
    </div>

    <!-- Localidad Id Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('localidad_id', 'Localidad:') !!}</b>
        <p>{{ ($clientes->localidad_id)? $clientes->localidades->name : '' }}</p>
    </div>

    <!-- Direccion Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('direccion', 'Dirección:') !!}</b>
        <p>{{ $clientes->direccion }}</p>
    </div>


    <!-- Bonificacion Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('monto_financiar', 'Monto a Financiar:') !!}</b>
        <p>{{ $clientes->monto_financiar }}</p>
    </div>

    <!-- Bonificacion Field -->
    <div class="form-group col-sm-4">
        <b>{!! Form::label('periodo_facturarcion', 'Perdiodo de Facturación:') !!}</b>
        <p>{{ $clientes->periodo_facturarcion }}</p>
    </div>
    <!-- Observacion Field -->
    <div class="form-group col-sm-12">
        <b>{!! Form::label('observacion', 'Observación:') !!}</b>
        <p>{{ $clientes->observacion }}</p>
    </div>


</div>