@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Contacto</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Provincia</th>
            <th>Localidad</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clientes as $clientes)
            <tr>
                <td>{{ $clientes->cliente }}</td>
                <td>{{ $clientes->contacto }}</td>
                <td>{{ $clientes->telefono }}</td>
                <td>{{ $clientes->email }}</td>
                <td>{{ ($clientes->provincias)? $clientes->provincias->name : ''}}</td>
                <td>{{ ($clientes->localidades)? $clientes->localidades->name : ''}}</td>
                <td class="td-actions text-center">
                    @can('clientes.show')
                        <a href="{{ route('clientes.show', [$clientes->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('clientes.edit')
                        <a href="{{ route('clientes.edit', [$clientes->id]) }}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('clientes.destroy')
                        {!! Form::open(['route' => ['clientes.destroy', $clientes->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>

@endsection