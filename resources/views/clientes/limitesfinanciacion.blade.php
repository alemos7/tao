@extends('layouts.app')
@section('nombre_modulo', 'Clientes')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="{{route('clientes.index')}}">Clientes</a></li>
    <li class="breadcrumb-item active">Cliente</li>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="col-sm-12">

                        Limite de Financiacio: {{$limite}}

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
