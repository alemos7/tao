@extends('layouts.app')
@section('nombre_modulo', 'Clientes')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="{{route('clientes.index')}}">Clientes</a></li>
    <li class="breadcrumb-item active">Cliente</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-user-tag"></i>
            Cliente# <b>{{str_pad($clientes->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="col-sm-12">

                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#datos" role="tab">
                                    <span class="hidden-sm-up">
                                        <i class="fas fa-user"></i>
                                    </span>
                                    <span class="hidden-xs-down">
                                        <i class="icon-user"></i>
                                        Datos del Cliente
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#cuenta" role="tab">
                                    <span class="hidden-sm-up">
                                        <i class="icon-graph"></i>
                                    </span>
                                    <span class="hidden-xs-down">
                                        <i class="icon-graph"></i>
                                        Cuenta Corriente
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="datos" role="tabpanel">
                                @include('clientes.datos_clientes')
                            </div>
                            <div class="tab-pane  p-20" id="cuenta" role="tabpanel">
                                @include('clientes.cuenta_corriente')
                            </div>
                        </div>
                        <hr>
                        <a href="{{ route('clientes.index') }}" class="btn btn-primary float-right">Regresar</a>


                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
