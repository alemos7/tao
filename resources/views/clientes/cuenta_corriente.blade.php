<table class="table">
    <thead>
    <tr>
        <th>Fecha</th>
        <th>Tipo de Comprobante</th>
        <th>Nº de Orden</th>
        <th>Nº de Suborden</th>
        <th>Monto del Comprobante</th>
        <th>Debe</th>
        <th>Haber</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
        @foreach($subordenes AS $item)
            <tr>
                <td>{{ Carbon\Carbon::parse($item->fecha)->format('d/m/Y') }}</td>
                <td>{{$item->tipo}}</td>
                <td>{{str_pad($item->orden, 6, '0', STR_PAD_LEFT)}}</td>
                <td>{{($item->suborden_id)? str_pad($item->suborden_id, 6, '0', STR_PAD_LEFT) : '' }}</td>
                <td>{{number_format($item->monto,2,',','.')}}</td>
                <td>{{($item->tipo=='Orden')? number_format($item->monto,2,',','.') : '0'}}</td>
                <td>{{($item->tipo=='Recibo')? number_format($item->monto,2,',','.') : '0'}}</td>
                <td>{{number_format(($item->total),2,',','.')}}</td>
            </tr>
        @endforeach
    </tbody>
</table>