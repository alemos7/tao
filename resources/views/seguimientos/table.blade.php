@section('css')
    <link rel="stylesheet" href="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.css')}}">
@endsection

<div class="table-responsive">
    <table class="table" id="data-table">
        <thead>
        <tr>
            <th>Suborden</th>
            <th>Usuario</th>
            <th>Hora</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Observacion</th>
            <th>Latitud</th>
            <th>Longitud</th>
        </tr>
        </thead>
        <tbody>
        @foreach($seguimientos as $seguimiento)
            <tr>
                <td>{{ str_pad($seguimiento->suborden_id, 6, '0', STR_PAD_LEFT) }}</td>
                <td>{{ $seguimiento->usuarios->nombre }}</td>
                <td>{{ $seguimiento->hora }}</td>
                <td>{{ $seguimiento->fecha }}</td>
                <td>{{ ($seguimiento->tipo==1)? 'Llego' : 'Se Fue'  }}</td>
                <td>{{ $seguimiento->observacion }}</td>
                <td>{{ $seguimiento->latitud }}</td>
                <td>{{ $seguimiento->longitud }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('js')

    <script src="{{asset('vendor/wrappixel/material-pro/4.2.1/assets/plugins/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/data_tables_index.js')}}"></script>
@endsection
