@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Adicionales Eficiencia
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                    <div class="col-sm-12">
                        @include('flash::message')
                    </div>

                    <form action="guardarAdicionales" method="post">
                        @csrf

                        @foreach($adicionales AS $adicional)
                        <div class="row">
                            <input type="hidden" name="id[]" value="{{$adicional->id}}">
                            <!-- Convenio Id Field -->
                            <div class="form-group col-sm-3 text-center">
                                <label for="convenio_id">{{ __('Convenio') }}</label>
                                <select class="form-control {{ $errors->has('convenio_id') ? ' is-invalid' : '' }}" name="convenio_id[]" id="convenio_id">
                                    <option value="">:: Seleccione ::</option>
                                    @foreach($convenios AS $convenio)
                                        <option value="{{$convenio->id}}" @isset($adicional) @if($convenio->id==$adicional->convenio_id) selected
                                                @endif @endisset @if(old('convenio_id')==$convenio->id) selected @endif>{{$convenio->convenio}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('convenio_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('convenio_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <!-- Minimo Field -->
                            <div class="form-group col-sm-3 text-center">
                                {!! Form::label('minimo', 'Minimo:') !!}
                                {!! Form::number('minimo[]', $adicional->minimo, ['class' => 'form-control']) !!}
                            </div>

                            <!-- Maximo Field -->
                            <div class="form-group col-sm-3 text-center">
                                {!! Form::label('maximo', 'Maximo:') !!}
                                <table>
                                    <tr>
                                        <td style="font-size: xx-large" class="font-weight-bold">>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="center">{!! Form::number('maximo[]', $adicional->maximo, ['class' => 'form-control']) !!}</td>
                                        <td style="font-size: xx-large" class="font-weight-bold">&nbsp;&nbsp;&nbsp;&nbsp;<</td>
                                    </tr>
                                </table>
                            </div>

                            <!-- Adicional Costo Field -->
                            <div class="form-group col-sm-3 text-center">
                                {!! Form::label('adicional_costo', 'Adicional Costo:') !!}
                                {!! Form::number('adicional_costo[]', $adicional->adicional_costo, ['class' => 'form-control']) !!}
                            </div>


                        </div>
                        @endforeach
                            <!-- Submit Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                <a href="{{ route('adicionalesEficiencias.index') }}" class="btn btn-default">Cancel</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection