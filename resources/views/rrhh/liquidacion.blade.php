@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Liquidar sueldos
        </h1>
    </section>

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <form class="" action="{{route('liquidacion')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Empleado:</label>
                                <select class="form-control" name="user">
                                    <option value="">:: Seleccione ::</option>
                                    @foreach($usuarios AS $item)
                                        <option value="{{$item->id}}" @if($usuario->id==$item->id) selected @endif>{{$item->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Periodo:</label>
                                <select class="form-control" name="periodo">
                                    <option value="">:: Seleccione ::</option>
                                    <option value="1" @if($periodo=='1') selected @endif>Primera Quincena</option>
                                    <option value="2" @if($periodo=='2') selected @endif>Segunda Quincena</option>
                                    <option value="3" @if($periodo=='3') selected @endif>Mes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Mes:</label>
                                <select class="form-control" name="mes">
                                    <option value="">:: Seleccione ::</option>
                                    <option value="1" @if($mes=='1') selected @endif>Enero</option>
                                    <option value="2" @if($mes=='2') selected @endif>Febrero</option>
                                    <option value="3" @if($mes=='3') selected @endif>Marzo</option>
                                    <option value="4" @if($mes=='4') selected @endif>Abril</option>
                                    <option value="5" @if($mes=='5') selected @endif>Mayo</option>
                                    <option value="6" @if($mes=='6') selected @endif>Junio</option>
                                    <option value="7" @if($mes=='7') selected @endif>Julio</option>
                                    <option value="8" @if($mes=='8') selected @endif>Agosto</option>
                                    <option value="9" @if($mes=='9') selected @endif>Septiembre</option>
                                    <option value="10" @if($mes=='10') selected @endif>Octubre</option>
                                    <option value="11" @if($mes=='11') selected @endif>Noviembre</option>
                                    <option value="12" @if($mes=='12') selected @endif>Diciembre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <button type="submit" name="button" class="btn btn-primary">Calcular</button>
                            </div>

                        </div>


                    </div>
                </form>
                <div class="row m-t-30">
                    <div class="col-lg-6">
                        <span><b>Empleado:</b> {{$usuario->nombre}}</span><span class="float-right"><b>Convenio:</b> {{$usuario->convenios->convenio}}</span>
                        <table class="table table-bordered mt-3">
                            <thead>
                            <tr>
                                <th>
                                    Concepto
                                </th>
                                <th class="text-right">Monto</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    Sueldo Básico
                                </td>
                                <td align="right">
                                    ${{ $sueldoBasico = $usuario->convenios->valor_cobrado }}
                                </td>
                            </tr>
                            <tr>
                                <td>Extra por Chofer (<b>{{$cantchofer}} x {{$costoAC->adicional_costo}}</b>)</td>
                                <td align="right">
                                    ${{ $extraChofer = $costoAC->adicional_costo * $cantchofer }}
                                </td>
                            </tr>
                            <tr>
                                <td>Adicional Eficiencia (Cant. Modulos <b>{{$cantModulos}}</b>)</td>
                                <td align="right">
                                    ${{ ($adicionalEficiencia)? $eficiencia = $adicionalEficiencia->adicional_costo : $eficiencia=0}}
                                </td>
                            </tr>
                            @if(count($horasExtras)>0)
                                <tr>
                                    <td>Total Horas extras: <b>{{$horasExtras->sum('horas_extras')}} hras. x ${{$horasExtras[0]->monto_hora_extra}}</b>)</td>
                                    <td align="right">
                                        ${{ (count($horasExtras)>0)? $adicionalHorasExtras = $horasExtras->sum('horas_extras') * $horasExtras[0]->monto_hora_extra : $adicionalHorasExtras = 0 }}
                                    </td>
                                </tr>
                            @else
                                <?php $adicionalHorasExtras = 0; ?>
                            @endif
                            @if(count($trabajoFeriados)>0)
                                <tr>
                                    <td>Trabajos en días feriados: <b>{{count($trabajoFeriados)}}</b>)</td>
                                    <td align="right">
                                        ${{ $extrasFeriados=$trabajoFeriados->sum('horas_extras') }}
                                    </td>
                                </tr>
                            @else
                                <?php $extrasFeriados = 0; ?>
                            @endif
                            @if(count($totalPrestamos)>0)
                                @foreach($totalPrestamos AS $prestamo)
                                    <tr>
                                        <td>Prestamo de fecha: {{Carbon\Carbon::parse($prestamo->fecha)->format('d/m/Y')}}, cuota nro: {{$prestamo->nro_cuota}}</td>
                                        <td align="right">
                                            -${{ $prestamo->monto_cuota }}
                                        </td>
                                    </tr>
                                @endforeach
                                <?php $cuotaPrestamos = $totalPrestamos->sum('monto_cuota'); ?>
                            @else
                                <?php $cuotaPrestamos = 0; ?>

                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-right">Total</th>
                                <th class="text-right">${{ $totalLiquidacion = ($sueldoBasico + $extraChofer + $eficiencia + $adicionalHorasExtras + $extrasFeriados) - $cuotaPrestamos }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <h4>Ordenes de trabajo</h4>
                        <table class="table table-bordered mt-3" id="data-table">
                            <thead>
                            <tr>
                                <th>Orden Nº</th>
                                <th>Suborden Nº</th>
                                <th>Fecha</th>
                                <th>Ver</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ordenes as $ordenesItem)
                                <tr>
                                    <td>{{str_pad($ordenesItem->subordenes->orden_id, 6, '0', STR_PAD_LEFT)}}</td>
                                    <td>{{str_pad($ordenesItem->suborden_id, 6, '0', STR_PAD_LEFT)}}</td>
                                    <td>{{ Carbon\Carbon::parse($ordenesItem->fecha)->format('d/m/Y') }}</td>
                                    <td>
                                        <a href="{{route('subOrden.edit', [$ordenesItem->suborden_id])}}" class="btn btn-outline-success btn-round btn-sm btnSave">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <h4>Llegadas Tarde</h4>
                        <table class="table table-bordered mt-3" id="data-table">
                            <thead>
                            <tr>
                                <th>Orden Nº</th>
                                <th>Suborden Nº</th>
                                <th>Fecha</th>
                                <th>Inicio</th>
                                <th>Llegada</th>
                                <th>Ver</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($llegadastarde as $llegadastardeItem)
                                <tr>
                                    <td>{{$llegadastardeItem->orden_id}}</td>
                                    <td>{{$llegadastardeItem->suborden_id }}</td>
                                    <td>{{$llegadastardeItem->fecha }}</td>
                                    <td>{{Carbon\Carbon::parse($llegadastardeItem->hora_inicio)->format('H:m A') }}</td>
                                    <td>{{Carbon\Carbon::parse($llegadastardeItem->hora)->format('H:m A') }}</td>
                                    <td>
                                        <a href="{{route('subOrden.edit', [$llegadastardeItem->suborden_id])}}" class="btn btn-outline-success btn-round btn-sm btnSave">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <form class="" action="{{route('storeLiquidacion')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="user_id" value="{{ $usuario->id }}">
                            <input type="hidden" name="periodo" value="{{ $periodo }}">
                            <input type="hidden" name="mes" value="{{ $mes }}">
                            <input type="hidden" name="idsPrestamos" value="{{ json_encode($totalPrestamos) }}">
                            <input type="hidden" name="sueldoBasico" value="{{$sueldoBasico}}">
                            <input type="hidden" name="extraChofer" value="{{$extraChofer}}">
                            <input type="hidden" name="extrasFeriados" value="{{$extrasFeriados}}">
                            <input type="hidden" name="eficiencia" value="{{$eficiencia}}">
                            <input type="hidden" name="cuotaPrestamos" value="{{$cuotaPrestamos}}">
                            <input type="hidden" name="adicionalHorasExtras" value="{{$adicionalHorasExtras}}">
                            <input type="hidden" name="totalLiquidacion" value="{{$totalLiquidacion}}">

                            <button class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
