@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Liquidar sueldos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       @include('flash::message')
       <div class="box box-primary">
           <div class="box-body">
               <form class="" action="{{route('liquidacion')}}" method="post">
                 @csrf
                 <div class="row">
                     <div class="col-sm-3">
                         <select class="form-control" name="user">
                             <option value="">Seleccionar empleado</option>
                             @foreach($usuarios AS $item)
                                 <option value="{{$item->id}}">{{$item->nombre}}</option>
                             @endforeach
                         </select>
                     </div>
                     <div class="col-sm-3">
                         <select class="form-control" name="periodo">
                             <option value="">Seleccionar Periodo</option>
                             <option value="1">Primer Quincena</option>
                             <option value="2">Segunda Quincena</option>
                             <option value="3">Mes</option>
                         </select>
                     </div>
                   <div class="col-sm-3">
                       <select class="form-control" name="mes">
                           <option value="1">Enero</option>
                           <option value="2">Febrero</option>
                           <option value="3">Marzo</option>
                           <option value="4">Abril</option>
                           <option value="5">Mayo</option>
                           <option value="6">Junio</option>
                           <option value="7">Julio</option>
                           <option value="8">Agosto</option>
                           <option value="9">Septiembre</option>
                           <option value="10">Octubre</option>
                           <option value="11">Noviembre</option>
                           <option value="12">Diciembre</option>
                       </select>
                   </div>
                     <div class="col-sm-3">
                        <button type="submit" name="button" class="btn btn-primary">Calcular</button>

                     </div>


                 </div>
               </form>
           </div>
       </div>
   </div>
@endsection
