<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>TAO</title>
    <style>
        body {
            font-family: Arial;
        }

        .fondoGris {
            background-color: #f5f5f5;
        }

        .fondoGrisOscuro {
            background-color: #373736;
        }

        .alto2px {
            height: 2px
        }

        .padding40 {
            padding: 40px;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            line-height: 150%;
        }

        .fondoAzul {
            background-color: #0079be !important;
        }

        .LetrasBlancas {
            color: #FFFFFF !important;
        }

        .x-small {
            font-size: x-small;
        }

        .textCenter {
            text-align: center;
        }

        .textRight {
            text-align: right;
        }

        .tabla {
            width: 100%;
        }

        .tabla thead {
            background-color: #0079be;
            color: #ffffff;
        }
        .tabla tbody tr td {
            border: 1px solid #0079be;
        }
        .tabla tfoot tr td {
            border: 1px solid #0079be;
        }
        .tabla tfoot tr td:first-child {
            border: 0px !important;
        }
    </style>
</head>
<body>
<table style="margin: 0 auto; width: 75%">
    <tr>
        <td align="center">
            <img src="{{asset('images/logo.png')}}" alt="TAO" style="height: 10em"/>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr class="fondoAzul LetrasBlancas">
        <td align="center">
            <h1>Informa que no te quedaste dormido</h1>
        </td>
    </tr>
    <tr class="alto2px"><td></td></tr>
    <tr class="fondoAzul LetrasBlancas alto2px"><td></td></tr>
    <tr class="alto2px"><td></td></tr>
    <tr class="fondoGris">
        <td class="padding40">
            <p>Estimado,</p>
            <p><b>{{strtoupper($suborden->tecnicos4->nombre)}}</b></p>

            <p>El día de hoy le fue asignada una Suborden de tipo <b>{{$suborden->tipos->tipo}}</b></p>
            <p>
                <button>
                    <a href="visithouse.com.ar/nodormido/{{$suborden->tecnicos4->id}}">
                        No me quede Dormido
                    </a>
                </button>
            </p>
            <br>
            <br>
            <p>Atentamente,</p>
            <p><b>Equipo TAO</b></p>
        </td>
    </tr>
    <tr class="alto2px"><td></td></tr>
    <tr class="LetrasBlancas fondoAzul alto2px"><td></td></tr>
    <tr class="alto2px"><td></td></tr>
    <tr class="fondoGrisOscuro LetrasBlancas">
        <td class="padding10 textCenter">
            <span class="x-small">
                 TAO respeta su privacidad y solamente utiliza esta dirección de correo electrónico para mantenerlo informado sobre las operaciones logisticas y operativas de la empresa.
            </span>
        </td>
    </tr>
</table>

</body>
</html>
