@extends('layouts.app')
@section('nombre_modulo', 'Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active"><a href="{{route('ordenes.index')}}">Crear una Orden de Trabajo </a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="far fa-newspaper"></i>
            Crear una Orden de Trabajo
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ordenes.store', 'class'=>'w-100', 'files' => true]) !!}
                            @include('ordenes.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection