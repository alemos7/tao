<div class="table-responsive">
    <table class="table" id="ordenes-table">
        <thead>
        <tr>
            <th>Nº Orden</th>
            <th>Cliente</th>
            <th>Consumidor</th>
            <th>Subordenes</th>
            <th>Agregar Suborden</th>
            <th>Ver</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ordenes as $orden)
            <tr>
                <td>{{$codOrden=str_pad($orden->id, 6, '0', STR_PAD_LEFT)}}</td>
                <td>{{ ($orden->clientes)? $orden->clientes->cliente : ''}}</td>
                <td>{{ ($orden->clientes)? $orden->consumidor_nombre : ''}}</td>
                <td class="subdetail">
                    @if(count($orden->subOrdenes)>0)
                        <a href="{{route('subOrden.listar', [$orden->id])}}" class="m-r-10">
                            {{count($orden->subOrdenes)}}
                        </a>
                        @foreach($orden->subOrdenes as $subordenes)
                            @if($subordenes['tipo_id'] == "1")
                            <i class="fas fa-tools"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Instalacion"></i>
                            @endif
                            @if($subordenes['tipo_id'] == "2")
                            <i class="fas fa-pencil-ruler" data-toggle="tooltip" data-placement="top" title="" data-original-title="Medicion"></i>
                            @endif
                            @if($subordenes['tipo_id'] == "3")
                            <i class="far fa-life-ring"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Soporte"></i>
                            @endif
                            @if($subordenes['tipo_id'] == "4")
                            <i class="fas fa-exclamation-triangle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reclamo"></i>
                            @endif
                            @if($subordenes['tipo_id'] == "5")
                            <i class="far fa-check-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verificacion"></i>
                            @endif
                        @endforeach
                    @else
                        0
                    @endif
                </td>
                <td width="5%">
                    @can('subOrden.create')
                        <button type="button" data-target="#ModalTipo" data-toggle="modal" data-idorden="{{$orden->id}}" class="btn btn-outline-success btn-round btn-sm btnSubCrearOrden" title="">
                            <i class="fas fa-plus"></i>
                        </button>
                    @endcan
                </td>
                <td width="5%">
                    @can('ordenes.show')
                        <a href="{{route('ordenes.show', [$orden->id])}}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                </td>
                <td width="5%">
                    @can('ordenes.edit')
                        <a href="{{route('ordenes.edit', [$orden->id])}}" class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                </td>
                <td width="5%">
                    @can('ordenes.destroy')
                        {!! Form::open(['route' => ['ordenes.destroy', $orden->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                            <button class="btn btn-outline-success btn-round btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@include('ordenes.modalTipo')

@section('js')
    <script src="{{asset('js/modalTipoSubOrden.js')}}"></script>
@endsection
