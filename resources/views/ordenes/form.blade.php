{{--<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>--}}
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC09s4_7SUlpwPak7Irj6SP5rtqG6mWMDI&callback=initMap" type="text/javascript"></script>
@include('flash::message')
<div class="row">
    <!-- Cliente Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('cliente_id', 'Cliente:') !!}
        <select class="form-control {{ $errors->has('cliente_id') ? ' is-invalid' : '' }}" name="cliente_id" id="cliente_id">
            <option value="">:: Seleccione::</option>
            @foreach($clientes AS $cliente)
                <option value="{{$cliente->id}}" @isset($ordenes) @if($cliente->id==$ordenes->cliente_id) selected
                        @endif @endisset @if(@old("cliente_id")==$cliente->id) selected @endif>{{$cliente->cliente}}</option>
            @endforeach
        </select>
    </div>
    <!-- Vendedor Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('vendedor_id', 'Vendedor/a:') !!}
        <select name="vendedor_id" id="vendedor_id" class="form-control">
            <option value="">:: Debe Selecionar un Cliente ::</option>
        </select>
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('tipo_obra_id', 'Tipo Obra:') !!}
        <select class="form-control {{ $errors->has('tipo_obra_id') ? ' is-invalid' : '' }}" name="tipo_obra_id" id="tipo_obra_id">
            <option value="">:: Seleccione ::</option>
            @foreach($tipo_obras AS $tipo_obra)
                <option value="{{$tipo_obra->id}}" @isset($ordenes) @if($tipo_obra->id==$ordenes->tipo_obra_id) selected
                        @endif @endisset @if(@old("tipo_obra_id")==$tipo_obra->id) selected @endif>{{$tipo_obra->tipo_obra}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <!-- Consumidor Nombre Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('consumidor_nombre', 'Consumidor:') !!}
        {!! Form::text('consumidor_nombre', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Consumidor Tlf Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('consumidor_tlf', 'Teléfono:') !!}
        {!! Form::text('consumidor_tlf', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('consumidor_email', 'Email:') !!}
        {!! Form::text('consumidor_email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">

    <!-- Consumidor Provincia Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('consumidor_provincia_id', 'Provincia:') !!}
        <select class="form-control {{ $errors->has('consumidor_provincia_id') ? ' is-invalid' : '' }}" name="consumidor_provincia_id" id="provincia_id">
            <option value="">:: Seleccione ::</option>
            @foreach($provincias AS $provincia)
                <option value="{{$provincia->id}}" @isset($ordenes) @if($provincia->id==$ordenes->consumidor_provincia_id) selected
                        @endif @endisset @if(@old("consumidor_provincia_id")==$provincia->id) selected @endif>{{$provincia->name}}</option>
            @endforeach
        </select>
    </div>
    <!-- Consumidor Localidad Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('consumidor_localidad_id', 'Localidad:') !!}
        <select name="consumidor_localidad_id" id="localidad_id" class="form-control">
            <option value="">:: Debe Selecionar una Provincia ::</option>
        </select>
    </div>
    <!-- Consumidor Barrio Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('barrio_id', 'Barrio:') !!}
        <select class="form-control {{ $errors->has('barrio_id') ? ' is-invalid' : '' }}" name="barrio_id" id="barrio_id">
            <option value="">:: Seleccione ::</option>
            @foreach($barrios AS $barrio)
                <option value="{{$barrio->id}}" @isset($ordenes) @if($barrio->id==$ordenes->barrio_id) selected
                        @endif @endisset @if(@old("barrio_id")==$barrio->id) selected @endif>{{$barrio->barrio}}</option>
            @endforeach
        </select>
    </div>
    <!-- Consumidor Address Field -->

</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('consumidor_address', 'Dirección:') !!}
        {!! Form::textarea('consumidor_address', null, ['class' => 'form-control', 'cols'=>'', 'rows'=>''] ) !!}
    </div>
    <!-- Descripcion Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('observaciones', 'Observaciones:') !!}
        {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'cols'=>'', 'rows'=>''] ) !!}
    </div>
</div>
@if(!isset($ordenes))
<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        <label for="">Documentación:</label>
        <button type="button" class="btn btn-outline-info btn-block" data-toggle="modal" data-target="#ModalFiles" id="btnFile">
            <i class="fas fa-file-invoice"></i>
            Subir Archivos
        </button>
    </div>
</div>
@endif
<br>
<div class="row">
    <div>
        {!! Form::text('latitud', null, ['class' => 'form-control', 'style'=>'display: block']) !!}
        {!! Form::text('longitud', null, ['class' => 'form-control', 'style'=>'display: block']) !!}
    </div>
    <div style="height: 600px" class="col-12 col-md-12 col-lg-12">
        <label for="">Ubicación Geográfica:</label>
        <div id="map"></div>

{{--        {!! Mapper::render() !!}--}}
        <?php

        ?>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <hr>
    {!! Form::submit('Guardar', ['class' => 'btn btn-info']) !!}
    <a href="{{ route('ordenes.index') }}" class="btn btn-default">Cancelar</a>
</div>

@if(isset($ordenes))
    <input type="hidden" name="vendedor_select" id="vendedor_select" value="{{$ordenes->vendedor_id}}">
    <input type="hidden" name="ciudad_select" id="ciudad_select" value="{{$ordenes->consumidor_localidad_id}}">
@endif

@include('ordenes.modalFotos')

@section('scripts')
    <script src="{{asset('js/buscar_vendedores.js')}}"></script>
    <script src="{{asset('js/geo_dependientes_2niveles.js')}}"></script>
    <script src="{{asset('js/addFiles.js')}}"></script>

    <script>
        var map;

        function initMap(){

            //your code here
        }

        function showResult(result) {
            document.getElementById('latitud').value = result.geometry.location.lat();
            document.getElementById('longitud').value = result.geometry.location.lng();

            lat  = result.geometry.location.lat();
            lang = result.geometry.location.lng();

            var myLatlng = new google.maps.LatLng(lat,lang);
            var mapOptions = {
                zoom: 16,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                title:"Hello World!"
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);
        }
        $('#provincia_id').change(function () {
           let provincia_id = $('#provincia_id').val();

           if(provincia_id!=4147){
               $("#auto_id").addClass("is-invalid");
           }else{
               $("#auto_id").removeClass("is-invalid");
           }
        });

    </script>
@endsection
