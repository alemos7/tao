<!--/*************************************************************
**      MODAL Documento                                     **
**************************************************************/-->
<div id="ModalTipo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <form action="{{route('subOrden.create')}}" method="get" name="formTipo" id="formTipo" autocomplete="off">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Tipo de Sub Orden de Trabajo</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        @foreach($tipos AS $tipo)
                            <div class="row">
                                <label class="custom-control custom-radio">
                                    <input id="tipos_id{{$tipo->id}}" name="tipos_id" type="radio" class="custom-control-input" value="{{$tipo->id}}">
                                    <span class="custom-control-label">{{$tipo->tipo}}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="idOrden" id="idOrden" value="">
                    <button type="submit" class="btn btn-success waves-effect text-left">Siguiente</button>
                    <button type="button" class="btn btn-success waves-effect text-left" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--************************************************************-->
