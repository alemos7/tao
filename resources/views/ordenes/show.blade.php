@extends('layouts.app')
@section('nombre_modulo', 'Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active">Orden de Trabajo</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="far fa-newspaper"></i>
            Orden de Trabajo# <b>{{str_pad($ordenes->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <b class="font-weight-bold">{!! Form::label('cliente_id', 'Cliente:') !!}</b>
                            <h3>{{ $ordenes->clientes->cliente }}</h3>
                        </div>

                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('vendedor_id', 'Vendedor:') !!}</b>
                            <p>{{ $ordenes->vendedores->vendedor }}</p>
                        </div>
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('tipo_obra_id', 'Tipo Obra:') !!}</b>
                            <p>{{ $ordenes->tiposObras->tipo_obra }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Consumidor Nombre Field -->
                        <div class="form-group col-sm-8">
                            <b class="font-weight-bold">{!! Form::label('consumidor_nombre', 'Consumidor:') !!}</b>
                            <p>{{ $ordenes->consumidor_nombre }}</p>
                        </div>
                        <!-- Consumidor Tlf Field -->
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('consumidor_tlf', 'Teléfono:') !!}</b>
                            <p>{{ $ordenes->consumidor_tlf }}</p>
                        </div>
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('consumidor_email', 'Email:') !!}</b>
                            <p>{{ $ordenes->consumidor_email }}</p>
                        </div>

                    </div>
                    <div class="row">

                        <!-- Consumidor Provincia Id Field -->
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('consumidor_provincia_id', 'Provincia:') !!}</b>
                            <p>{{ $ordenes->provincias->name }}</p>
                        </div>
                        <!-- Consumidor Localidad Id Field -->
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('consumidor_localidad_id', 'Localidad:') !!}</b>
                            <p>{{ $ordenes->localidades->name }}</p>
                        </div>
                        <!-- Consumidor Address Field -->
                        <div class="form-group col-sm-4">
                            <b class="font-weight-bold">{!! Form::label('consumidor_address', 'Dirección:') !!}</b>
                            <p>{{ $ordenes->consumidor_address }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <!-- Descripcion Field -->
                        <div class="form-group col-sm-12 col-lg-12">
                            <b class="font-weight-bold">{!! Form::label('observaciones', 'Descripción del Servicio y Observaciones:') !!}</b>
                            <p>{{ $ordenes->observaciones }}</p>
                        </div>
                    </div>

                    <hr>
                    <div class="row float-right">
                        <div class="col-sm-12">
                            <a href="{{ route('ordenes.index') }}" class="btn btn-success">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
