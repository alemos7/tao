<!--    MODAL TOMAR FILE-->
<div class="modal fade" tabindex="-1" role="dialog" id="ModalFiles">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5>Subir Archivo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive-md">
                            <table class="table table-striped table-bordered" id="tableFiles">
                                <tbody>
                                <tr></tr>
                                <tr>
                                    <td>
                                        <input type="file" name="file[]" class="form-control pb-5">
                                    </td>
                                    <td style="vertical-align: middle">
                                        <button class="btn btn-outline-danger btn-sm btnDelFile" type="button">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <input type="hidden" id="contadorFile" name="contadorFile" value="1">
                        <button class="btn btn-primary btn-sm" type="button" id="btnAddFile">Agregar Otro Archivo</button>
                        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal">Terminar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
