@extends('layouts.app')
@section('nombre_modulo', 'Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="far fa-newspaper"></i>
            Ordenes de Trabajo
            @can('ordenes.create')
                <a href="{{route('ordenes.create')}}" class="btn btn-outline-success float-right">
                    <i class="fas fa-plus"></i>
                    Crear Orden de Trabajo
                </a>
            @endcan
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('ordenes.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection