@extends('layouts.app')
@section('nombre_modulo', 'Orden de Trabajo')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('home-one')}}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{route('ordenes.index')}}">Ordenes de Trabajo</a></li>
    <li class="breadcrumb-item active"><a href="{{route('ordenes.index')}}">Editar una Orden de Trabajo </a></li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <i class="far fa-newspaper"></i>
            Orden de Trabajo# <b>{{$codOrden=str_pad($ordenes->id, 6, '0', STR_PAD_LEFT)}}</b>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenes, ['route' => ['ordenes.update', $ordenes->id], 'class'=>'w-100', 'method' => 'patch']) !!}
                   @include('ordenes.form')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection