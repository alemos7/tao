<div class="table-responsive">
    <table class="table" id="notificaciones-table">
        <thead>
            <tr>
                <th>Comentario</th>
        <th>Fecha Desde</th>
        <th>Fecha Hasta</th>
        <th>Permission Role Id</th>
        <th>Estatus</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notificaciones as $notificaciones)
            <tr>
                <td>{{ $notificaciones->comentario }}</td>
            <td>{{ $notificaciones->fecha_desde }}</td>
            <td>{{ $notificaciones->fecha_hasta }}</td>
            <td>{{ $notificaciones->permission_role_id }}</td>
            <td>{{ $notificaciones->estatus }}</td>
                <td>
                    {!! Form::open(['route' => ['notificaciones.destroy', $notificaciones->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('notificaciones.show', [$notificaciones->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('notificaciones.edit', [$notificaciones->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
