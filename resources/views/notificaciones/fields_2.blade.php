<!-- Comentario Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comentario', 'Comentario:') !!}
    {!! Form::textarea('comentario', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Desde Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_desde', 'Fecha Desde:') !!}
    {!! Form::date('fecha_desde', null, ['class' => 'form-control','id'=>'fecha_desde']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_desde').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fecha Hasta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_hasta', 'Fecha Hasta:') !!}
    {!! Form::date('fecha_hasta', null, ['class' => 'form-control','id'=>'fecha_hasta']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_hasta').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Permission Role Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('permission_role_id', 'Permission Role Id:') !!}
    {!! Form::number('permission_role_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::number('estatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('notificaciones.index') }}" class="btn btn-default">Cancel</a>
</div>
