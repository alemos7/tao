<!-- Autos Field -->

<div class="row">
    <div class="form-group col-sm-12">
        {!! Form::label('autos', 'Autos:') !!}
        {!! Form::text('autos', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        {!! Form::label('observacion', 'Observación:') !!}
        <textarea name="observacion" id="observacion" class="form-control"></textarea>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <!-- Adjunto 2 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_1', 'Adjunto 1:') !!}
            {!! Form::file('adjunto_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <!-- Adjunto 2 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_2', 'Adjunto 2:') !!}
            {!! Form::file('adjunto_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <!-- Adjunto 3 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_3', 'Adjunto 3:') !!}
            {!! Form::file('adjunto_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <!-- Adjunto 4 Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('adjunto_4', 'Adjunto 4:') !!}
            {!! Form::file('adjunto_4', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('autos.index') }}" class="btn btn-default">Cancel</a>
</div>
