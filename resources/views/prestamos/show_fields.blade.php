<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $prestamos->id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $prestamos->user_id }}</p>
</div>

<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{{ $prestamos->monto }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $prestamos->fecha }}</p>
</div>

<!-- Cuotas Field -->
<div class="form-group">
    {!! Form::label('cuotas', 'Cuotas:') !!}
    <p>{{ $prestamos->cuotas }}</p>
</div>

<!-- Pendientes Field -->
<div class="form-group">
    {!! Form::label('pendientes', 'Pendientes:') !!}
    <p>{{ $prestamos->pendientes }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $prestamos->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $prestamos->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $prestamos->deleted_at }}</p>
</div>

