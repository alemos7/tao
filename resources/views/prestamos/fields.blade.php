<div class="row">

    <!-- User Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('user_id', 'Empleado:') !!}
        <select class="form-control" name="user_id">
            <option value="">Seleccionar empeado</option>
            @foreach($usuarios AS $item)
                <option value="{{$item->id}}">{{$item->nombre}}</option>
            @endforeach
        </select>
    </div>

    <!-- Monto Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('monto', 'Monto:') !!}
        {!! Form::number('monto', null, ['class' => 'form-control']) !!}
    </div>

<!-- Cuotas Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cuotas', 'Cuotas:') !!}
        {!! Form::number('cuotas', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('prestamos.index') }}" class="btn btn-default">Cancel</a>
    </div>

</div>