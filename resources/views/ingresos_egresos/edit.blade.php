@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ingresos Egresos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ingresosEgresos, ['route' => ['ingresosEgresos.update', $ingresosEgresos->id], 'method' => 'patch']) !!}

                        @include('ingresos_egresos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection