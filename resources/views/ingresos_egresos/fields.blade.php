<div class="row">

    <!-- Tipo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tipo', 'Tipo:') !!}
        <select name="tipo" id="tipo" class="form-control">
            <option value="">:: Seleccione ::</option>
            <option value="I" @isset($ingresosEgresos) @if('I'==$ingresosEgresos->tipo) selected @endif @endisset @if(@old("tipo")=='I') selected @endif>Ingreso</option>
            <option value="E" @isset($ingresosEgresos) @if('E'==$ingresosEgresos->tipo) selected @endif @endisset @if(@old("tipo")=='E') selected @endif>Egreso</option>
        </select>
    </div>

    <!-- Concepto Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('concepto', 'Concepto:') !!}
        {!! Form::text('concepto', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Monto Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('monto', 'Monto:') !!}
        {!! Form::number('monto', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Periocidad Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('periocidad', 'Dia del Mes:') !!}
        {!! Form::number('periocidad', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('ingresosEgresos.index') }}" class="btn btn-default">Cancel</a>
    </div>

</div>