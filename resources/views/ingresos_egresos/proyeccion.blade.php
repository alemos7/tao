@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-hamsa"></i>
            Proyección
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <div class="row">
                    <div class="col-12 m-t-30">
                        <div class="card-group">
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h1 class="m-b-0 text-white font-weight-bold text-center">Semana 1</h1>
                                </div>
                                <div class="card-body">
                                    <h4 class="text-center card-title">Del 1 al 7</h4>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6 font-weight-bold text-center">Ingreso</div>
                                        <div class="col-sm-6 font-weight-bold text-center">Egreso</div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $totalISem1=0;
                                        $totalESem1=0;
                                        ?>
                                        <div class="col-sm-6">
                                            @foreach($ie['ingresos']['semana1'] AS $ISem1)
                                                <?php
                                                $totalISem1 += $ISem1->monto;
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ISem1->concepto}}: {{$ISem1->monto}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-6">
                                            @for ($i = 0; $i < count($ie['ingresos']['semana1'])+1; $i++)
                                                <div class="row">
                                                    <div class="col-sm-12">&nbsp;</div>
                                                </div>
                                            @endfor
                                            @foreach($ie['egresos']['semana1'] AS $ESem1)
                                                <?php
                                                    $totalESem1 += $ESem1->monto*-1;
                                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ESem1->concepto}}: {{($ESem1->monto)*-1}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr class="hrgrueso">
                                    <div class="row">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-success">{{$totalISem1}}</h4>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-danger">{{$totalESem1*-1}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h1 class="m-b-0 text-white font-weight-bold text-center">Semana 2</h1>
                                </div>
                                <div class="card-body">
                                    <h4 class="text-center card-title">Del 8 al 14</h4>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6 font-weight-bold text-center">Ingreso</div>
                                        <div class="col-sm-6 font-weight-bold text-center">Egreso</div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $totalISem2=0;
                                        $totalESem2=0;
                                        ?>
                                        <div class="col-sm-6">
                                            @foreach($ie['ingresos']['semana2'] AS $ISem2)
                                                <?php
                                                $totalISem2 += $ISem2->monto;
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ISem2->concepto}}: {{$ISem2->monto}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-6">
                                            @for ($i = 0; $i < count($ie['ingresos']['semana2'])+1; $i++)
                                                <div class="row">
                                                    <div class="col-sm-12">&nbsp;</div>
                                                </div>
                                            @endfor
                                            @foreach($ie['egresos']['semana2'] AS $ESem2)
                                                <?php
                                                    $totalESem2 += $ESem2->monto*-1;
                                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ESem2->concepto}}: {{($ESem2->monto)*-1}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr class="hrgrueso">
                                    <div class="row">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-success">{{$totalISem2}}</h4>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-danger">{{$totalESem2*-1}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h1 class="m-b-0 text-white font-weight-bold text-center">Semana 3</h1>
                                </div>
                                <div class="card-body">
                                    <h4 class="text-center card-title">Del 15 al 21</h4>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6 font-weight-bold text-center">Ingreso</div>
                                        <div class="col-sm-6 font-weight-bold text-center">Egreso</div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $totalISem3=0;
                                        $totalESem3=0;
                                        ?>
                                        <div class="col-sm-6">
                                            @foreach($ie['ingresos']['semana3'] AS $ISem3)
                                                <?php
                                                $totalISem3 += $ISem3->monto;
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ISem3->concepto}}: {{$ISem3->monto}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-6">
                                            @for ($i = 0; $i < count($ie['ingresos']['semana3'])+1; $i++)
                                                <div class="row">
                                                    <div class="col-sm-12">&nbsp;</div>
                                                </div>
                                            @endfor
                                            @foreach($ie['egresos']['semana3'] AS $ESem3)
                                                <?php
                                                    $totalESem3 += $ESem3->monto*-1;
                                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ESem3->concepto}}: {{($ESem3->monto)*-1}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr class="hrgrueso">
                                    <div class="row">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-success">{{$totalISem3}}</h4>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-danger">{{$totalESem3*-1}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h1 class="m-b-0 text-white font-weight-bold text-center">Semana 4</h1>
                                </div>
                                <div class="card-body">
                                    <h4 class="text-center card-title">Del 22 al {{$ultimoDiaMes}}</h4>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6 font-weight-bold text-center">Ingreso</div>
                                        <div class="col-sm-6 font-weight-bold text-center">Egreso</div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $totalISem4=0;
                                        $totalESem4=0;
                                        ?>
                                        <div class="col-sm-6">
                                            @foreach($ie['ingresos']['semana4'] AS $ISem4)
                                                <?php
                                                $totalISem4 += $ISem4->monto;
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ISem4->concepto}}: {{$ISem4->monto}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-6">
                                            @for ($i = 0; $i < count($ie['ingresos']['semana4'])+1; $i++)
                                                <div class="row">
                                                    <div class="col-sm-12">&nbsp;</div>
                                                </div>
                                            @endfor
                                            @foreach($ie['egresos']['semana4'] AS $ESem4)
                                                <?php
                                                    $totalESem4 += $ESem4->monto*-1;
                                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <small>{{$ESem4->concepto}}: {{($ESem4->monto)*-1}}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr class="hrgrueso">
                                    <div class="row">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-success">{{$totalISem4}}</h4>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <h4 class="font-weight-bold text-danger">{{$totalESem4*-1}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

