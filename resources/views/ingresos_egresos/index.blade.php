@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fas fa-chart-line"></i>
            Ingresos y Egresos
            @can('ordenes.create')
                <a href="{{route('ingresosEgresos.create')}}" class="btn btn-outline-success float-right">
                    <i class="fas fa-plus"></i>
                    Crear Nuevo
                </a>
            @endcan
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('ingresos_egresos.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

