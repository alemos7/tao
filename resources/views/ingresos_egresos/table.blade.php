<div class="table-responsive">
    <table class="table" id="ingresosEgresos-table">
        <thead>
        <tr>
            <th>Concepto</th>
            <th>Monto</th>
            <th>Periocidad</th>
            <th>Tipo</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ingresosEgresos as $ingresosEgresos)
            <tr>
                <td>{{ $ingresosEgresos->concepto }}</td>
                <td>{{ $ingresosEgresos->monto }}</td>
                <td>{{ $ingresosEgresos->periocidad }}</td>
                <td>{{ ($ingresosEgresos->tipo=='I')? 'Ingreso' : 'Egreso' }}</td>
                <td class="td-actions text-center">
                    @can('ingresosEgresos.edit')
                        <a href="{{route('ingresosEgresos.edit', [$ingresosEgresos->id])}}" class="btn btn-outline-success btn-round btn-sm btnSave" title="Guardar Cantidad">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    @endcan
                    @can('ingresosEgresos.destroy')
                        {!! Form::open(['route' => ['ingresosEgresos.destroy', $ingresosEgresos->id], 'method' => 'DELETE','id' => 'formDelete','class' => 'd-inline']) !!}
                        <button class="btn btn-outline-success btn-round btn-sm">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
