@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Productos de la Caja
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'herramientas.store']) !!}

                        @include('herramientas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
